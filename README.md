# GestorDeProyectos

Sistema de Gestión de configuración de artefactos

## Comandos Utiles
Generar Documentacion con Pycco
```
pycco **/*.py -p -i
```

Preparar Migrar
```
DELETE FROM django_migrations WHERE app='Proyecto';
DROP TABLE "Proyecto_miembros", "Proyecto_planningpoker", 
"Proyecto_proyectos", "Proyecto_roles", "Proyecto_sprint",
"Proyecto_userstory", "Proyecto_actividadus", "Proyecto_historialmodificacion" CASCADE;
```

## Dependecias a Instalar en la VENV
```
python -m pip install --upgrade pip
pip install -r requirements.txt
```

## Agregar Origin
```
cd existing_repo
git remote add origin https://gitlab.com/larrea.pedrolarrea.pedro/gestordeproyectos.git
git branch -M main
git push -uf origin main
```

## Notas Adicionales
```
IDE utilizado: Pycharm
Version de Python: 3.8.10
Libreria para pruebas unitarias (tests): Pytest
Generador de documentacion: Pycco
```