from django.test import TestCase
from Proyecto.models import *
from django.contrib.auth.models import Permission, User, GroupManager
import datetime


class modeloProyectoTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Crear Usuario de prueba
        Usuario = User.objects.create(username='usu', first_name='Usuario', last_name='Prueba',
                                      email='correodePrueba@gmail.com')
        # Crear Proyecto de Prueba
        Proyectos.objects.create(nombre='ProyectoDePrueba', fechaInicio="2021-09-19", scrumMaster=Usuario)
        # Crear Rol de Prueba
        Roles.objects.create(nombre='Rol de prueba', descripcion='Descripcion Rol',
                             idProyecto=Proyectos.objects.get(id=1))
        # Crear Sprint de prueba
        Sprint.objects.create(idProyecto=Proyectos.objects.get(id=1), numero=1)
        # Crear Miembro de prueba
        miembros.objects.create(idUser=User.objects.get(id=1), idRol=Roles.objects.get(id=1),
                                idProyecto=Proyectos.objects.get(id=1))
        # Crear UserSTory
        userStory.objects.create(idProyecto=Proyectos.objects.get(id=1), titulo='Us de Prueba',
                                 encargado=User.objects.get(id=1))
        # Crear Planning Poker
        PlanningPoker.objects.create(idUs=userStory.objects.get(id=1), idSprint=Sprint.objects.get(id=1),
                                     miembroEncargado=miembros.objects.get(id=1),
                                     estimacionSM=10, prioridad=1)

        # Crear una Activida
        ActividadUs.objects.create(detalle="detalle...", duracion=10.0, idMiembro=miembros.objects.get(id=1),
                                   idUs=userStory.objects.get(id=1), idSprint=Sprint.objects.get(id=1))

        HistorialModificacion.objects.create(detalle="detalle modificacion...", idMiembro=miembros.objects.get(id=1),
                                             idUs=userStory.objects.get(id=1), fecha=datetime.datetime.now(),
                                             hora=datetime.datetime.now().time())

    # Prueba de __str__() definidos en Proyecto.models

    def test_User_str(self):
        u = User.objects.get(id=1)
        self.assertEquals(str(u), 'Usuario Prueba | correodePrueba@gmail.com')

    def test_Proyectos_str(self):
        p = Proyectos.objects.get(id=1)
        self.assertEquals(str(p), p.nombre)

    def test_Roles_str(self):
        r = Roles.objects.get(id=1)
        self.assertEquals(str(r), r.nombre)

    def test_Sprint_str(self):
        s = Sprint.objects.get(id=1)
        self.assertEquals(str(s), str(s.numero))

    # Prueba de verificacion de valores por default
    def test_default_value(self):
        # Para proyectos
        p = Proyectos.objects.get(id=1)
        self.assertEquals(p.estado, 'P')
        # Para Roles
        r = Roles.objects.get(id=1)
        self.assertFalse(r.agregarUserStory)
        self.assertFalse(r.modificarUserStory)
        self.assertFalse(r.eliminarUserStory)
        # Para Sprint
        s = Sprint.objects.get(id=1)
        self.assertEquals(s.estado, 'P')
        # Para miembro
        m = miembros.objects.get(id=1)
        self.assertFalse(m.activo)
        self.assertEquals(m.lunes, 0)
        self.assertEquals(m.martes, 0)
        self.assertEquals(m.miercoles, 0)
        self.assertEquals(m.jueves, 0)
        self.assertEquals(m.viernes, 0)
        self.assertEquals(m.sabado, 0)
        self.assertEquals(m.domingo, 0)
        # Para userStory
        us = userStory.objects.get(id=1)
        self.assertEquals(us.estado, 'N')
        # self.assertEquals(us.fechaIngreso, date.today)
        self.assertEquals(us.tiempoDedicado, 0)
        # Para planning Poker
        us = PlanningPoker.objects.get(id=1)
        # Para Atividades
        actividad = ActividadUs.objects.get(id=1)
        self.assertEquals(actividad.fecha, datetime.date.today())
        # self.assertEquals(actividad.hora, datetime.time())

    # Prueba de verificacion de que trae bien los datos de Proyecto
    def test_get_Proyecto(self):
        p = Proyectos.objects.get(id=1)
        self.assertEquals(p.nombre, 'ProyectoDePrueba')
        self.assertEquals(p.fechaInicio, datetime.date(2021, 9, 19))

    # Prueba de verificacion de que trae bien los datos de roles
    def test_get_Roles(self):
        p = Proyectos.objects.get(id=1)
        r = Roles.objects.get(id=1)
        self.assertEquals(r.nombre, 'Rol de prueba')
        self.assertEquals(r.descripcion, 'Descripcion Rol')
        self.assertEquals(r.idProyecto, p)

    # Prueba de verificacion de que trae bien los datos de Sprint
    def test_get_Sprint(self):
        p = Proyectos.objects.get(id=1)
        s = Sprint.objects.get(id=1)
        self.assertEquals(s.numero, 1)
        self.assertEquals(s.idProyecto, p)

    # Prueba de verificacion de que trae bien los datos de miembro
    def test_get_Mienbros(self):
        p = Proyectos.objects.get(id=1)
        r = Roles.objects.get(id=1)
        m = miembros.objects.get(id=1)
        self.assertEquals(m.idProyecto, p)
        self.assertEquals(m.idRol, r)

    # Prueba de verificacion de que trae bien los datos de userStory
    def test_get_US(self):
        p = Proyectos.objects.get(id=1)
        m = miembros.objects.get(id=1)
        us = userStory.objects.get(id=1)
        self.assertEquals(us.titulo, 'Us de Prueba')
        self.assertEquals(us.idProyecto, p)
        self.assertEquals(us.encargado, m.idUser)

    # Prueba de verificacion de que trae bien los datos de planning Poker
    def test_get_PP(self):
        us = userStory.objects.get(id=1)
        m = miembros.objects.get(id=1)
        s = Sprint.objects.get(id=1)
        pp = PlanningPoker.objects.get(id=1)
        self.assertEquals(pp.estimacionSM, 10)
        self.assertEquals(pp.prioridad, 1)
        self.assertEquals(pp.idUs, us)
        self.assertEquals(pp.idSprint, s)
        self.assertEquals(pp.miembroEncargado, m)

    # Prueba de verificacion de que trae bien los datos de Atividades
    def test_get_ActividadUS(self):
        us = userStory.objects.get(id=1)
        m = miembros.objects.get(id=1)
        s = Sprint.objects.get(id=1)
        actividad = ActividadUs.objects.get(id=1)
        self.assertEquals(actividad.detalle, "detalle...")
        self.assertEquals(actividad.duracion, 10.0)
        self.assertEquals(actividad.idSprint, s)
        self.assertEquals(actividad.idMiembro, m)

    # Prueba de verificacion de que trae bien los datos de Historial de modificaciones
    def test_get_Historial(self):
        us = userStory.objects.get(id=1)
        m = miembros.objects.get(id=1)
        historial = HistorialModificacion.objects.get(id=1)
        self.assertEquals(historial.detalle, "detalle modificacion...")
        self.assertEquals(historial.idUs, us)
        self.assertEquals(historial.idMiembro, m)

