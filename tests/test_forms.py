from django.test import TestCase

# Create your tests here.

import datetime
from django.utils import timezone
from Proyecto.forms import *
from django.contrib.auth.models import Permission, User, GroupManager


class formularioProyectoTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Crear Usuario de prueba
        Usuario = User.objects.create(username='usu', first_name='Usuario', last_name='Prueba',
                                      email='correodePrueba@gmail.com')
        # Crear Proyecto de Prueba
        Proyectos.objects.create(nombre='ProyectoDePrueba', fechaInicio="2021-09-19", scrumMaster=Usuario)
        # Crear Rol de Prueba
        Roles.objects.create(nombre='Rol de prueba', descripcion='Descripcion Rol',
                             idProyecto=Proyectos.objects.get(id=1))
        # Crear Sprint de prueba
        Sprint.objects.create(idProyecto=Proyectos.objects.get(id=1), numero=1)
        # Crear Miembro de prueba
        miembros.objects.create(idUser=User.objects.get(id=1), idRol=Roles.objects.get(id=1),
                                idProyecto=Proyectos.objects.get(id=1))
        # Crear UserSTory
        userStory.objects.create(idProyecto=Proyectos.objects.get(id=1), titulo='Us de Prueba',
                                 encargado=User.objects.get(id=1))
        # Crear Planning Poker
        PlanningPoker.objects.create(idUs=userStory.objects.get(id=1), idSprint=Sprint.objects.get(id=1),
                                     miembroEncargado=miembros.objects.get(id=1),
                                     estimacionSM=10, prioridad=1)

    # Verifica titulo de los campos de FormularioUs
    def test_FormularioUs_label(self):
        form = FormularioUs()
        self.assertTrue(form.fields['idProyecto'].label == 'Proyecto')
        self.assertTrue(form.fields['titulo'].label == 'Titulo')
        self.assertTrue(form.fields['descripcion'].label == 'Descripcion')
        self.assertTrue(form.fields['estimacion'].label == 'Estimacion')
        self.assertTrue(form.fields['prioridad'].label == 'Prioridad')
        self.assertTrue(form.fields['encargado'].label == 'Encargado')
        self.assertTrue(form.fields['fechaIngreso'].label == 'Fecha de Ingreso')

    # Verifica Validacion de los campos de FormularioUs
    def test_FormularioUs_validacion(self):
        form_data = {'idProyecto': Proyectos.objects.get(id=1),
                     'titulo': 'Us de prueba',
                     'descripcion': 'Descripcion Us',
                     'estimacion': 10,
                     'prioridad': 2,
                     'encargado': User.objects.get(id=1),
                     'fechaIngreso': datetime.date.today()}
        form = FormularioUs(data=form_data)
        self.assertTrue(form.is_valid())

    # Verifica titulo de los campos de FormularioUs
    def test_FormularioEditUs_label(self):
        form = FormularioUs()
        self.assertTrue(form.fields['idProyecto'].label == 'Proyecto')
        self.assertTrue(form.fields['titulo'].label == 'Titulo')
        self.assertTrue(form.fields['descripcion'].label == 'Descripcion')
        self.assertTrue(form.fields['estimacion'].label == 'Estimacion')
        self.assertTrue(form.fields['prioridad'].label == 'Prioridad')
        self.assertTrue(form.fields['encargado'].label == 'Encargado')
        self.assertTrue(form.fields['fechaIngreso'].label == 'Fecha de Ingreso')

    # Verifica titulo de los campos de FormularioMiembros
    def test_FormularioMiembros_label(self):
        form = FormularioMiembros()
        self.assertTrue(form.fields['idUser'].label == 'Usuario')
        self.assertTrue(form.fields['idProyecto'].label == 'Proyecto')
        self.assertTrue(form.fields['idRol'].label == 'Rol')
        self.assertTrue(form.fields['activo'].label == 'Activo')
        self.assertTrue(form.fields['lunes'].label == 'Lunes')
        self.assertTrue(form.fields['martes'].label == 'Martes')
        self.assertTrue(form.fields['miercoles'].label == 'Miercoles')
        self.assertTrue(form.fields['jueves'].label == 'Jueves')
        self.assertTrue(form.fields['viernes'].label == 'Viernes')
        self.assertTrue(form.fields['sabado'].label == 'Sabado')
        self.assertTrue(form.fields['domingo'].label == 'Domingo')

    # Verifica Validacion de los campos de FormularioMiembros
    def test_FormularioMiembros_validacion(self):
        form_data = {'idUser': User.objects.get(id=1),
                     'idProyecto': Proyectos.objects.get(id=1),
                     'idRol': Roles.objects.get(id=1),
                     'activo': True,
                     'lunes': 8,
                     'martes': 8,
                     'miercoles': 8,
                     'jueves': 8,
                     'viernes': 8,
                     'sabado': 0,
                     'domingo': 0}
        form = FormularioMiembros(data=form_data)
        self.assertTrue(form.is_valid())

    # Verifica titulo de los campos de guardarEstimacion
    def test_guardarEstimacion_label(self):
        form = guardarEstimacion()
        self.assertTrue(form.fields['estimacionEncargado'].label == '')

    # Verifica Validacion de los campos de guardarEstimacion
    def test_guardarEstimacion_validacion(self):
        form_data = {'estimacionSM': 10,
                     'estimacionFinal': 10,
                     'miembroEncargado': miembros.objects.get(id=1),
                     'idUs': userStory.objects.get(id=1),
                     'idSprint': Sprint.objects.get(id=1),
                     'estimacionEncargado': 10,
                     'prioridad': 5,
                     'estado': 'PP'}
        form = guardarEstimacion(data=form_data)
        self.assertTrue(form.is_valid())

    # Verifica titulo de los campos de FormularioNuevoSprint
    def test_FormularioNuevoSprint_label(self):
        form = FormularioNuevoSprint()
        self.assertTrue(form.fields['idProyecto'].label == 'ID Proyecto')
        self.assertTrue(form.fields['numero'].label == 'Numero de Sprint')
        self.assertTrue(form.fields['fechaInicio'].label == 'Fecha de Inicio')
        self.assertTrue(form.fields['fechaFin'].label == 'Fecha de Fin')

    # Verifica titulo de los campos de formCrearPlanningPoker
    def test_formCrearPlanningPoker_label(self):
        form = formCrearPlanningPoker()
        self.assertTrue(form.fields['prioridad'].label == 'Prioridad')
        self.assertTrue(form.fields['estimacionSM'].label == 'Estimacion del SM')
        self.assertTrue(form.fields['estimacionEncargado'].label == 'Estimacion del Encargado')
        self.assertTrue(form.fields['estimacionFinal'].label == 'Estimacion Final')
        self.assertTrue(form.fields['miembroEncargado'].label == 'Encargado')
        self.assertTrue(form.fields['idUs'].label == 'ID Us')
        self.assertTrue(form.fields['idSprint'].label == 'ID Sprint')

    # Verifica titulo de los campos de FormAlterarFechaSprint
    def test_FormAlterarFechaSprint_label(self):
        form = FormAlterarFechaSprint()
        self.assertTrue(form.fields['idProyecto'].label == 'ID Proyecto')
        self.assertTrue(form.fields['numero'].label == 'Numero de Sprint')
        self.assertTrue(form.fields['fechaInicio'].label == 'Fecha de Inicio')
        self.assertTrue(form.fields['fechaFin'].label == 'Fecha de Fin')
        self.assertTrue(form.fields['estado'].label == 'Estado')

    # Verifica Validacion de los campos de FormAlterarFechaSprint
    def test_FormAlterarFechaSprint_validacion(self):
        form_data = {'idProyecto': Proyectos.objects.get(id=1),
                     'numero': 2,
                     'fechaInicio': '2021-10-10',
                     'fechaFin': '2021-10-20',
                     'estado': 'P'}
        form = FormAlterarFechaSprint(data=form_data)
        self.assertTrue(form.is_valid())

    # Verifica titulo de los campos de FormActividadNueva
    def test_FormActividadNueva(self):
        form = FormActividadNueva()
        self.assertTrue(form.fields['fecha'].label == 'Fecha')
        # self.assertTrue(form.fields['hora'].label == 'Hora')
        self.assertTrue(form.fields['detalle'].label == 'Detalle')
        self.assertTrue(form.fields['duracion'].label == 'Duracion')
        self.assertTrue(form.fields['idMiembro'].label == 'Miembro')
        self.assertTrue(form.fields['idUs'].label == 'User Story')
        self.assertTrue(form.fields['idSprint'].label == 'Sprint')
