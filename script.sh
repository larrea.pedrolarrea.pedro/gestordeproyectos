#!/bin/bash

# Ruta para clonar
# SETTEAR LUEGO DEL TEST
RUTA='/home/'$USERNAME
NOMBRECARPETA='produccion' # camciar a produccion
RAMA='produccion' # cambiar a produccion

# ruta de la capeta
RUTACARPETA=$RUTA'/'$NOMBRECARPETA

# Borramos la carpeta
rm -r -f $RUTACARPETA
cd $RUTA
mkdir $NOMBRECARPETA
cd $RUTACARPETA


# Clonamos la rama de Produccion
git clone -b $RAMA https://gitlab.com/larrea.pedrolarrea.pedro/gestordeproyectos.git

# borrar la BD
dropdb GestorDeProyectos_Db_Produccion

# Crear BD vacio
createdb GestorDeProyectos_Db_Produccion

# poblar bd
psql -h localhost -p 5432  -U postgres GestorDeProyectos_Db_Produccion < /home/$USERNAME/$NOMBRECARPETA/gestordeproyectos/backup.sql

# Creamos entorno Virtual y descargamos la dependencias
cd gestordeproyectos
python3 -m venv venv
source venv/bin/activate
python -m pip install --upgrade pip
python -m pip install -r requirements.txt
deactivate

# Abrimos el proyecto
google-chrome gestordeproyectos.com

