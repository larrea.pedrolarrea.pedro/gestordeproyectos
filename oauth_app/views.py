from django.contrib.sitemaps.views import index
from django.shortcuts import render

# Imports
from allauth.socialaccount.models import SocialAccount
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.models import Permission, User, Group

from Proyecto.forms import guardarEstimacion
from Proyecto.models import miembros, Proyectos, PlanningPoker, Sprint, userStory
from decimal import Decimal, ROUND_HALF_UP
from Proyecto.views import registrarModificacionBase


# vistas para Ingreso y permisos de usuarios del sistema

# Definicion para elegir si mostrar la página principal o el login que llega al SSO


def inicio(request):
    # Verificar si el usuario tiene la sesion activa
    if request.user.is_authenticated:
        # Condicion que verifica si el usuario que ingresa al sistema no tiene un rol asignado
        if not request.user.groups.all():
            # Si ingresa por primera vez se le da el rlo de Nuevo Usuario
            rol = Group.objects.filter(name='Nuevo Usuario').first()
            # SI no existe el rol de nuevo usuario se crea
            if not rol:
                nuevoRol = Group(name='Nuevo Usuario')
                nuevoRol.save()
                # Tambien se crea eñ rol de SUPERADMIN y se le asigna todos los permisos
                superAdmin = Group(name='SUPERADMIN')
                superAdmin.save()
                superAdmin.permissions.add(Permission.objects.get(codename='change_permission'))
                superAdmin.permissions.add(Permission.objects.get(codename='add_proyectos'))
                superAdmin.permissions.add(Permission.objects.get(codename='view_proyectos'))
                superAdmin.permissions.add(Permission.objects.get(codename='add_group'))
                superAdmin.permissions.add(Permission.objects.get(codename='change_group'))
                superAdmin.user_set.add(request.user)
            else:
                # Se asigna el rol 'Nuevo Usuario'
                rol.user_set.add(request.user)
        # Template de la pagina principal del proyecto
        template = loader.get_template('index.html')
    # Si el usuario no tiene la sesion activa entonces mostrar el login
    else:
        # Template de la pagina principal del login
        template = loader.get_template('login.html')
        # Retornamos una respuesta mostrando la pagina html correspondiente
    context = cartasMenuInicio(request)
    return HttpResponse(template.render(context, request))


def cartasMenuInicio(request):
    if request.method == 'POST':
        formGuardarEstimacion = guardarEstimacion(request.POST)
        sprint = Sprint.objects.filter(pk=formGuardarEstimacion['idSprint'].value()).first()
        us = userStory.objects.filter(pk=formGuardarEstimacion['idUs'].value()).first()
        planning = PlanningPoker.objects.filter(idUs=us, idSprint=sprint).first()
        formGuardarEstimacion.instance = planning




        if formGuardarEstimacion.is_valid():
            formGuardarEstimacion.save()
            estimacionEncargado = planning.estimacionEncargado
            estimacionScrumMaster = planning.estimacionSM
            planning.estimacionFinal = Decimal(( estimacionEncargado + estimacionScrumMaster ) / 2 ).quantize(0, ROUND_HALF_UP)
            us.estimacion = Decimal(( estimacionEncargado + estimacionScrumMaster ) / 2 ).quantize(0, ROUND_HALF_UP)

            # registro de modificacion
            registroModificacion = registrarModificacionBase(request, us.idProyecto.id)
            registroModificacion.idUs = us
            registroModificacion.detalle = "Estimacion miembro: %d horas Estimacion Final: %.1f horas" % (estimacionEncargado, (estimacionScrumMaster+estimacionEncargado)/2)
            registroModificacion.save()

            us.save()
            planning.save()

    # Estimaciones
    PPoker = []
    proyecto = []
    formEstimacion = []
    if request.user.is_authenticated:
        # buscamos todos los proyectos donde esta el user
        miembro = miembros.objects.all().filter(idUser=request.user)
        for x in miembro:
            # planning poker en un proyecto x
            pPMiembro = PlanningPoker.objects.all().filter(miembroEncargado=x)
            # si el planning poker existe
            for i in pPMiembro:
                # si aun no se ha cargado una estimacion
                if i and not i.estimacionEncargado:
                    # agregar a la lista de proyectos
                    proyecto.append(x.idProyecto)
                    # agregar a la lista de planning poker
                    PPoker.append(i)
                    formEstimacion.append(guardarEstimacion(instance=i))
    context = {
        'PlanningPoker': PPoker,
        'proyectos': proyecto,
        'formEstimacion': formEstimacion,
    }
    # Tarea
    # agregar aqui el codigo para las tareas
    return context;


# Definicion que despliega la pagina principal de la seccionde 'permisos'
def index(request):
    # lista de usuario del sistema autenticados por el sso
    listaDeUsarios = SocialAccount.objects.order_by('id')
    # Obtener el template del menu principal de permisos
    template = loader.get_template('oauth_app/index.html')
    context = {
        # Definimos la lista de usuarios del sistema para el template
        'listaDeUsarios': listaDeUsarios,
    }
    # Retornamos una respuesta mostrando la pagina html correspondiente
    return HttpResponse(template.render(context, request))


# Definicion que despliega una pagina con la lista de roles
def listaRol(request):
    # lista de roles del sistema
    listaRol = Group.objects.order_by('id')
    # Obtener el template de la lista de roles
    template = loader.get_template('oauth_app/listarRol.html')
    context = {
        # Definimos la lista de roles del sistema para el template
        'listaRol': listaRol,
    }
    # Retornamos una respuesta mostrando la pagina html correspondiente
    return HttpResponse(template.render(context, request))


# Definicion que despliega una pagina para crear un nuevo rol
def crearRol(request):
    # Obtener el template para crear roles
    template = loader.get_template('oauth_app/crearRol.html')
    context = {
    }
    # Retornamos la pagina para crear roles
    return HttpResponse(template.render(context, request))


# Definicion para eliminar un rol
def eliminarRol(request, RolId):
    # Obtenemos un rol en base al id que se recibe como parametro
    rol = Group.objects.get(pk=RolId)
    # Se elimina el rol
    rol.delete()
    # Se llama al def listaRol que despliega la lisra de roles
    return listaRol(request)


# Definicion que despliega una pagina para modifica un rol existente
def modificarRol(request, RolId):
    # Obtenemos un rol en base al id que se recibe como parametro
    rol = Group.objects.get(pk=RolId)
    # Obtenemos la lista de permisos del rol
    permisos = rol.permissions.all().values_list('codename', flat=True)
    # Obtener el template para modificar roles
    template = loader.get_template('oauth_app/editarRol.html')
    context = {
        # Definimos un objeto del tipo rol para el template
        'Rol': rol,
        # Definimos el id del rol el template
        'Rolid': RolId,
        # Definimos la lista de permisos del rol el template
        'Permisos': permisos
    }
    # Retornamos la pagina para modificar roles
    return HttpResponse(template.render(context, request))


# Definicion para guardar el nuevo rol creado
def guardarNuevoRol(request):
    # Se obtiene el nombre del rol desde el formulario
    nombreRol = request.GET.get('nombre', False)
    rol = Group(name=nombreRol)
    # SE crea un nuevo rol
    rol.save()
    # Obtenemos los permisos desde el formulario
    asignarPermisosForm1 = request.GET.get('p1', False)
    asignarPermisosForm2 = request.GET.get('p2', False)
    asignarPermisosForm3 = request.GET.get('p3', False)
    asignarPermisosForm4 = request.GET.get('p4', False)
    asignarPermisosForm5 = request.GET.get('p5', False)
    # se obtienen los permisos del sistema
    permiso1 = Permission.objects.get(codename='change_permission')
    permiso2 = Permission.objects.get(codename='add_proyectos')
    permiso3 = Permission.objects.get(codename='view_proyectos')
    permiso4 = Permission.objects.get(codename='add_group')
    permiso5 = Permission.objects.get(codename='change_group')
    # Si se selecciono el permiso 'Gestionar Permisos' en el formulario
    if asignarPermisosForm1 == 'on':
        # Se agrega el permiso 'Gestionar Permisos' al rol
        rol.permissions.add(permiso1)
    # Si se selecciono el permiso 'Crear Proyecto' en el formulario
    if asignarPermisosForm2 == 'on':
        # Se agrega el permiso 'Crear Proyecto' al rol
        rol.permissions.add(permiso2)
    # Si se selecciono el permiso 'Ver Todos los Proyectos' en el formulario
    if asignarPermisosForm3 == 'on':
        # Se agrega el permiso 'Ver Todos los Proyectos' al rol
        rol.permissions.add(permiso3)
    # Si se selecciono el permiso 'Crear Roles del Sistema' en el formulario
    if asignarPermisosForm4 == 'on':
        # Se agrega el permiso 'Crear Roles del Sistema ' al rol
        rol.permissions.add(permiso4)
    # Si se selecciono el permiso 'Modificar Roles del Sistema' en el formulario
    if asignarPermisosForm5 == 'on':
        # Se agrega el permiso 'Modificar Roles del Sistema' al rol
        rol.permissions.add(permiso5)
    # Se llama al def index que despliega la lista de usuarios
    return index(request)


# Definicion para guardar las modificaciones del rol
def guardarModificacionRol(request, RolId):
    # Obtenemos un rol en base al id que se recibe como parametro
    rol = Group.objects.get(pk=RolId)
    # Obtenemos la lista de permisos del rol
    permisos = rol.permissions.all().values_list('codename', flat=True)
    # Obtenemos los permisos desde el formulario
    asignarPermisosForm1 = request.GET.get('p1', False)
    asignarPermisosForm2 = request.GET.get('p2', False)
    asignarPermisosForm3 = request.GET.get('p3', False)
    asignarPermisosForm4 = request.GET.get('p4', False)
    asignarPermisosForm5 = request.GET.get('p5', False)
    # se obtienen los permisos del sistema
    permiso1 = Permission.objects.get(codename='change_permission')
    permiso2 = Permission.objects.get(codename='add_proyectos')
    permiso3 = Permission.objects.get(codename='view_proyectos')
    permiso4 = Permission.objects.get(codename='add_group')
    permiso5 = Permission.objects.get(codename='change_group')
    # Verificamos si se selecciono el permiso 'Gestionar Permisos' en el formulario
    if asignarPermisosForm1 == 'on':
        # verificamos si el permiso no esta asociado al rol
        if not 'change_permission' in permisos:
            # Se agrega el permiso al rol
            rol.permissions.add(permiso1)
    else:
        if 'change_permission' in permisos:
            # Se elimina el permiso al rol
            rol.permissions.remove(permiso1)
    # Verificamos si se selecciono el permiso 'Crear Proyecto' en el formulario
    if asignarPermisosForm2 == 'on':
        # verificamos si el permiso no esta asociado al rol
        if not 'add_proyectos' in permisos:
            # Se agrega el permiso al rol
            rol.permissions.add(permiso2)
    else:
        if 'add_proyectos' in permisos:
            # Se elimina el permiso al rol
            rol.permissions.remove(permiso2)
    # Verificamos si se selecciono el permiso 'Ver Todos los Proyectos' en el formulario
    if asignarPermisosForm3 == 'on':
        # verificamos si el permiso no esta asociado al rol
        if not 'view_proyectos' in permisos:
            # Se agrega el permiso al rol
            rol.permissions.add(permiso3)
    else:
        if 'view_proyectos' in permisos:
            # Se elimina el permiso al rol
            rol.permissions.remove(permiso3)
    # Verificamos si se selecciono el permiso 'Crear Roles del Sistema' en el formulario
    if asignarPermisosForm4 == 'on':
        # verificamos si el permiso no esta asociado al rol
        if not 'add_group' in permisos:
            # Se agrega el permiso al rol
            rol.permissions.add(permiso4)
    else:
        if 'add_group' in permisos:
            # Se elimina el permiso al rol
            rol.permissions.remove(permiso4)
    # Verificamos si se selecciono el permiso 'Modificar Roles del Sistema' en el formulario
    if asignarPermisosForm5 == 'on':
        # verificamos si el permiso no esta asociado al rol
        if not 'change_group' in permisos:
            # Se agrega el permiso al rol
            rol.permissions.add(permiso5)
    else:
        if 'change_group' in permisos:
            # Se elimina el permiso al rol
            rol.permissions.remove(permiso5)
    # Se retorna al template con la lista de roles
    return listaRol(request)


# Definicion para obtener el template con el formulario para cambiar el rol del usuario
def datosUsuario(request, UsuarioId):
    # obtenemos los datos del usuario obtenidos anteriormente por el sso
    SA = SocialAccount.objects.get(pk=UsuarioId)
    # obtenemos los datos del usuario con el id que se obtiene como parametro
    user = User.objects.get(email__icontains=SA.extra_data['email'])
    # obtenemos los datos del rol del usuario
    userRol = Group.objects.filter(user=user)
    # obtenemos una lista con los roles del sistema
    listaRol = Group.objects.order_by('id')
    # Obtener el template para modificar rol del usuario
    template = loader.get_template('oauth_app/form.html')
    context = {
        'Usuario': SA,
        'Roles': listaRol,
        'Rol': userRol,
    }
    # se retorna el formulario
    return HttpResponse(template.render(context, request))


# Definicion para guardar los datos del usuario
def datosForm(request, UsuarioId):
    # obtenemos los datos del usuario obtenidos anteriormente por el sso
    SA = SocialAccount.objects.get(pk=UsuarioId)
    # obtenemos los datos del usuario con el id que se obtiene como parametro
    user = User.objects.get(email__icontains=SA.extra_data['email'])
    # se obtiene el rol seleccionado
    rolSeleccionado = request.GET.get('seleccionRol')
    rol = Group.objects.get(pk=rolSeleccionado)
    # eliminamos todos los roles del usuario
    user.groups.clear()
    # se agrega al usuario al rol
    rol.user_set.add(user)
    # retornamos el menu principal de permisos
    return index(request)
