from django.urls import path

from oauth_app import views

app_name = 'oauth_app'
urlpatterns = [
    path('', views.index, name='index'),
    path('rol/listar/', views.listaRol, name='listarRol'),
    path('rol/crear/', views.crearRol, name='crearRol'),
    path('rol/crear/guardar', views.guardarNuevoRol, name='guardarRol'),
    path('rol/modificar/<int:RolId>/', views.modificarRol, name='modificarRol'),
    path('rol/modificar/<int:RolId>/guardar/', views.guardarModificacionRol, name='guardarModificacionRol'),
    path('rol/modificar/<int:RolId>/eliminar/', views.eliminarRol, name='eliminarRol'),
    path('<int:UsuarioId>/', views.datosUsuario, name='datosUsuario'),
    path('<int:UsuarioId>/save/', views.datosForm, name='guardar')
]
