#!/bin/bash

RUTAPROYECTO='/home/'$USERNAME'/produccion/gestordeproyectos/' # ruta de la capeta

cd $RUTAPROYECTO

git fetch
git checkout "$2" # Checkout tag

if [ "$1" = "-d" ]; then
	
	BD_NAME="GestorDeProyectos_Db"
	ENTORNO="desarrollo"

	# reset bd
	dropdb $BD_NAME 	# borrar la BD
	createdb $BD_NAME	# Crear BD vacio

	psql -h localhost -p 5432  -U postgres $BD_NAME < "$RUTAPROYECTO""$ENTORNO".sql	 # poblar BD

	# Run Desarrollo
	source venv/bin/activate
	python manage.py runserver
fi

if [ "$1" = "-p" ]; then

	BD_NAME="GestorDeProyectos_Db_Produccion"
	ENTORNO="produccion"

	dropdb $BD_NAME 	# borrar la BD
	createdb $BD_NAME	# Crear BD vacio

	psql -h localhost -p 5432  -U postgres $BD_NAME < "$RUTAPROYECTO""$ENTORNO".sql	 # poblar BD
fi
