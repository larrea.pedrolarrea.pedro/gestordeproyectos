--
-- PostgreSQL database dump
--

-- Dumped from database version 12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)

-- Started on 2021-11-18 22:27:35 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 250 (class 1259 OID 69147)
-- Name: Proyecto_actividadus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto_actividadus" (
    id bigint NOT NULL,
    fecha date NOT NULL,
    detalle character varying(500) NOT NULL,
    duracion double precision NOT NULL,
    "idMiembro_id" bigint NOT NULL,
    "idSprint_id" bigint,
    "idUs_id" bigint NOT NULL
);


ALTER TABLE public."Proyecto_actividadus" OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 69145)
-- Name: Proyecto_actividadus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_actividadus_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_actividadus_id_seq" OWNER TO postgres;

--
-- TOC entry 3380 (class 0 OID 0)
-- Dependencies: 249
-- Name: Proyecto_actividadus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_actividadus_id_seq" OWNED BY public."Proyecto_actividadus".id;


--
-- TOC entry 248 (class 1259 OID 69136)
-- Name: Proyecto_historialmodificacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto_historialmodificacion" (
    id bigint NOT NULL,
    detalle character varying(500) NOT NULL,
    fecha date NOT NULL,
    hora time without time zone NOT NULL,
    "idMiembro_id" bigint NOT NULL,
    "idUs_id" bigint NOT NULL
);


ALTER TABLE public."Proyecto_historialmodificacion" OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 69134)
-- Name: Proyecto_historialmodificacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_historialmodificacion_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_historialmodificacion_id_seq" OWNER TO postgres;

--
-- TOC entry 3381 (class 0 OID 0)
-- Dependencies: 247
-- Name: Proyecto_historialmodificacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_historialmodificacion_id_seq" OWNED BY public."Proyecto_historialmodificacion".id;


--
-- TOC entry 236 (class 1259 OID 69062)
-- Name: Proyecto_miembros; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto_miembros" (
    id bigint NOT NULL,
    activo boolean NOT NULL,
    lunes double precision,
    martes double precision,
    miercoles double precision,
    jueves double precision,
    viernes double precision,
    sabado double precision,
    domingo double precision,
    "idProyecto_id" bigint NOT NULL,
    "idRol_id" bigint,
    "idUser_id" integer NOT NULL
);


ALTER TABLE public."Proyecto_miembros" OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 69060)
-- Name: Proyecto_miembros_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_miembros_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_miembros_id_seq" OWNER TO postgres;

--
-- TOC entry 3382 (class 0 OID 0)
-- Dependencies: 235
-- Name: Proyecto_miembros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_miembros_id_seq" OWNED BY public."Proyecto_miembros".id;


--
-- TOC entry 246 (class 1259 OID 69113)
-- Name: Proyecto_planningpoker; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto_planningpoker" (
    id bigint NOT NULL,
    "estimacionSM" double precision NOT NULL,
    "estimacionEncargado" double precision,
    "estimacionFinal" double precision,
    prioridad integer NOT NULL,
    estado character varying(4) NOT NULL,
    "idSprint_id" bigint NOT NULL,
    "idUs_id" bigint NOT NULL,
    "miembroEncargado_id" bigint NOT NULL
);


ALTER TABLE public."Proyecto_planningpoker" OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 69111)
-- Name: Proyecto_planningpoker_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_planningpoker_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_planningpoker_id_seq" OWNER TO postgres;

--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 245
-- Name: Proyecto_planningpoker_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_planningpoker_id_seq" OWNED BY public."Proyecto_planningpoker".id;


--
-- TOC entry 238 (class 1259 OID 69070)
-- Name: Proyecto_proyectos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto_proyectos" (
    id bigint NOT NULL,
    nombre character varying(150) NOT NULL,
    "fechaInicio" date NOT NULL,
    "fechaFin" date,
    descripcion character varying(450),
    estado character varying(12) NOT NULL,
    "scrumMaster_id" integer NOT NULL
);


ALTER TABLE public."Proyecto_proyectos" OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 69068)
-- Name: Proyecto_proyectos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_proyectos_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_proyectos_id_seq" OWNER TO postgres;

--
-- TOC entry 3384 (class 0 OID 0)
-- Dependencies: 237
-- Name: Proyecto_proyectos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_proyectos_id_seq" OWNED BY public."Proyecto_proyectos".id;


--
-- TOC entry 244 (class 1259 OID 69102)
-- Name: Proyecto_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto_roles" (
    id bigint NOT NULL,
    nombre character varying(250) NOT NULL,
    descripcion character varying(450) NOT NULL,
    "agregarUserStory" boolean NOT NULL,
    "eliminarUserStory" boolean NOT NULL,
    "modificarUserStory" boolean NOT NULL,
    "agregarMiembro" boolean NOT NULL,
    "modificarMiembro" boolean NOT NULL,
    "eliminarMiembro" boolean NOT NULL,
    "crearRol" boolean NOT NULL,
    "modificarRol" boolean NOT NULL,
    "eliminarRol" boolean NOT NULL,
    "crearSprint" boolean NOT NULL,
    "empezarSprint" boolean NOT NULL,
    "finalizarSprint" boolean NOT NULL,
    "agregarSprintBacklog" boolean NOT NULL,
    "modificarSprintBacklog" boolean NOT NULL,
    "idProyecto_id" bigint NOT NULL
);


ALTER TABLE public."Proyecto_roles" OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 69100)
-- Name: Proyecto_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_roles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_roles_id_seq" OWNER TO postgres;

--
-- TOC entry 3385 (class 0 OID 0)
-- Dependencies: 243
-- Name: Proyecto_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_roles_id_seq" OWNED BY public."Proyecto_roles".id;


--
-- TOC entry 242 (class 1259 OID 69094)
-- Name: Proyecto_sprint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto_sprint" (
    id bigint NOT NULL,
    numero integer NOT NULL,
    "fechaInicio" date,
    "fechaFin" date,
    estado character varying(1) NOT NULL,
    "idProyecto_id" bigint NOT NULL
);


ALTER TABLE public."Proyecto_sprint" OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 69092)
-- Name: Proyecto_sprint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_sprint_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_sprint_id_seq" OWNER TO postgres;

--
-- TOC entry 3386 (class 0 OID 0)
-- Dependencies: 241
-- Name: Proyecto_sprint_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_sprint_id_seq" OWNED BY public."Proyecto_sprint".id;


--
-- TOC entry 240 (class 1259 OID 69083)
-- Name: Proyecto_userstory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proyecto_userstory" (
    id bigint NOT NULL,
    titulo character varying(150) NOT NULL,
    descripcion character varying(450),
    estado character varying(4) NOT NULL,
    "fechaIngreso" date NOT NULL,
    estimacion double precision,
    "tiempoDedicado" double precision NOT NULL,
    prioridad integer,
    encargado_id integer,
    "idProyecto_id" bigint NOT NULL
);


ALTER TABLE public."Proyecto_userstory" OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 69081)
-- Name: Proyecto_userstory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Proyecto_userstory_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Proyecto_userstory_id_seq" OWNER TO postgres;

--
-- TOC entry 3387 (class 0 OID 0)
-- Dependencies: 239
-- Name: Proyecto_userstory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Proyecto_userstory_id_seq" OWNED BY public."Proyecto_userstory".id;


--
-- TOC entry 202 (class 1259 OID 68504)
-- Name: account_emailaddress; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account_emailaddress (
    id bigint NOT NULL,
    email character varying(254) NOT NULL,
    verified boolean NOT NULL,
    "primary" boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.account_emailaddress OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 68507)
-- Name: account_emailaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_emailaddress_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailaddress_id_seq OWNER TO postgres;

--
-- TOC entry 3388 (class 0 OID 0)
-- Dependencies: 203
-- Name: account_emailaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.account_emailaddress_id_seq OWNED BY public.account_emailaddress.id;


--
-- TOC entry 204 (class 1259 OID 68509)
-- Name: account_emailconfirmation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account_emailconfirmation (
    id bigint NOT NULL,
    created timestamp with time zone NOT NULL,
    sent timestamp with time zone,
    key character varying(64) NOT NULL,
    email_address_id bigint NOT NULL
);


ALTER TABLE public.account_emailconfirmation OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 68512)
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_emailconfirmation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailconfirmation_id_seq OWNER TO postgres;

--
-- TOC entry 3389 (class 0 OID 0)
-- Dependencies: 205
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.account_emailconfirmation_id_seq OWNED BY public.account_emailconfirmation.id;


--
-- TOC entry 206 (class 1259 OID 68514)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 68517)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- TOC entry 3390 (class 0 OID 0)
-- Dependencies: 207
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- TOC entry 208 (class 1259 OID 68519)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 68522)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 3391 (class 0 OID 0)
-- Dependencies: 209
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- TOC entry 210 (class 1259 OID 68524)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 68527)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- TOC entry 3392 (class 0 OID 0)
-- Dependencies: 211
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- TOC entry 212 (class 1259 OID 68529)
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 68535)
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_groups (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 68538)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- TOC entry 3393 (class 0 OID 0)
-- Dependencies: 214
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- TOC entry 215 (class 1259 OID 68540)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- TOC entry 3394 (class 0 OID 0)
-- Dependencies: 215
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- TOC entry 216 (class 1259 OID 68542)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_user_permissions (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 68545)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- TOC entry 3395 (class 0 OID 0)
-- Dependencies: 217
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- TOC entry 218 (class 1259 OID 68547)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 68554)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- TOC entry 3396 (class 0 OID 0)
-- Dependencies: 219
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- TOC entry 220 (class 1259 OID 68556)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 68559)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- TOC entry 3397 (class 0 OID 0)
-- Dependencies: 221
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- TOC entry 222 (class 1259 OID 68561)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 68567)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- TOC entry 3398 (class 0 OID 0)
-- Dependencies: 223
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- TOC entry 224 (class 1259 OID 68569)
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 68575)
-- Name: django_site; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 68578)
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO postgres;

--
-- TOC entry 3399 (class 0 OID 0)
-- Dependencies: 226
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- TOC entry 227 (class 1259 OID 68580)
-- Name: socialaccount_socialaccount; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socialaccount_socialaccount (
    id bigint NOT NULL,
    provider character varying(30) NOT NULL,
    uid character varying(191) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialaccount OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 68586)
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.socialaccount_socialaccount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialaccount_id_seq OWNER TO postgres;

--
-- TOC entry 3400 (class 0 OID 0)
-- Dependencies: 228
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.socialaccount_socialaccount_id_seq OWNED BY public.socialaccount_socialaccount.id;


--
-- TOC entry 229 (class 1259 OID 68588)
-- Name: socialaccount_socialapp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socialaccount_socialapp (
    id bigint NOT NULL,
    provider character varying(30) NOT NULL,
    name character varying(40) NOT NULL,
    client_id character varying(191) NOT NULL,
    secret character varying(191) NOT NULL,
    key character varying(191) NOT NULL
);


ALTER TABLE public.socialaccount_socialapp OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 68594)
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.socialaccount_socialapp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_id_seq OWNER TO postgres;

--
-- TOC entry 3401 (class 0 OID 0)
-- Dependencies: 230
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.socialaccount_socialapp_id_seq OWNED BY public.socialaccount_socialapp.id;


--
-- TOC entry 231 (class 1259 OID 68596)
-- Name: socialaccount_socialapp_sites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socialaccount_socialapp_sites (
    id bigint NOT NULL,
    socialapp_id bigint NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialapp_sites OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 68599)
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.socialaccount_socialapp_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_sites_id_seq OWNER TO postgres;

--
-- TOC entry 3402 (class 0 OID 0)
-- Dependencies: 232
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.socialaccount_socialapp_sites_id_seq OWNED BY public.socialaccount_socialapp_sites.id;


--
-- TOC entry 233 (class 1259 OID 68601)
-- Name: socialaccount_socialtoken; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.socialaccount_socialtoken (
    id bigint NOT NULL,
    token text NOT NULL,
    token_secret text NOT NULL,
    expires_at timestamp with time zone,
    account_id bigint NOT NULL,
    app_id bigint NOT NULL
);


ALTER TABLE public.socialaccount_socialtoken OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 68607)
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.socialaccount_socialtoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialtoken_id_seq OWNER TO postgres;

--
-- TOC entry 3403 (class 0 OID 0)
-- Dependencies: 234
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.socialaccount_socialtoken_id_seq OWNED BY public.socialaccount_socialtoken.id;


--
-- TOC entry 3050 (class 2604 OID 69150)
-- Name: Proyecto_actividadus id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_actividadus" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_actividadus_id_seq"'::regclass);


--
-- TOC entry 3049 (class 2604 OID 69139)
-- Name: Proyecto_historialmodificacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_historialmodificacion" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_historialmodificacion_id_seq"'::regclass);


--
-- TOC entry 3043 (class 2604 OID 69065)
-- Name: Proyecto_miembros id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_miembros" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_miembros_id_seq"'::regclass);


--
-- TOC entry 3048 (class 2604 OID 69116)
-- Name: Proyecto_planningpoker id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_planningpoker" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_planningpoker_id_seq"'::regclass);


--
-- TOC entry 3044 (class 2604 OID 69073)
-- Name: Proyecto_proyectos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_proyectos" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_proyectos_id_seq"'::regclass);


--
-- TOC entry 3047 (class 2604 OID 69105)
-- Name: Proyecto_roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_roles" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_roles_id_seq"'::regclass);


--
-- TOC entry 3046 (class 2604 OID 69097)
-- Name: Proyecto_sprint id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_sprint" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_sprint_id_seq"'::regclass);


--
-- TOC entry 3045 (class 2604 OID 69086)
-- Name: Proyecto_userstory id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_userstory" ALTER COLUMN id SET DEFAULT nextval('public."Proyecto_userstory_id_seq"'::regclass);


--
-- TOC entry 3026 (class 2604 OID 68617)
-- Name: account_emailaddress id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailaddress ALTER COLUMN id SET DEFAULT nextval('public.account_emailaddress_id_seq'::regclass);


--
-- TOC entry 3027 (class 2604 OID 68618)
-- Name: account_emailconfirmation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailconfirmation ALTER COLUMN id SET DEFAULT nextval('public.account_emailconfirmation_id_seq'::regclass);


--
-- TOC entry 3028 (class 2604 OID 68619)
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- TOC entry 3029 (class 2604 OID 68620)
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 3030 (class 2604 OID 68621)
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- TOC entry 3031 (class 2604 OID 68622)
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- TOC entry 3032 (class 2604 OID 68623)
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- TOC entry 3033 (class 2604 OID 68624)
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 3034 (class 2604 OID 68625)
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- TOC entry 3036 (class 2604 OID 68626)
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- TOC entry 3037 (class 2604 OID 68627)
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- TOC entry 3038 (class 2604 OID 68628)
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- TOC entry 3039 (class 2604 OID 68629)
-- Name: socialaccount_socialaccount id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialaccount ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialaccount_id_seq'::regclass);


--
-- TOC entry 3040 (class 2604 OID 68630)
-- Name: socialaccount_socialapp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_id_seq'::regclass);


--
-- TOC entry 3041 (class 2604 OID 68631)
-- Name: socialaccount_socialapp_sites id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_sites_id_seq'::regclass);


--
-- TOC entry 3042 (class 2604 OID 68632)
-- Name: socialaccount_socialtoken id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialtoken_id_seq'::regclass);


--
-- TOC entry 3374 (class 0 OID 69147)
-- Dependencies: 250
-- Data for Name: Proyecto_actividadus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto_actividadus" (id, fecha, detalle, duracion, "idMiembro_id", "idSprint_id", "idUs_id") FROM stdin;
1	2021-11-22	Se agrega el boton necesario y se valida	2	2	1	1
3	2021-11-24	Se agregan estilos	2	2	1	1
2	2021-11-25	Devolver los pagos recientes y mostrarlos en una tabla	8	2	1	1
4	2021-11-23	Se arma el query que devuelve los datos	3	2	1	3
6	2021-11-24	Se cambian estilos	1	2	1	3
5	2021-11-26	Se muestra correctamente	5.5	2	1	3
7	2021-11-25	Se envian correos	6.5	2	1	4
9	2021-11-26	Se termino de desarrollar	8.5	3	1	2
10	2021-11-26	US_aprobada_actividad	0	2	1	1
11	2021-11-26	US_aprobada_actividad	0	3	1	2
12	2021-11-26	US_aprobada_actividad	0	2	1	3
13	2021-11-26	US_aprobada_actividad	0	2	1	4
8	2021-11-22	Se traen pagos pendientes	8	3	1	2
15	2021-12-06	Se desarrolla	6	6	3	7
14	2021-12-08	Se elije la libreria por usar y se instala	5	6	3	7
16	2021-12-11	US_aprobada_actividad	0	6	3	7
17	2021-12-09	Analisis de la BD	3	9	3	8
18	2021-12-10	Maquetacion	6	9	3	8
19	2021-12-11	Desarrollo y despliegue	3	9	3	8
20	2021-12-11	US_aprobada_actividad	0	9	3	8
21	2022-01-05	Cambios	6	13	6	11
22	2022-01-06	US_aprobada_actividad	0	13	6	11
\.


--
-- TOC entry 3372 (class 0 OID 69136)
-- Dependencies: 248
-- Data for Name: Proyecto_historialmodificacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto_historialmodificacion" (id, detalle, fecha, hora, "idMiembro_id", "idUs_id") FROM stdin;
1	Ingresa a planificacion Sprint 1	2021-11-19	00:24:03.450513	1	1
2	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	00:24:03.456631	1	1
3	Prioridad: 2 Estimacion SM: 10 horasEncargado: andres heo kim	2021-11-19	00:24:03.457737	1	1
4	Estimacion miembro: 13 horas Estimacion Final: 11.5 horas	2021-11-19	00:24:31.561431	2	1
5	Estimacion miembro: 13 horas Estimacion Final: 11.5 horas	2021-11-19	00:24:34.569452	2	1
6	Ingresa a planificacion Sprint 1	2021-11-19	00:30:08.731277	1	2
7	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	00:30:08.736112	1	2
8	Prioridad: 3 Estimacion SM: 14 horasEstimacion miembro: 16Encargado: andres heo kim	2021-11-19	00:30:08.737259	1	2
9	Estimacion final: 15.0 horas	2021-11-19	00:30:08.746809	1	2
10	Ingresa a planificacion Sprint 1	2021-11-19	00:30:42.686291	1	3
11	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	00:30:42.691073	1	3
12	Prioridad: 4 Estimacion SM: 9 horasEstimacion miembro: 8Encargado: andres heo kim	2021-11-19	00:30:42.692129	1	3
13	Estimacion final: 8.5 horas	2021-11-19	00:30:42.700463	1	3
14	Ingresa a planificacion Sprint 1	2021-11-19	00:31:01.310623	1	4
15	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	00:31:01.3153	1	4
16	Prioridad: 5 Estimacion SM: 6 horasEstimacion miembro: 7Encargado: andres heo kim	2021-11-19	00:31:01.316326	1	4
17	Estimacion final: 6.5 horas	2021-11-19	00:31:01.325479	1	4
18	ingreso al sprint numero 1	2021-11-19	00:32:08.242219	1	1
19	ingreso al sprint numero 1	2021-11-19	00:32:08.256726	1	2
20	ingreso al sprint numero 1	2021-11-19	00:32:08.267831	1	3
21	ingreso al sprint numero 1	2021-11-19	00:32:08.278048	1	4
22	Cambio de Estado: Pendiente --> En Proceso	2021-11-19	00:33:05.272697	2	1
23	Cambio de Estado: En Proceso --> Hecho	2021-11-19	00:33:08.839232	2	1
24	Cambio de Estado: Hecho --> En Proceso	2021-11-19	00:40:15.643515	2	1
25	Cambio de Estado: En Proceso --> Hecho	2021-11-19	00:43:13.287787	2	1
26	Cambio de Estado: Pendiente --> En Proceso	2021-11-19	00:43:42.665267	2	3
27	Cambio de Estado: En Proceso --> Hecho	2021-11-19	00:47:06.695218	2	3
28	Cambio de Estado: Pendiente --> En Proceso	2021-11-19	00:47:15.903977	2	4
29	Cambio de Estado: En Proceso --> Hecho	2021-11-19	00:48:21.950024	2	4
30	Cambio de Estado: Pendiente --> En Proceso	2021-11-19	00:48:38.952965	3	2
31	Cambio de Estado: En Proceso --> Hecho	2021-11-19	00:50:21.634242	3	2
32	Cambio de Estado: Hecho --> Aprobado	2021-11-19	00:50:42.783101	1	1
33	Cambio de Estado: Hecho --> Aprobado	2021-11-19	00:50:47.221583	1	2
34	Cambio de Estado: Hecho --> Aprobado	2021-11-19	00:50:51.057626	1	3
35	Cambio de Estado: Hecho --> Aprobado	2021-11-19	00:50:55.000375	1	4
36	Ingresa a planificacion Sprint 2	2021-11-19	00:56:43.6949	1	6
37	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	00:56:43.698604	1	6
38	Prioridad: 1 Estimacion SM: 3 horasEstimacion miembro: 4Encargado: andres heo kim	2021-11-19	00:56:43.700783	1	6
39	Estimacion final: 3.5 horas	2021-11-19	00:56:43.708627	1	6
40	Ingresa a planificacion Sprint 2	2021-11-19	00:57:12.920672	1	5
41	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	00:57:12.92835	1	5
42	Prioridad: 4 Estimacion SM: 6 horasEstimacion miembro: 8Encargado: andres heo kim	2021-11-19	00:57:12.930248	1	5
43	Estimacion final: 7.0 horas	2021-11-19	00:57:12.940391	1	5
44	Sprint 2 Modificacion de Encargado Anterior: andres heo kim  Nuevo: andres heo kim	2021-11-19	00:57:40.913452	1	5
45	Sprint 2 Modificacion de planificacion Prioridad: Anterior: 4  Nueva: 4	2021-11-19	00:57:40.923164	1	5
46	Sprint 2 Modificacion de planificacion Estimacion Final: Anterior: 7.0  Nueva: 7.0	2021-11-19	00:57:40.935404	1	5
47	Ingresa a planificacion Sprint 1	2021-11-19	01:08:38.080708	5	7
48	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	01:08:38.08486	5	7
49	Prioridad: 3 Estimacion SM: 12 horasEstimacion miembro: 11Encargado: andres heo kim	2021-11-19	01:08:38.087616	5	7
50	Estimacion final: 11.5 horas	2021-11-19	01:08:38.095008	5	7
51	Ingresa a planificacion Sprint 1	2021-11-19	01:09:30.474802	5	8
52	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	01:09:30.478015	5	8
53	Prioridad: 3 Estimacion SM: 12 horasEstimacion miembro: 12Encargado: andres heo kim	2021-11-19	01:09:30.480411	5	8
54	Estimacion final: 12.0 horas	2021-11-19	01:09:30.48886	5	8
55	ingreso al sprint numero 1	2021-11-19	01:09:51.254774	5	7
56	ingreso al sprint numero 1	2021-11-19	01:09:51.267264	5	8
57	Cambio de Estado: Pendiente --> En Proceso	2021-11-19	01:10:05.043789	6	7
58	Cambio de Estado: En Proceso --> Hecho	2021-11-19	01:11:33.895457	6	7
59	Cambio de Estado: Hecho --> Aprobado	2021-11-19	01:12:24.043857	5	7
60	Cambio de Estado: Pendiente --> Pendiente	2021-11-19	01:12:26.624457	5	8
61	Cambio de Estado: Pendiente --> En Proceso	2021-11-19	01:12:37.529307	9	8
62	Cambio de Estado: En Proceso --> Hecho	2021-11-19	01:14:27.750562	9	8
63	Cambio de Estado: Hecho --> Aprobado	2021-11-19	01:14:48.854271	5	8
64	Ingresa a planificacion Sprint 2	2021-11-19	01:16:04.044152	5	9
65	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	01:16:04.050218	5	9
66	Prioridad: 4 Estimacion SM: 6 horasEstimacion miembro: 8Encargado: andres heo kim	2021-11-19	01:16:04.052637	5	9
67	Estimacion final: 7.0 horas	2021-11-19	01:16:04.060334	5	9
68	Estado final en el Sprint 1 : Aprobado	2021-11-19	01:16:50.897837	5	7
69	Estado final en el Sprint 1 : Aprobado	2021-11-19	01:16:50.909036	5	8
70	ingreso al sprint numero 2	2021-11-19	01:17:00.239991	5	9
71	Ingresa a planificacion Sprint 3	2021-11-19	01:18:05.193819	5	10
72	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	01:18:05.197953	5	10
73	Prioridad: 4 Estimacion SM: 8 horasEstimacion miembro: 7Encargado: andres heo kim	2021-11-19	01:18:05.200548	5	10
74	Estimacion final: 7.5 horas	2021-11-19	01:18:05.209062	5	10
75	Ingresa a planificacion Sprint 1	2021-11-19	01:23:05.730802	10	11
76	Cambio de Estado: En Planning Poker --> En Planning Poker	2021-11-19	01:23:05.734411	10	11
77	Prioridad: 3 Estimacion SM: 5 horasEstimacion miembro: 6Encargado: andres heo kim	2021-11-19	01:23:05.736628	10	11
78	Estimacion final: 5.5 horas	2021-11-19	01:23:05.744739	10	11
79	ingreso al sprint numero 1	2021-11-19	01:23:15.222035	10	11
80	Cambio de Estado: Pendiente --> En Proceso	2021-11-19	01:23:28.63552	13	11
81	Cambio de Estado: En Proceso --> Hecho	2021-11-19	01:24:23.035037	13	11
82	Cambio de Estado: Hecho --> Aprobado	2021-11-19	01:24:36.054025	10	11
\.


--
-- TOC entry 3360 (class 0 OID 69062)
-- Dependencies: 236
-- Data for Name: Proyecto_miembros; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto_miembros" (id, activo, lunes, martes, miercoles, jueves, viernes, sabado, domingo, "idProyecto_id", "idRol_id", "idUser_id") FROM stdin;
1	t	0	0	0	0	0	0	0	1	1	3
2	t	5	5	5	5	5	0	0	1	4	6
3	t	6	6	6	6	6	0	0	1	3	7
4	t	1	1	1	1	1	1	0	1	5	2
5	t	0	0	0	0	0	0	0	2	6	3
6	t	4	5	5	6	4	0	0	2	7	6
7	t	0	5	0	0	0	0	0	2	11	5
8	t	3	2	5	0	0	0	0	2	10	4
9	t	5	6	6	2	1	0	0	2	7	7
10	t	0	0	0	0	0	0	0	3	13	3
11	t	1	1	1	1	1	0	0	3	14	5
12	t	5	5	5	5	5	5	0	3	15	4
13	t	6	5	5	5	5	0	0	3	16	6
\.


--
-- TOC entry 3370 (class 0 OID 69113)
-- Dependencies: 246
-- Data for Name: Proyecto_planningpoker; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto_planningpoker" (id, "estimacionSM", "estimacionEncargado", "estimacionFinal", prioridad, estado, "idSprint_id", "idUs_id", "miembroEncargado_id") FROM stdin;
1	10	13	12	2	A	1	1	2
2	14	16	15	3	A	1	2	3
3	9	8	8.5	4	A	1	3	2
4	6	7	6.5	5	A	1	4	2
5	3	4	3.5	1	PP	2	6	3
6	6	8	7	4	PP	2	5	2
7	12	11	11.5	3	A	3	7	6
8	12	12	12	3	A	3	8	9
9	6	8	7	4	P	4	9	9
10	8	7	7.5	4	PP	5	10	6
11	5	6	5.5	3	A	6	11	13
\.


--
-- TOC entry 3362 (class 0 OID 69070)
-- Dependencies: 238
-- Data for Name: Proyecto_proyectos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto_proyectos" (id, nombre, "fechaInicio", "fechaFin", descripcion, estado, "scrumMaster_id") FROM stdin;
1	Bancard Pago Movil	2021-11-18	\N	Proyecto para gestionar pagos desde el celular	E	3
2	Biggie	2021-12-01	2021-12-31	Sistema de facturacion y ventas de la tienda Biggie	E	3
3	Forever 21	2022-01-01	2022-01-30	Tienda de ropa	E	3
\.


--
-- TOC entry 3368 (class 0 OID 69102)
-- Dependencies: 244
-- Data for Name: Proyecto_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto_roles" (id, nombre, descripcion, "agregarUserStory", "eliminarUserStory", "modificarUserStory", "agregarMiembro", "modificarMiembro", "eliminarMiembro", "crearRol", "modificarRol", "eliminarRol", "crearSprint", "empezarSprint", "finalizarSprint", "agregarSprintBacklog", "modificarSprintBacklog", "idProyecto_id") FROM stdin;
1	SM_under	rol SM_under creado por debajo al crear un proyecto	t	t	t	t	t	t	t	t	t	t	t	t	t	t	1
2	Administrador de Proyecto	Administra el Proyecto a nivel general, Sprints y Kanban	t	t	t	t	t	t	t	t	t	t	t	t	t	t	1
3	Desarrollador junior	Desarrollador de android	t	f	t	f	f	f	f	f	f	t	f	f	t	t	1
4	Analista	Gestiona la BD	t	t	t	t	t	f	f	t	f	t	f	f	t	t	1
5	Cliente	Cliente de la aplicacion	f	f	f	f	f	f	f	f	f	f	f	f	f	f	1
6	SM_under	rol SM_under creado por debajo al crear un proyecto	t	t	t	t	t	t	t	t	t	t	t	t	t	t	2
7	Administrador de Proyecto	Administra el Proyecto a nivel general, Sprints y Kanban	t	t	t	t	t	t	t	t	t	t	t	t	t	t	2
8	Desarrollador junior	Desarrollador de android	t	f	t	f	f	f	f	f	f	t	f	f	t	t	2
9	Analista	Gestiona la BD	t	t	t	t	t	f	f	t	f	t	f	f	t	t	2
10	Cliente	Cliente de la aplicacion	f	f	f	f	f	f	f	f	f	f	f	f	f	f	2
11	Proveedor	Provee productos y carga datos al sistema	t	f	t	t	t	f	f	f	f	f	f	f	t	f	2
12	Tester	Verifica el desarrollo hecho y busca errores	t	t	t	f	f	f	f	f	f	t	f	f	t	t	2
13	SM_under	rol SM_under creado por debajo al crear un proyecto	t	t	t	t	t	t	t	t	t	t	t	t	t	t	3
14	Cliente	Comprador	f	f	f	f	f	f	f	f	f	f	f	f	f	f	3
15	Vendedor	Vende	f	f	f	f	f	f	f	f	f	f	f	f	f	f	3
16	Desarrollador	Programa	t	t	t	t	t	f	f	f	f	t	t	f	t	t	3
\.


--
-- TOC entry 3366 (class 0 OID 69094)
-- Dependencies: 242
-- Data for Name: Proyecto_sprint; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto_sprint" (id, numero, "fechaInicio", "fechaFin", estado, "idProyecto_id") FROM stdin;
1	1	2021-11-22	2021-11-27	E	1
2	2	2021-11-29	2021-12-04	P	1
3	1	2021-12-06	2021-12-11	T	2
4	2	2021-12-13	2021-12-18	E	2
5	3	2021-12-20	2021-12-25	P	2
6	1	2022-01-03	2022-01-08	E	3
\.


--
-- TOC entry 3364 (class 0 OID 69083)
-- Dependencies: 240
-- Data for Name: Proyecto_userstory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Proyecto_userstory" (id, titulo, descripcion, estado, "fechaIngreso", estimacion, "tiempoDedicado", prioridad, encargado_id, "idProyecto_id") FROM stdin;
6	Bug: Costo total no muestra IVA	Agregar IVA al costo total	PP	2021-11-19	3.5	0	1	7	1
5	Cambio de formato de reporte	Los pdfs no cumplen con el formato deseado por el ministerio	PP	2021-11-19	7	0	4	6	1
7	Crear reportes de ventas	Reporte de ventas del mes	A	2021-11-19	11.5	11	3	6	2
8	Generar diagrama de BD	Generar diagrama de BD	A	2021-11-19	12	12	3	7	2
9	Migrar datos de años anteriores	Para tener mejor control de datos, se deben tener mas datos	P	2021-11-19	7	0	4	7	2
10	Arreglo de errores	Errores varios	PP	2021-11-19	7.5	0	4	6	2
12	Talle de ropa	Al mostrar las ropas se debe mencionar el talle	N	2021-11-19	\N	0	4	\N	3
1	Visualizar pagos recientes	Se debe tener un apartado para visualizar los pagos	A	2021-11-19	12	12	2	6	1
3	Filtrar pagos por mes	Filtro de pagos realizados en la seccion de movimientos.	A	2021-11-19	8.5	9.5	4	6	1
4	Enviar factura correo	Se debe enviar correo para digitalizar pago	A	2021-11-19	6.5	6.5	5	6	1
2	Mostrar los pagos pendientes	Listar los pagos pendientes del cliente, como la essap o ande.	A	2021-11-19	15	16.5	3	7	1
11	Mostrar lista de ropa por categoria	Mostrar categorias como adulto, adolescente, ninho	A	2021-11-19	5.5	6	3	6	3
\.


--
-- TOC entry 3326 (class 0 OID 68504)
-- Dependencies: 202
-- Data for Name: account_emailaddress; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.account_emailaddress (id, email, verified, "primary", user_id) FROM stdin;
1	larrea.pedrolarrea.pedro@fpuna.edu.py	t	t	2
2	andresssshk@fpuna.edu.py	t	t	3
3	denismaldonado69@fpuna.edu.py	t	t	4
4	lauramarielatroche@fpuna.edu.py	t	t	5
5	andresssshk@gmail.com	t	t	6
6	heokimhk@gmail.com	t	t	7
\.


--
-- TOC entry 3328 (class 0 OID 68509)
-- Dependencies: 204
-- Data for Name: account_emailconfirmation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.account_emailconfirmation (id, created, sent, key, email_address_id) FROM stdin;
\.


--
-- TOC entry 3330 (class 0 OID 68514)
-- Dependencies: 206
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
1	SUPERADMIN
2	Administrador
3	Nuevo Usuario
\.


--
-- TOC entry 3332 (class 0 OID 68519)
-- Dependencies: 208
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	9
2	1	10
3	1	6
4	1	49
5	1	52
6	2	6
7	2	49
8	2	52
9	2	9
10	2	10
\.


--
-- TOC entry 3334 (class 0 OID 68524)
-- Dependencies: 210
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add site	7	add_site
26	Can change site	7	change_site
27	Can delete site	7	delete_site
28	Can view site	7	view_site
29	Can add email address	8	add_emailaddress
30	Can change email address	8	change_emailaddress
31	Can delete email address	8	delete_emailaddress
32	Can view email address	8	view_emailaddress
33	Can add email confirmation	9	add_emailconfirmation
34	Can change email confirmation	9	change_emailconfirmation
35	Can delete email confirmation	9	delete_emailconfirmation
36	Can view email confirmation	9	view_emailconfirmation
37	Can add social account	10	add_socialaccount
38	Can change social account	10	change_socialaccount
39	Can delete social account	10	delete_socialaccount
40	Can view social account	10	view_socialaccount
41	Can add social application	11	add_socialapp
42	Can change social application	11	change_socialapp
43	Can delete social application	11	delete_socialapp
44	Can view social application	11	view_socialapp
45	Can add social application token	12	add_socialtoken
46	Can change social application token	12	change_socialtoken
47	Can delete social application token	12	delete_socialtoken
48	Can view social application token	12	view_socialtoken
49	Can add proyectos	13	add_proyectos
50	Can change proyectos	13	change_proyectos
51	Can delete proyectos	13	delete_proyectos
52	Can view proyectos	13	view_proyectos
53	Can add roles	14	add_roles
54	Can change roles	14	change_roles
55	Can delete roles	14	delete_roles
56	Can view roles	14	view_roles
57	Can add miembros	15	add_miembros
58	Can change miembros	15	change_miembros
59	Can delete miembros	15	delete_miembros
60	Can view miembros	15	view_miembros
61	Can add user story	16	add_userstory
62	Can change user story	16	change_userstory
63	Can delete user story	16	delete_userstory
64	Can view user story	16	view_userstory
65	Can add sprint	17	add_sprint
66	Can change sprint	17	change_sprint
67	Can delete sprint	17	delete_sprint
68	Can view sprint	17	view_sprint
\.


--
-- TOC entry 3336 (class 0 OID 68529)
-- Dependencies: 212
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
2	!DdrWj20Lsj5xhHA0I22OqEPhfVm87H8pYJEUolpX	2021-09-02 16:05:51-04	f	pedro_ivan	Pedro Ivan	Larrea Montiel	larrea.pedrolarrea.pedro@fpuna.edu.py	f	t	2021-09-02 16:05:51-04
4	!ehwh25BKV7PNo5NSoJLNNMlxPmL696btXQbnXA3t	2021-09-02 17:35:42-04	f	denis_gabriel	Denis Gabriel	Maldonado Alarcon	denismaldonado69@fpuna.edu.py	f	t	2021-09-02 17:35:42-04
5	!zBXZsMiNEAcMGIiUeCMNqtEEIOL8JgbVkNAz9Uk1	2021-09-02 17:41:00-04	f	laura_mariela	Laura Mariela	Troche Insfran	lauramarielatroche@fpuna.edu.py	f	t	2021-09-02 17:41:00-04
1	pbkdf2_sha256$260000$vNXTofQF9hpKpKGqu1XKOH$2bOkQNISIo1ET+sdopOqwA7pUE7HmqbC17nS84MKleU=	2021-09-02 22:36:47.743617-04	t	admin				t	t	2021-09-02 15:59:14.92102-04
6	!LcMm6wUABtR37PIhNNe1HzYmL84ccHb51ZqVpsGy	2021-11-13 17:39:25.005835-03	f	andres6	andres	heo kim	andresssshk@gmail.com	f	t	2021-09-19 22:08:28.886681-04
3	!OIP7LpNDsrvmVguvx41BDgoC8pwCmthTTnhKBhnv	2021-11-14 18:53:50.023783-03	f	andres	Andres	Heo Kim	andresssshk@fpuna.edu.py	f	t	2021-09-02 16:35:30-04
7	!81dWQ5sTiWko1LP7bETe6FcvTlbJOIUjuMXjpGCG	2021-11-18 16:12:16.321013-03	f	andres8	andres	heo kim	heokimhk@gmail.com	f	t	2021-11-18 16:12:16.287276-03
\.


--
-- TOC entry 3337 (class 0 OID 68535)
-- Dependencies: 213
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	2	1
4	4	1
6	5	1
7	3	1
8	6	3
9	7	3
\.


--
-- TOC entry 3340 (class 0 OID 68542)
-- Dependencies: 216
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- TOC entry 3342 (class 0 OID 68547)
-- Dependencies: 218
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2021-09-02 16:00:07.543647-04	1	OAuth App	1	[{"added": {}}]	11	1
2	2021-09-02 16:00:17.08696-04	4	127.0.0.1:8000	1	[{"added": {}}]	7	1
3	2021-09-02 16:04:15.782705-04	1	OAuth App	2	[{"changed": {"fields": ["Sites"]}}]	11	1
4	2021-09-02 16:11:08.473276-04	1	SUPERADMIN	1	[{"added": {}}]	3	1
5	2021-09-02 16:11:20.910282-04	2	pedro_ivan	2	[{"changed": {"fields": ["Groups"]}}]	4	1
6	2021-09-02 16:15:42.772985-04	1	SUPERADMIN	2	[{"changed": {"fields": ["Permissions"]}}]	3	1
7	2021-09-02 16:20:44.160471-04	3	Nuevo Usuario	2	[{"changed": {"fields": ["Permissions"]}}]	3	1
8	2021-09-02 17:36:46.844635-04	4	denis_gabriel	2	[{"changed": {"fields": ["Groups"]}}]	4	1
9	2021-09-02 17:42:10.457193-04	5	laura_mariela	2	[{"changed": {"fields": ["Groups"]}}]	4	1
10	2021-09-02 19:42:00.477995-04	3	andres	2	[{"changed": {"fields": ["User permissions"]}}]	4	1
11	2021-09-02 22:37:25.349427-04	3	andres	2	[{"changed": {"fields": ["Groups", "User permissions"]}}]	4	1
\.


--
-- TOC entry 3344 (class 0 OID 68556)
-- Dependencies: 220
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	sites	site
8	account	emailaddress
9	account	emailconfirmation
10	socialaccount	socialaccount
11	socialaccount	socialapp
12	socialaccount	socialtoken
13	Proyecto	proyectos
14	Proyecto	roles
15	Proyecto	miembros
16	Proyecto	userstory
17	Proyecto	sprint
\.


--
-- TOC entry 3346 (class 0 OID 68561)
-- Dependencies: 222
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
57	contenttypes	0001_initial	2021-10-11 12:49:15.943968-03
58	auth	0001_initial	2021-10-11 12:49:15.982282-03
\.


--
-- TOC entry 3348 (class 0 OID 68569)
-- Dependencies: 224
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
1gllfrivjk7jr68briaibbr482qclzth	.eJxVjMEOgyAQRP9lz41BBIve6o-QhV0iKcVEoZem_15tvHicNzPvA-j9UnOxb15jiEyWXxgTjLmmdAOLtcy2brzaSDCChAtz6J-cjwJTOnBz6pr_5qy35rEnziV6LHHJ0_m6qGbc5t1DkomxJ2PYC9GaoAlZs2uF0EaJziEPSrkgzdCG3nSalRRKyd6Rvu8Ivj-bKUes:1mLsy7:bJ0rCRlxNkFbwUu1wd8dMimsIzYxX2H-JNWGUtBuNr8	2021-09-16 16:05:51.216611-04
btuotg9hgm0m2tkbokpf5e3hc6mrcvee	.eJxVjE0OwiAYRO_C2jT8FaQ7vQiZ0o-UiDSx4MZ4d1vTTZfzZuZ9GEJYWqn-Ta8UE02enkiZDaXlfGEerc6-rfTyaWIDU-zERoQHlb1AzjvuDl333xz12t22RKWmgJqWcj9eJ9WMdd48RvScRziHaISTMFw7FSVNnJSMo7QkLTeA0NBaWDjVW0FXPTrSznCw7w9l0kbl:1mLtQo:Bx_7B0Nri6Toa2cGsso9TO6VfBEByYiqtDlKEQnlu8o	2021-09-16 16:35:30.781739-04
j06f4oviyjopvmb0h51r5k5fyn0er0tw	.eJxVjMEOgyAQRP9lz40BQXG9tT9CFlgiKcWkQi9N_7218eJx3sy8N5D3ayvVvviZYuJg-UEpw1xazhew1Opi28ZPmwLMoOHEHPk7l72gnHfcHbruvznqrbv-EpeaPNW0ltvxOqkW2pbdQy4qLYSSIWDQiCNqVL0kQscTCtdjb0wctJuUiWIwRBxjmIQco5Sk4fMFeqtHhA:1mLuN4:Zfz5BXddbUQfSQwctF7WaHlIrsTrjv-dNzM01uqdxDY	2021-09-16 17:35:42.693243-04
tdlakjlv0ebyim9aurla80i4ontkavgj	.eJxVjEEOwiAQRe_C2hCBDgWX7nsGMgxTqRpISrsy3l1JutDtf-_9lwi4bznsjdewJHERSpx-t4j04NJBumO5VUm1bOsSZVfkQZucauLn9XD_DjK23GsmDwPOBtyZHRlvUTkeZ1KEFgG1osQjWE-WwKqvm5I2EFk56zUM4v0B-UU35Q:1mLuNn:Wq2Lg8JvGKsV6xW5wDG-NatjeVfVjTuh3cj-TxTNeuw	2021-09-16 17:36:27.825753-04
heyqfeln9e2929ym4stbqi63zu91pirg	.eJxVjMEOgyAQRP-Fc2OUioC3-iNk2V0iKcVEoZem_15tvHicmTfvIwBxqbm4N68xRCbHL4hJjLmmdBMOapld3Xh1kcQolLh0HvDJ-RggpaNuTl3zZ855ax574lwiQolLns7XRTXDNu8eg4F0sJZlsL5VSINBbkl391YOGjsCJa3qgwZJsEM-SGs6S70PvSEcxPcHoW9IiQ:1mLuSC:G4BS6P9VlzKD9Evagw82qW162V-EzCRwFEFlldR-veU	2021-09-16 17:41:00.401136-04
pydr270ofj8kzsb8txwzuwxc017l3zxf	.eJxVjEEOwiAQRe_C2hCBDgWX7nsGMgxTqRpISrsy3l1JutDtf-_9lwi4bznsjdewJHERSpx-t4j04NJBumO5VUm1bOsSZVfkQZucauLn9XD_DjK23GsmDwPOBtyZHRlvUTkeZ1KEFgG1osQjWE-WwKqvm5I2EFk56zUM4v0B-UU35Q:1mLvmG:YS-MvATkVagEVGuMFEA2evb3rpEAy1a4SSVM4RndARQ	2021-09-16 19:05:48.576705-04
tc1pnw18kj3n1xmt8ycybu7mxvegnbtp	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mM0UD:nPq2gSXcXUHzIvz3zW-8xkZfqJkDfbP5SRT7vkI1bXs	2021-09-17 00:07:29.9009-04
7gda9zhygkgsdqz7i3jgs32r1zq0jmb8	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mQdkx:J2cSb4RksXwCll9f4y-vOTTenBtPOfeZehkiCpPpkyg	2021-09-29 18:51:55.998538-04
gj5nq7pr5qtmkgbtnmg2cns24v9ni6jp	.eJxVjMEOwiAQRP-Fs2nAssB6sz_SLFsIxIYmAifjv9uaHvQ4b2beS8zUW5p7Dc85L-ImjLj8Mk_8COUoaF0PPBDz1ksbvpuzrsN9T6G0zNTyVqbz9adKVNPukRohGI_AOHrnrJRXiARKLVoiOQLPqEE5iAFJRjti9JY9mwU0BUTx_gCjdDw1:1mS8jf:LhHeb5TtG280fcPINqxT9RoRzuRUqwbZBI_9ScoxpRA	2021-10-03 23:08:47.936818-03
e2bsy17gfhbpa8wf8v7tnh9cacdihgkk	.eJxVjMEOwiAQRP-Fs2nAssB6sz_SLFsIxIYmAifjv9uaHvQ4b2beS8zUW5p7Dc85L-ImjLj8Mk_8COUoaF0PPBDz1ksbvpuzrsN9T6G0zNTyVqbz9adKVNPukRohGI_AOHrnrJRXiARKLVoiOQLPqEE5iAFJRjti9JY9mwU0BUTx_gCjdDw1:1mSA2y:_aHDcN7JuwMwDnuM_SnyVtC8q8A45ltDOw0kJsxGfGU	2021-10-04 00:32:48.277189-03
duvttj7dwenbf7ircqq6w5nretleetuf	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mYZzu:HIUGdVlw2Y_5FUEioPeqJH-dF2Y1ugC8tkCU39keRds	2021-10-21 17:28:10.574805-03
2qeij53xnj69bds6isl4rfvedrgp4bwb	.eJxVjMEOwiAQRP-Fs2nAssB6sz_SLFsIxIYmAifjv9uaHvQ4b2beS8zUW5p7Dc85L-ImjLj8Mk_8COUoaF0PPBDz1ksbvpuzrsN9T6G0zNTyVqbz9adKVNPukRohGI_AOHrnrJRXiARKLVoiOQLPqEE5iAFJRjti9JY9mwU0BUTx_gCjdDw1:1mZxoI:RAvwAJwbdZeWZklpkSxxmlev--X98cWwHcoQAPUigsw	2021-10-25 13:05:54.475786-03
4o919d6aijh92mnl6es59zzrma8taznx	eyJzb2NpYWxhY2NvdW50X3N0YXRlIjpbeyJwcm9jZXNzIjoibG9naW4iLCJzY29wZSI6IiIsImF1dGhfcGFyYW1zIjoiIn0sIk5VYjJYc2FNMHlPcCJdfQ:1mfoOw:s4PGIRoe4L96c1rnQCbFQ5K3CESP3GGrmGhz1nhvosY	2021-11-10 16:15:54.271242-03
g5s4coo71yor3ip6iunqn71n2e8dfk7d	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mfoQB:0Vzd0hYazpeW4JPhdTjI7zFFIomb7NCJRP1gUnUwkvA	2021-11-10 16:17:11.720482-03
fue09jryzqk0xhsuebzoxtvh0s06eowe	.eJxVjMEOwiAQRP-Fs2nAssB6sz_SLFsIxIYmAifjv9uaHvQ4b2beS8zUW5p7Dc85L-ImjLj8Mk_8COUoaF0PPBDz1ksbvpuzrsN9T6G0zNTyVqbz9adKVNPukRohGI_AOHrnrJRXiARKLVoiOQLPqEE5iAFJRjti9JY9mwU0BUTx_gCjdDw1:1mfot2:YBUAoCu4uX6uqOaSR9VLfJVHMgpIenpUN-P20SmzWnE	2021-11-10 16:47:00.47395-03
nvb6671prsnrrl7a0ltyo69evbmnjc81	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mfw0i:Tp2GUKuheaQtFhYeU6DJ2QXpbR_7DnWfYVj75AHlYP8	2021-11-11 00:23:24.766438-03
m2pn2u4my37zshj7uh3vdehpsw0n1uw2	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mgGmd:p_U2-frYe6IYDmOR1VDctYLDSQO63UDqMxfOcvSy4YY	2021-11-11 22:34:15.308207-03
ldauav30g83ve9v9zacrd1bw7mma0l5i	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mgU7b:iuVZpIl0RLtIrtohPhxIl_Nur8BO6BJejLuko3xA6OI	2021-11-12 12:48:47.626113-03
kn8hl06hk22lmojuyhucy6qc9n547pxh	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mhzQV:dVfVCdX0oK34A-ulVXKxQok879VAmsmtBaVZcn9IsOc	2021-11-16 16:26:31.733931-03
1tqxa3qi0apbifgphjzsykgclzsrjohx	.eJxVjMEOwiAQRP-Fs2nAssB6sz_SLFsIxIYmAifjv9uaHvQ4b2beS8zUW5p7Dc85L-ImjLj8Mk_8COUoaF0PPBDz1ksbvpuzrsN9T6G0zNTyVqbz9adKVNPukRohGI_AOHrnrJRXiARKLVoiOQLPqEE5iAFJRjti9JY9mwU0BUTx_gCjdDw1:1mky7x:niMPoUL5-w9EDKQkbzG5KnJ9c-a-A6_3iFpg__jrLpw	2021-11-24 21:39:41.822324-03
sfxz1iy250xazy7d1etdsxfb4lgyroxj	.eJxVjMEOwiAQRP-Fs2nAssB6sz_SLFsIxIYmAifjv9uaHvQ4b2beS8zUW5p7Dc85L-ImjLj8Mk_8COUoaF0PPBDz1ksbvpuzrsN9T6G0zNTyVqbz9adKVNPukRohGI_AOHrnrJRXiARKLVoiOQLPqEE5iAFJRjti9JY9mwU0BUTx_gCjdDw1:1mlzo5:XSac_HnpmDDDv_aclAZYdnpevYiWsGJY8EExMJdstCU	2021-11-27 17:39:25.010232-03
7068r09nobg5v7pkyjeaeix2xpf0yb8n	.eJxVjMsOgjAURP-la0P6ovW6kx8h03KbNpKSSFkZ_10wLHQ5Z2bOS4zYWh63lZ9jmcRNGHH5ZQHxwfUoMM8H7hDjstXWfTdnvXb3PXFtJaKVpQ7n60-Vsebd41QvZQIRklOk4aQlkzRPko1OQXvWXjpAWVirPMj0XvHVBmJLTkK8P59sO38:1mmNRe:5onUFUiNK3tLlOhah3cjwA22IKvEFnrQ9tFGaRCMlX8	2021-11-28 18:53:50.031633-03
thoxdfsmcc4b2ypatg9mbox487qo259z	.eJxVjDsOwyAQRO9CHVnrHx93yUXQwrIyCsGSDWmi3D125MblvJl5H4HeLzUX-w5r5BjIhhfGJKZcU7oJi7XMtm5htZHEJJS4MIf-GfJRYEoHbk5d89-c9dbc9xRyiR5LXPLjfF1UM27z7mGDDLrVYEABtU5hQDc4MrLrgiLDRADMErqBe-1lL5UZW9IEwwijZ_H9AYzNR7c:1mnmpU:nd0Iv_dBnHLeqq553ZWo7RwfwKjBlzeGcFK6E_aLrFU	2021-12-02 16:12:16.326151-03
\.


--
-- TOC entry 3349 (class 0 OID 68575)
-- Dependencies: 225
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_site (id, domain, name) FROM stdin;
2	example.com	example.com
3	127.0.0.1:8000	127.0.0.1:8000
\.


--
-- TOC entry 3351 (class 0 OID 68580)
-- Dependencies: 227
-- Data for Name: socialaccount_socialaccount; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialaccount (id, provider, uid, last_login, date_joined, extra_data, user_id) FROM stdin;
1	google	117738699638136702283	2021-09-02 16:05:51.157826-04	2021-09-02 16:05:51.157842-04	{"id": "117738699638136702283", "email": "larrea.pedrolarrea.pedro@fpuna.edu.py", "verified_email": true, "name": "Pedro Ivan Larrea Montiel", "given_name": "Pedro Ivan", "family_name": "Larrea Montiel", "picture": "https://lh3.googleusercontent.com/a/AATXAJzegG31mOLFIYMm9QNvd9a_iU46R2Mc_r78VrSw=s96-c", "locale": "es", "hd": "fpuna.edu.py"}	2
3	google	117812745059172188772	2021-09-02 17:35:42.64207-04	2021-09-02 17:35:42.642087-04	{"id": "117812745059172188772", "email": "denismaldonado69@fpuna.edu.py", "verified_email": true, "name": "Denis Gabriel Maldonado Alarcon", "given_name": "Denis Gabriel", "family_name": "Maldonado Alarcon", "picture": "https://lh3.googleusercontent.com/a-/AOh14GgrfZZp0mtT934CefI8k7HEsddiEw_jvf4LnKsb=s96-c", "locale": "es", "hd": "fpuna.edu.py"}	4
4	google	101480759865477050795	2021-09-02 17:41:00.350102-04	2021-09-02 17:41:00.35012-04	{"id": "101480759865477050795", "email": "lauramarielatroche@fpuna.edu.py", "verified_email": true, "name": "Laura Mariela Troche Insfran", "given_name": "Laura Mariela", "family_name": "Troche Insfran", "picture": "https://lh3.googleusercontent.com/a-/AOh14GjIUy4Xe_cMYDGup3CmAsHSV7dbiVzn8KeYNL_4=s96-c", "locale": "es", "hd": "fpuna.edu.py"}	5
5	google	110234187333566165112	2021-11-13 17:39:24.990129-03	2021-09-19 22:08:28.904237-04	{"id": "110234187333566165112", "email": "andresssshk@gmail.com", "verified_email": true, "name": "andres heo kim", "given_name": "andres", "family_name": "heo kim", "picture": "https://lh3.googleusercontent.com/a-/AOh14GitPj7vEWH8Zahua4c5VGW1PLS6GFM-0KyliaquPQ=s96-c", "locale": "es-419"}	6
2	google	113743055098235482541	2021-11-14 18:53:50.005384-03	2021-09-02 16:35:30.665579-04	{"id": "113743055098235482541", "email": "andresssshk@fpuna.edu.py", "verified_email": true, "name": "Andres Heo Kim", "given_name": "Andres", "family_name": "Heo Kim", "picture": "https://lh3.googleusercontent.com/a-/AOh14GjS-nC_c2ieApDnjSyyh8SCjokKW_QIuR-zpl2p=s96-c", "locale": "es", "hd": "fpuna.edu.py"}	3
6	google	115671212394419277499	2021-11-18 16:12:16.300688-03	2021-11-18 16:12:16.300705-03	{"id": "115671212394419277499", "email": "heokimhk@gmail.com", "verified_email": true, "name": "andres heo kim", "given_name": "andres", "family_name": "heo kim", "picture": "https://lh3.googleusercontent.com/a/AATXAJxtahhUdgPIfn85qpvAifhOKlzFInkpu1e7rF90=s96-c", "locale": "es-419"}	7
\.


--
-- TOC entry 3353 (class 0 OID 68588)
-- Dependencies: 229
-- Data for Name: socialaccount_socialapp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialapp (id, provider, name, client_id, secret, key) FROM stdin;
1	google	OAuth App	293464491932-r5u2850sf2391k6ru0uig2rq4m1lugf2.apps.googleusercontent.com	V2iCorNGHxR-eXGAmZHcncFA	
\.


--
-- TOC entry 3355 (class 0 OID 68596)
-- Dependencies: 231
-- Data for Name: socialaccount_socialapp_sites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialapp_sites (id, socialapp_id, site_id) FROM stdin;
1	1	3
\.


--
-- TOC entry 3357 (class 0 OID 68601)
-- Dependencies: 233
-- Data for Name: socialaccount_socialtoken; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialtoken (id, token, token_secret, expires_at, account_id, app_id) FROM stdin;
1	ya29.a0ARrdaM-W1Bf93NhACTwuG93ZPsQFbKH6nt0NB-HJeduxLcr_2y-YW5oNXQlyyvxdlr-1-RVOO7W34lyeKtFcjH_9oDQ5r7ON_iik55WXTgGHJkuVt5fR3E5RkDDIQme7-Qn-2FRfenp_KwobbI1nDHCn9ctwsQ		2021-09-02 17:05:39.843452-04	1	1
3	ya29.a0ARrdaM-Nzcx1PMNNY06VBGsto25ulE1KuYwnfw8UNwNxjstOw7LbkRozel8u-2xBVhj3KuGOD7_mBgQBKCxGNAoyjh_JmvKMUiOcDhYnYAdtteNtdTtu0VlxfytpuOi_FvK9d_d6ckixTTdmfLasStacj-_l		2021-09-02 18:35:41.368962-04	3	1
4	ya29.a0ARrdaM8RdyMzvp3-RPz5cj8Ifx8-1C7u2T0ic3QNKk1L8Msq2Nb154Iqzt89tR30qPSkd_4nl57AxakKQfwjYpHwuyWulbT0a6dS0GJ-CGppfMvAOTaxkc2A4otqSdfJ0BuK_iYaMPqPCwpcNUIezAWbvCj-		2021-09-02 18:40:59.078152-04	4	1
5	ya29.a0ARrdaM_UDFnK_p7GAdjbHdBIUdXXJw1BsE94FjN_-nIHApQWj-4qR5EntYJFnqytCkxIYOebJDYpdsPB2n9gHAdfuXiV00zxhVPFnwBLSn87Z1epabqf7XXYb0-R0iLqTExunLVjKszDqDm9E96AiWI5t7kf		2021-11-13 18:39:23.773674-03	5	1
2	ya29.a0ARrdaM8IlnOupfxXTvCYz6ULe8zvhU5IG7LI6FVJY4uyVJaSGuX8XuAu_IMGasS1eKWnsKzbW71YbCqIadAsMyTvwgfs59PjvawhpJmMXRvBBhqpSVW_PBtG_XKZA69hr2G8-DhAZG-BIrha03tphIT3FQm15NI		2021-11-14 19:53:48.820792-03	2	1
6	ya29.a0ARrdaM-BsdpW8R4Qj6bh2OFI-64UKuZMSChUXUdjIpQjMY7RJj_sg5R6of6p3N8a2agmdJWg9efZQJv7MwNbMhnzqEWUiPFQPQJpEqnrKtNahWEJsV2OS1CLPxxrH2-qTeGdwhjR4wFmvjeSiLVytCxBkAih		2021-11-18 17:12:15.076751-03	6	1
\.


--
-- TOC entry 3404 (class 0 OID 0)
-- Dependencies: 249
-- Name: Proyecto_actividadus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_actividadus_id_seq"', 22, true);


--
-- TOC entry 3405 (class 0 OID 0)
-- Dependencies: 247
-- Name: Proyecto_historialmodificacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_historialmodificacion_id_seq"', 82, true);


--
-- TOC entry 3406 (class 0 OID 0)
-- Dependencies: 235
-- Name: Proyecto_miembros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_miembros_id_seq"', 13, true);


--
-- TOC entry 3407 (class 0 OID 0)
-- Dependencies: 245
-- Name: Proyecto_planningpoker_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_planningpoker_id_seq"', 11, true);


--
-- TOC entry 3408 (class 0 OID 0)
-- Dependencies: 237
-- Name: Proyecto_proyectos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_proyectos_id_seq"', 3, true);


--
-- TOC entry 3409 (class 0 OID 0)
-- Dependencies: 243
-- Name: Proyecto_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_roles_id_seq"', 16, true);


--
-- TOC entry 3410 (class 0 OID 0)
-- Dependencies: 241
-- Name: Proyecto_sprint_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_sprint_id_seq"', 6, true);


--
-- TOC entry 3411 (class 0 OID 0)
-- Dependencies: 239
-- Name: Proyecto_userstory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Proyecto_userstory_id_seq"', 12, true);


--
-- TOC entry 3412 (class 0 OID 0)
-- Dependencies: 203
-- Name: account_emailaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_emailaddress_id_seq', 6, true);


--
-- TOC entry 3413 (class 0 OID 0)
-- Dependencies: 205
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_emailconfirmation_id_seq', 1, false);


--
-- TOC entry 3414 (class 0 OID 0)
-- Dependencies: 207
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 3, true);


--
-- TOC entry 3415 (class 0 OID 0)
-- Dependencies: 209
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 15, true);


--
-- TOC entry 3416 (class 0 OID 0)
-- Dependencies: 211
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 68, true);


--
-- TOC entry 3417 (class 0 OID 0)
-- Dependencies: 214
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 9, true);


--
-- TOC entry 3418 (class 0 OID 0)
-- Dependencies: 215
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 7, true);


--
-- TOC entry 3419 (class 0 OID 0)
-- Dependencies: 217
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, true);


--
-- TOC entry 3420 (class 0 OID 0)
-- Dependencies: 219
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 11, true);


--
-- TOC entry 3421 (class 0 OID 0)
-- Dependencies: 221
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 17, true);


--
-- TOC entry 3422 (class 0 OID 0)
-- Dependencies: 223
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 68, true);


--
-- TOC entry 3423 (class 0 OID 0)
-- Dependencies: 226
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_site_id_seq', 4, true);


--
-- TOC entry 3424 (class 0 OID 0)
-- Dependencies: 228
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialaccount_id_seq', 6, true);


--
-- TOC entry 3425 (class 0 OID 0)
-- Dependencies: 230
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_id_seq', 1, true);


--
-- TOC entry 3426 (class 0 OID 0)
-- Dependencies: 232
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_sites_id_seq', 1, true);


--
-- TOC entry 3427 (class 0 OID 0)
-- Dependencies: 234
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialtoken_id_seq', 6, true);


--
-- TOC entry 3168 (class 2606 OID 69155)
-- Name: Proyecto_actividadus Proyecto_actividadus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_actividadus"
    ADD CONSTRAINT "Proyecto_actividadus_pkey" PRIMARY KEY (id);


--
-- TOC entry 3163 (class 2606 OID 69144)
-- Name: Proyecto_historialmodificacion Proyecto_historialmodificacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_historialmodificacion"
    ADD CONSTRAINT "Proyecto_historialmodificacion_pkey" PRIMARY KEY (id);


--
-- TOC entry 3138 (class 2606 OID 69067)
-- Name: Proyecto_miembros Proyecto_miembros_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_miembros"
    ADD CONSTRAINT "Proyecto_miembros_pkey" PRIMARY KEY (id);


--
-- TOC entry 3159 (class 2606 OID 69118)
-- Name: Proyecto_planningpoker Proyecto_planningpoker_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_planningpoker"
    ADD CONSTRAINT "Proyecto_planningpoker_pkey" PRIMARY KEY (id);


--
-- TOC entry 3141 (class 2606 OID 69080)
-- Name: Proyecto_proyectos Proyecto_proyectos_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_proyectos"
    ADD CONSTRAINT "Proyecto_proyectos_nombre_key" UNIQUE (nombre);


--
-- TOC entry 3143 (class 2606 OID 69078)
-- Name: Proyecto_proyectos Proyecto_proyectos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_proyectos"
    ADD CONSTRAINT "Proyecto_proyectos_pkey" PRIMARY KEY (id);


--
-- TOC entry 3154 (class 2606 OID 69110)
-- Name: Proyecto_roles Proyecto_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_roles"
    ADD CONSTRAINT "Proyecto_roles_pkey" PRIMARY KEY (id);


--
-- TOC entry 3151 (class 2606 OID 69099)
-- Name: Proyecto_sprint Proyecto_sprint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_sprint"
    ADD CONSTRAINT "Proyecto_sprint_pkey" PRIMARY KEY (id);


--
-- TOC entry 3148 (class 2606 OID 69091)
-- Name: Proyecto_userstory Proyecto_userstory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_userstory"
    ADD CONSTRAINT "Proyecto_userstory_pkey" PRIMARY KEY (id);


--
-- TOC entry 3053 (class 2606 OID 68652)
-- Name: account_emailaddress account_emailaddress_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_email_key UNIQUE (email);


--
-- TOC entry 3055 (class 2606 OID 68654)
-- Name: account_emailaddress account_emailaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_pkey PRIMARY KEY (id);


--
-- TOC entry 3060 (class 2606 OID 68656)
-- Name: account_emailconfirmation account_emailconfirmation_key_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_key_key UNIQUE (key);


--
-- TOC entry 3062 (class 2606 OID 68658)
-- Name: account_emailconfirmation account_emailconfirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_pkey PRIMARY KEY (id);


--
-- TOC entry 3065 (class 2606 OID 68660)
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 3070 (class 2606 OID 68662)
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 3073 (class 2606 OID 68664)
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3067 (class 2606 OID 68666)
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3076 (class 2606 OID 68668)
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 3078 (class 2606 OID 68670)
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3086 (class 2606 OID 68672)
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 3089 (class 2606 OID 68674)
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- TOC entry 3080 (class 2606 OID 68676)
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3092 (class 2606 OID 68678)
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3095 (class 2606 OID 68680)
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- TOC entry 3083 (class 2606 OID 68682)
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 3098 (class 2606 OID 68684)
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3101 (class 2606 OID 68686)
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 3103 (class 2606 OID 68688)
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3105 (class 2606 OID 68690)
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 3108 (class 2606 OID 68692)
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 3112 (class 2606 OID 68694)
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- TOC entry 3114 (class 2606 OID 68696)
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- TOC entry 3116 (class 2606 OID 68698)
-- Name: socialaccount_socialaccount socialaccount_socialaccount_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_pkey PRIMARY KEY (id);


--
-- TOC entry 3118 (class 2606 OID 68700)
-- Name: socialaccount_socialaccount socialaccount_socialaccount_provider_uid_fc810c6e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_provider_uid_fc810c6e_uniq UNIQUE (provider, uid);


--
-- TOC entry 3123 (class 2606 OID 68702)
-- Name: socialaccount_socialapp_sites socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq UNIQUE (socialapp_id, site_id);


--
-- TOC entry 3121 (class 2606 OID 68704)
-- Name: socialaccount_socialapp socialaccount_socialapp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp
    ADD CONSTRAINT socialaccount_socialapp_pkey PRIMARY KEY (id);


--
-- TOC entry 3125 (class 2606 OID 68706)
-- Name: socialaccount_socialapp_sites socialaccount_socialapp_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp_sites_pkey PRIMARY KEY (id);


--
-- TOC entry 3131 (class 2606 OID 68708)
-- Name: socialaccount_socialtoken socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq UNIQUE (app_id, account_id);


--
-- TOC entry 3133 (class 2606 OID 68710)
-- Name: socialaccount_socialtoken socialaccount_socialtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_pkey PRIMARY KEY (id);


--
-- TOC entry 3164 (class 1259 OID 69235)
-- Name: Proyecto_actividadus_idMiembro_id_fc1d827c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_actividadus_idMiembro_id_fc1d827c" ON public."Proyecto_actividadus" USING btree ("idMiembro_id");


--
-- TOC entry 3165 (class 1259 OID 69236)
-- Name: Proyecto_actividadus_idSprint_id_d492925c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_actividadus_idSprint_id_d492925c" ON public."Proyecto_actividadus" USING btree ("idSprint_id");


--
-- TOC entry 3166 (class 1259 OID 69237)
-- Name: Proyecto_actividadus_idUs_id_c094742c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_actividadus_idUs_id_c094742c" ON public."Proyecto_actividadus" USING btree ("idUs_id");


--
-- TOC entry 3160 (class 1259 OID 69218)
-- Name: Proyecto_historialmodificacion_idMiembro_id_50f5bcda; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_historialmodificacion_idMiembro_id_50f5bcda" ON public."Proyecto_historialmodificacion" USING btree ("idMiembro_id");


--
-- TOC entry 3161 (class 1259 OID 69219)
-- Name: Proyecto_historialmodificacion_idUs_id_4c29c68c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_historialmodificacion_idUs_id_4c29c68c" ON public."Proyecto_historialmodificacion" USING btree ("idUs_id");


--
-- TOC entry 3134 (class 1259 OID 69205)
-- Name: Proyecto_miembros_idProyecto_id_88ce7400; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_miembros_idProyecto_id_88ce7400" ON public."Proyecto_miembros" USING btree ("idProyecto_id");


--
-- TOC entry 3135 (class 1259 OID 69206)
-- Name: Proyecto_miembros_idRol_id_505519ee; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_miembros_idRol_id_505519ee" ON public."Proyecto_miembros" USING btree ("idRol_id");


--
-- TOC entry 3136 (class 1259 OID 69207)
-- Name: Proyecto_miembros_idUser_id_18be9925; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_miembros_idUser_id_18be9925" ON public."Proyecto_miembros" USING btree ("idUser_id");


--
-- TOC entry 3155 (class 1259 OID 69202)
-- Name: Proyecto_planningpoker_idSprint_id_799b964e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_planningpoker_idSprint_id_799b964e" ON public."Proyecto_planningpoker" USING btree ("idSprint_id");


--
-- TOC entry 3156 (class 1259 OID 69203)
-- Name: Proyecto_planningpoker_idUs_id_d25cb34a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_planningpoker_idUs_id_d25cb34a" ON public."Proyecto_planningpoker" USING btree ("idUs_id");


--
-- TOC entry 3157 (class 1259 OID 69204)
-- Name: Proyecto_planningpoker_miembroEncargado_id_a4fd2737; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_planningpoker_miembroEncargado_id_a4fd2737" ON public."Proyecto_planningpoker" USING btree ("miembroEncargado_id");


--
-- TOC entry 3139 (class 1259 OID 69161)
-- Name: Proyecto_proyectos_nombre_b91f5433_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_proyectos_nombre_b91f5433_like" ON public."Proyecto_proyectos" USING btree (nombre varchar_pattern_ops);


--
-- TOC entry 3144 (class 1259 OID 69162)
-- Name: Proyecto_proyectos_scrumMaster_id_1a8c6694; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_proyectos_scrumMaster_id_1a8c6694" ON public."Proyecto_proyectos" USING btree ("scrumMaster_id");


--
-- TOC entry 3152 (class 1259 OID 69186)
-- Name: Proyecto_roles_idProyecto_id_19bcd5e7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_roles_idProyecto_id_19bcd5e7" ON public."Proyecto_roles" USING btree ("idProyecto_id");


--
-- TOC entry 3149 (class 1259 OID 69180)
-- Name: Proyecto_sprint_idProyecto_id_cb8f1f79; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_sprint_idProyecto_id_cb8f1f79" ON public."Proyecto_sprint" USING btree ("idProyecto_id");


--
-- TOC entry 3145 (class 1259 OID 69173)
-- Name: Proyecto_userstory_encargado_id_e23a6a88; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_userstory_encargado_id_e23a6a88" ON public."Proyecto_userstory" USING btree (encargado_id);


--
-- TOC entry 3146 (class 1259 OID 69174)
-- Name: Proyecto_userstory_idProyecto_id_56c22b72; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Proyecto_userstory_idProyecto_id_56c22b72" ON public."Proyecto_userstory" USING btree ("idProyecto_id");


--
-- TOC entry 3051 (class 1259 OID 68728)
-- Name: account_emailaddress_email_03be32b2_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX account_emailaddress_email_03be32b2_like ON public.account_emailaddress USING btree (email varchar_pattern_ops);


--
-- TOC entry 3056 (class 1259 OID 68729)
-- Name: account_emailaddress_user_id_2c513194; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX account_emailaddress_user_id_2c513194 ON public.account_emailaddress USING btree (user_id);


--
-- TOC entry 3057 (class 1259 OID 68730)
-- Name: account_emailconfirmation_email_address_id_5b7f8c58; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX account_emailconfirmation_email_address_id_5b7f8c58 ON public.account_emailconfirmation USING btree (email_address_id);


--
-- TOC entry 3058 (class 1259 OID 68731)
-- Name: account_emailconfirmation_key_f43612bd_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX account_emailconfirmation_key_f43612bd_like ON public.account_emailconfirmation USING btree (key varchar_pattern_ops);


--
-- TOC entry 3063 (class 1259 OID 68732)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 3068 (class 1259 OID 68733)
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- TOC entry 3071 (class 1259 OID 68734)
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- TOC entry 3074 (class 1259 OID 68735)
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- TOC entry 3084 (class 1259 OID 68736)
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- TOC entry 3087 (class 1259 OID 68737)
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- TOC entry 3090 (class 1259 OID 68738)
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 3093 (class 1259 OID 68739)
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 3081 (class 1259 OID 68740)
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 3096 (class 1259 OID 68741)
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- TOC entry 3099 (class 1259 OID 68742)
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- TOC entry 3106 (class 1259 OID 68743)
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- TOC entry 3109 (class 1259 OID 68744)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 3110 (class 1259 OID 68745)
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- TOC entry 3119 (class 1259 OID 68746)
-- Name: socialaccount_socialaccount_user_id_8146e70c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialaccount_user_id_8146e70c ON public.socialaccount_socialaccount USING btree (user_id);


--
-- TOC entry 3126 (class 1259 OID 68747)
-- Name: socialaccount_socialapp_sites_site_id_2579dee5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialapp_sites_site_id_2579dee5 ON public.socialaccount_socialapp_sites USING btree (site_id);


--
-- TOC entry 3127 (class 1259 OID 68748)
-- Name: socialaccount_socialapp_sites_socialapp_id_97fb6e7d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialapp_sites_socialapp_id_97fb6e7d ON public.socialaccount_socialapp_sites USING btree (socialapp_id);


--
-- TOC entry 3128 (class 1259 OID 68749)
-- Name: socialaccount_socialtoken_account_id_951f210e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialtoken_account_id_951f210e ON public.socialaccount_socialtoken USING btree (account_id);


--
-- TOC entry 3129 (class 1259 OID 68750)
-- Name: socialaccount_socialtoken_app_id_636a42d7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX socialaccount_socialtoken_app_id_636a42d7 ON public.socialaccount_socialtoken USING btree (app_id);


--
-- TOC entry 3197 (class 2606 OID 69220)
-- Name: Proyecto_actividadus Proyecto_actividadus_idMiembro_id_fc1d827c_fk_Proyecto_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_actividadus"
    ADD CONSTRAINT "Proyecto_actividadus_idMiembro_id_fc1d827c_fk_Proyecto_" FOREIGN KEY ("idMiembro_id") REFERENCES public."Proyecto_miembros"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3198 (class 2606 OID 69225)
-- Name: Proyecto_actividadus Proyecto_actividadus_idSprint_id_d492925c_fk_Proyecto_sprint_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_actividadus"
    ADD CONSTRAINT "Proyecto_actividadus_idSprint_id_d492925c_fk_Proyecto_sprint_id" FOREIGN KEY ("idSprint_id") REFERENCES public."Proyecto_sprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3199 (class 2606 OID 69230)
-- Name: Proyecto_actividadus Proyecto_actividadus_idUs_id_c094742c_fk_Proyecto_userstory_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_actividadus"
    ADD CONSTRAINT "Proyecto_actividadus_idUs_id_c094742c_fk_Proyecto_userstory_id" FOREIGN KEY ("idUs_id") REFERENCES public."Proyecto_userstory"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3195 (class 2606 OID 69208)
-- Name: Proyecto_historialmodificacion Proyecto_historialmo_idMiembro_id_50f5bcda_fk_Proyecto_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_historialmodificacion"
    ADD CONSTRAINT "Proyecto_historialmo_idMiembro_id_50f5bcda_fk_Proyecto_" FOREIGN KEY ("idMiembro_id") REFERENCES public."Proyecto_miembros"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3196 (class 2606 OID 69213)
-- Name: Proyecto_historialmodificacion Proyecto_historialmo_idUs_id_4c29c68c_fk_Proyecto_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_historialmodificacion"
    ADD CONSTRAINT "Proyecto_historialmo_idUs_id_4c29c68c_fk_Proyecto_" FOREIGN KEY ("idUs_id") REFERENCES public."Proyecto_userstory"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3184 (class 2606 OID 69119)
-- Name: Proyecto_miembros Proyecto_miembros_idProyecto_id_88ce7400_fk_Proyecto_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_miembros"
    ADD CONSTRAINT "Proyecto_miembros_idProyecto_id_88ce7400_fk_Proyecto_" FOREIGN KEY ("idProyecto_id") REFERENCES public."Proyecto_proyectos"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3185 (class 2606 OID 69124)
-- Name: Proyecto_miembros Proyecto_miembros_idRol_id_505519ee_fk_Proyecto_roles_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_miembros"
    ADD CONSTRAINT "Proyecto_miembros_idRol_id_505519ee_fk_Proyecto_roles_id" FOREIGN KEY ("idRol_id") REFERENCES public."Proyecto_roles"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3186 (class 2606 OID 69129)
-- Name: Proyecto_miembros Proyecto_miembros_idUser_id_18be9925_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_miembros"
    ADD CONSTRAINT "Proyecto_miembros_idUser_id_18be9925_fk_auth_user_id" FOREIGN KEY ("idUser_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3192 (class 2606 OID 69187)
-- Name: Proyecto_planningpoker Proyecto_planningpok_idSprint_id_799b964e_fk_Proyecto_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_planningpoker"
    ADD CONSTRAINT "Proyecto_planningpok_idSprint_id_799b964e_fk_Proyecto_" FOREIGN KEY ("idSprint_id") REFERENCES public."Proyecto_sprint"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3193 (class 2606 OID 69192)
-- Name: Proyecto_planningpoker Proyecto_planningpok_idUs_id_d25cb34a_fk_Proyecto_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_planningpoker"
    ADD CONSTRAINT "Proyecto_planningpok_idUs_id_d25cb34a_fk_Proyecto_" FOREIGN KEY ("idUs_id") REFERENCES public."Proyecto_userstory"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3194 (class 2606 OID 69197)
-- Name: Proyecto_planningpoker Proyecto_planningpok_miembroEncargado_id_a4fd2737_fk_Proyecto_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_planningpoker"
    ADD CONSTRAINT "Proyecto_planningpok_miembroEncargado_id_a4fd2737_fk_Proyecto_" FOREIGN KEY ("miembroEncargado_id") REFERENCES public."Proyecto_miembros"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3187 (class 2606 OID 69156)
-- Name: Proyecto_proyectos Proyecto_proyectos_scrumMaster_id_1a8c6694_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_proyectos"
    ADD CONSTRAINT "Proyecto_proyectos_scrumMaster_id_1a8c6694_fk_auth_user_id" FOREIGN KEY ("scrumMaster_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3191 (class 2606 OID 69181)
-- Name: Proyecto_roles Proyecto_roles_idProyecto_id_19bcd5e7_fk_Proyecto_proyectos_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_roles"
    ADD CONSTRAINT "Proyecto_roles_idProyecto_id_19bcd5e7_fk_Proyecto_proyectos_id" FOREIGN KEY ("idProyecto_id") REFERENCES public."Proyecto_proyectos"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3190 (class 2606 OID 69175)
-- Name: Proyecto_sprint Proyecto_sprint_idProyecto_id_cb8f1f79_fk_Proyecto_proyectos_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_sprint"
    ADD CONSTRAINT "Proyecto_sprint_idProyecto_id_cb8f1f79_fk_Proyecto_proyectos_id" FOREIGN KEY ("idProyecto_id") REFERENCES public."Proyecto_proyectos"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3188 (class 2606 OID 69163)
-- Name: Proyecto_userstory Proyecto_userstory_encargado_id_e23a6a88_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_userstory"
    ADD CONSTRAINT "Proyecto_userstory_encargado_id_e23a6a88_fk_auth_user_id" FOREIGN KEY (encargado_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3189 (class 2606 OID 69168)
-- Name: Proyecto_userstory Proyecto_userstory_idProyecto_id_56c22b72_fk_Proyecto_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proyecto_userstory"
    ADD CONSTRAINT "Proyecto_userstory_idProyecto_id_56c22b72_fk_Proyecto_" FOREIGN KEY ("idProyecto_id") REFERENCES public."Proyecto_proyectos"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3169 (class 2606 OID 68831)
-- Name: account_emailaddress account_emailaddress_user_id_2c513194_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_user_id_2c513194_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3170 (class 2606 OID 68836)
-- Name: account_emailconfirmation account_emailconfirmation_email_address_id_5b7f8c58_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_email_address_id_5b7f8c58_fk FOREIGN KEY (email_address_id) REFERENCES public.account_emailaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3171 (class 2606 OID 68841)
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3172 (class 2606 OID 68846)
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3173 (class 2606 OID 68851)
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3174 (class 2606 OID 68856)
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3175 (class 2606 OID 68861)
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3176 (class 2606 OID 68866)
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3177 (class 2606 OID 68871)
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3178 (class 2606 OID 68876)
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3179 (class 2606 OID 68881)
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3181 (class 2606 OID 68886)
-- Name: socialaccount_socialapp_sites socialaccount_social_site_id_2579dee5_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_social_site_id_2579dee5_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3180 (class 2606 OID 68891)
-- Name: socialaccount_socialaccount socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3182 (class 2606 OID 68896)
-- Name: socialaccount_socialtoken socialaccount_socialtoken_account_id_951f210e_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_account_id_951f210e_fk FOREIGN KEY (account_id) REFERENCES public.socialaccount_socialaccount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3183 (class 2606 OID 68901)
-- Name: socialaccount_socialtoken socialaccount_socialtoken_app_id_636a42d7_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_app_id_636a42d7_fk FOREIGN KEY (app_id) REFERENCES public.socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


-- Completed on 2021-11-18 22:27:36 -03

--
-- PostgreSQL database dump complete
--

