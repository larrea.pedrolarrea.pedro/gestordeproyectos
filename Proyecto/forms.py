from django import forms
from django.db.models import Model

from Proyecto.models import *
from django.forms import ModelForm, TextInput, EmailInput


class FormularioUs(forms.ModelForm):  # form para nuevo us
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # para no hacer obligadatorios algunos campos
        # self.fields['titulo', 'descripcion', 'encargado'].required = True

    # metodo para validar el dominio de los inputs de los campos
    def clean(self, *args, **kwargs):
        cleaned_data = super(FormularioUs, self).clean(*args, **kwargs)
        prioridad = cleaned_data.get('prioridad')
        if prioridad is not None:
            if prioridad < 1 or prioridad > 5:
                self.add_error('prioridad', 'rango no valido')
        estimacion = cleaned_data.get('estimacion')
        if estimacion is not None:
            if estimacion < 0:
                self.add_error('estimacion', 'rango no valido')

    class Meta:
        model = userStory
        fields = ['idProyecto', 'titulo', 'descripcion', 'estimacion', 'prioridad', 'encargado', 'fechaIngreso']
        labels = {
            'idProyecto': 'Proyecto',
            'titulo': 'Titulo',
            'descripcion': 'Descripcion',
            'estimacion': 'Estimacion',
            'prioridad': 'Prioridad',
            'encargado': 'Encargado',
            'fechaIngreso': 'Fecha de Ingreso',
        }

        widgets = {
            'idProyecto': forms.HiddenInput(),  # oculta el label del idProyectop
            'fechaIngreso': forms.DateInput(attrs={'type': 'date', 'readonly': 'readonly'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'titulo de US'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripcion de US'}),
            'estimacion': forms.HiddenInput(),
            'prioridad': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': '1 (Muy Alta) a 5 (Muy Baja)'}),
            'encargado': forms.HiddenInput(),

        }


class FormularioEditUs(forms.ModelForm):  # form para editar un us
    # metodo para validar el dominio de los inputs de los campos
    def clean(self, *args, **kwargs):
        cleaned_data = super(FormularioEditUs, self).clean(*args, **kwargs)
        prioridad = cleaned_data.get('prioridad')
        if prioridad is not None:
            if prioridad < 1 or prioridad > 5:
                self.add_error('prioridad', 'rango no valido')

    class Meta:
        model = userStory
        fields = ['idProyecto', 'titulo', 'descripcion', 'estimacion', 'prioridad', 'encargado', 'fechaIngreso']
        labels = {
            'idProyecto': 'Proyecto',
            'titulo': 'Titulo',
            'descripcion': 'Descripcion',
            'estimacion': 'Estimacion',
            'prioridad': 'Prioridad',
            'encargado': 'Encargado',
            'fechaIngreso': 'Fecha de Ingreso',
        }

        # DEFINIR CON LOS PERMISOS QUE SE PUEDE Y QUE NO SE PUEDE EDITAR
        widgets = {
            'idProyecto': forms.HiddenInput(),  # oculta el label del idProyectop
            'fechaIngreso': forms.DateInput(attrs={'type': 'date', 'readonly': 'readonly'}),
            'titulo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'titulo de US'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripcion de US'}),
            'estimacion': forms.HiddenInput(),
            'prioridad': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': '1 (Muy Alta) a 5 (Muy Baja)'}),
            'encargado': forms.HiddenInput(),

        }


# *
# Formato para Crear nuevo Proyecto
# *#
class ProyectosFormCrear(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['estado'].required = False

    class Meta:
        model = Proyectos
        fields = '__all__'
        widgets = {
            'nombre': TextInput(attrs={
                'class': "form-control"
            }),
            'fechaInicio': forms.DateInput(attrs={'type': 'date', 'class': "form-control"}),
            'fechaFin': forms.DateInput(attrs={'type': 'date', 'class': "form-control"}),
            'descripcion': forms.Textarea(attrs={'type': 'text', 'class': "form-control"}),
            'scrumMaster': forms.Select(attrs={'type': 'text', 'class': "custom-select custom-select mb-3 "}),
        }


# *
# Formato para Crear nuevo Rol de Proyecto
# *#
class RolProyectoFormCrear(forms.ModelForm):
    class Meta:
        model = Roles
        fields = '__all__'
        widgets = {
            'nombre': TextInput(attrs={
                'class': "form-control"
            }),
            'descripcion': forms.Textarea(attrs={'type': 'text', 'class': "form-control"}),
            'agregarUserStory': forms.CheckboxInput,
            'eliminarUserStory': forms.CheckboxInput,
            'modificarUserStory': forms.CheckboxInput,
            'agregarMiembro': forms.CheckboxInput,
            'modificarMiembro': forms.CheckboxInput,
            'eliminarMiembro': forms.CheckboxInput,
            'crearRol': forms.CheckboxInput,
            'modificarRol': forms.CheckboxInput,
            'eliminarRol': forms.CheckboxInput,
            'crearSprint': forms.CheckboxInput,
            'empezarSprint': forms.CheckboxInput,
            'finalizarSprint': forms.CheckboxInput,
            'agregarSprintBacklog': forms.CheckboxInput,
            'modificarSprintBacklog': forms.CheckboxInput

        }


# *
# Formato para editar Rol de Proyecto
# *#
class RolProyectoFormEditar(forms.ModelForm):
    class Meta:
        model = Roles
        fields = '__all__'
        widgets = {
            'nombre': TextInput(attrs={
                'class': "form-control"
            }),
            'descripcion': forms.Textarea(attrs={'type': 'text',
                                                 'class': "form-control"}),
            'agregarUserStory': forms.CheckboxInput,
            'eliminarUserStory': forms.CheckboxInput,
            'modificarUserStory': forms.CheckboxInput,
            'agregarMiembro': forms.CheckboxInput,
            'modificarMiembro': forms.CheckboxInput,
            'eliminarMiembro': forms.CheckboxInput,
            'crearRol': forms.CheckboxInput,
            'modificarRol': forms.CheckboxInput,
            'eliminarRol': forms.CheckboxInput,
            'crearSprint': forms.CheckboxInput,
            'empezarSprint': forms.CheckboxInput,
            'finalizarSprint': forms.CheckboxInput,
            'agregarSprintBacklog': forms.CheckboxInput,
            'modificarSprintBacklog': forms.CheckboxInput

        }


# *
# Formato para ver Rol de Proyecto
# *#
class RolProyectoFormVer(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs['readonly'] = True
        self.fields['descripcion'].widget.attrs['readonly'] = True

    class Meta:
        model = Roles
        fields = '__all__'
        widgets = {
            'nombre': TextInput(attrs={
                'class': "form-control"
            }),
            'descripcion': forms.Textarea(attrs={'type': 'text',
                                                 'class': "form-control"}),
            'agregarUserStory': forms.CheckboxInput,
            'eliminarUserStory': forms.CheckboxInput,
            'modificarUserStory': forms.CheckboxInput,
            'agregarMiembro': forms.CheckboxInput,
            'modificarMiembro': forms.CheckboxInput,
            'eliminarMiembro': forms.CheckboxInput,
            'crearRol': forms.CheckboxInput,
            'modificarRol': forms.CheckboxInput,
            'eliminarRol': forms.CheckboxInput,
            'crearSprint': forms.CheckboxInput,
            'empezarSprint': forms.CheckboxInput,
            'finalizarSprint': forms.CheckboxInput,
            'agregarSprintBacklog': forms.CheckboxInput,
            'modificarSprintBacklog': forms.CheckboxInput

        }


# *
# Formata para Editar nuevo Proyecto
# *#
class ProyectosFormEditar(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['estado'].required = False

    class Meta:
        model = Proyectos
        fields = '__all__'
        widgets = {
            'nombre': TextInput(attrs={
                'class': "form-control"
            }),
            'fechaInicio': forms.DateInput(attrs={'type': 'date',
                                                  'class': "form-control"}),
            'fechaFin': forms.DateInput(attrs={'type': 'date',
                                               'class': "form-control"}),
            'descripcion': forms.Textarea(attrs={'type': 'text',
                                                 'class': "form-control"}),
            'scrumMaster': forms.Select(attrs={'type': 'text',
                                               'class': "custom-select custom-select mb-3 "}),
        }


class FormularioMiembros(forms.ModelForm):  # form para nuevo miembro
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['lunes'].required = True
        self.fields['martes'].required = True
        self.fields['miercoles'].required = True
        self.fields['jueves'].required = True
        self.fields['viernes'].required = True
        self.fields['sabado'].required = True
        self.fields['domingo'].required = True
        self.fields['idRol'].required = True

    # metodo para validar el dominio de los inputs de los campos
    def clean(self, *args, **kwargs):
        cleaned_data = super(FormularioMiembros, self).clean(*args, **kwargs)
        lunes = cleaned_data.get('lunes')
        if lunes is not None:
            if lunes < 0 or lunes > 24:
                self.add_error('lunes', 'rango de horas no valido')
        martes = cleaned_data.get('martes')
        if martes is not None:
            if martes < 0 or martes > 24:
                self.add_error('martes', 'rango de horas no valido')
        miercoles = cleaned_data.get('miercoles')
        if miercoles is not None:
            if miercoles < 0 or miercoles > 24:
                self.add_error('miercoles', 'rango de horas no valido')
        jueves = cleaned_data.get('jueves')
        if jueves is not None:
            if jueves < 0 or jueves > 24:
                self.add_error('jueves', 'rango de horas no valido')
        viernes = cleaned_data.get('viernes')
        if viernes is not None:
            if viernes < 0 or viernes > 24:
                self.add_error('viernes', 'rango de horas no valido')
        sabado = cleaned_data.get('sabado')
        if sabado is not None:
            if sabado < 0 or sabado > 24:
                self.add_error('sabado', 'rango de horas no valido')
        domingo = cleaned_data.get('domingo')
        if domingo is not None:
            if domingo < 0 or domingo > 24:
                self.add_error('domingo', 'rango de horas no valido')

    class Meta:
        model = miembros
        fields = ['idUser', 'idProyecto', 'idRol', 'activo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes',
                  'sabado', 'domingo']
        labels = {
            'idUser': 'Usuario',
            'idProyecto': 'Proyecto',
            'idRol': 'Rol',
            'activo': 'Activo',
            'lunes': 'Lunes',
            'martes': 'Martes',
            'miercoles': 'Miercoles',
            'jueves': 'Jueves',
            'viernes': 'Viernes',
            'sabado': 'Sabado',
            'domingo': 'Domingo',
        }

        widgets = {
            'idUser': forms.HiddenInput(),  # oculta el label del idProyectop
            'idProyecto': forms.HiddenInput(),  # oculta el label del idProyectop
            'activo': forms.HiddenInput(),  # oculta el label del idProyectop
        }


class FormularioEditarMiembros(forms.ModelForm):  # form para Editar un miembro
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['lunes'].required = True
        self.fields['martes'].required = True
        self.fields['miercoles'].required = True
        self.fields['jueves'].required = True
        self.fields['viernes'].required = True
        self.fields['sabado'].required = True
        self.fields['domingo'].required = True
        self.fields['idRol'].required = True

    # metodo para validar el dominio de los inputs de los campos
    def clean(self, *args, **kwargs):
        cleaned_data = super(FormularioEditarMiembros, self).clean(*args, **kwargs)
        lunes = cleaned_data.get('lunes')
        if lunes is not None:
            if lunes < 0 or lunes > 24:
                self.add_error('lunes', 'rango de horas no valido')
        martes = cleaned_data.get('martes')
        if martes is not None:
            if martes < 0 or martes > 24:
                self.add_error('martes', 'rango de horas no valido')
        miercoles = cleaned_data.get('miercoles')
        if miercoles is not None:
            if miercoles < 0 or miercoles > 24:
                self.add_error('miercoles', 'rango de horas no valido')
        jueves = cleaned_data.get('jueves')
        if jueves is not None:
            if jueves < 0 or jueves > 24:
                self.add_error('jueves', 'rango de horas no valido')
        viernes = cleaned_data.get('viernes')
        if viernes is not None:
            if viernes < 0 or viernes > 24:
                self.add_error('viernes', 'rango de horas no valido')
        sabado = cleaned_data.get('sabado')
        if sabado is not None:
            if sabado < 0 or sabado > 24:
                self.add_error('sabado', 'rango de horas no valido')
        domingo = cleaned_data.get('domingo')
        if domingo is not None:
            if domingo < 0 or domingo > 24:
                self.add_error('domingo', 'rango de horas no valido')

    class Meta:
        model = miembros
        fields = ['idUser', 'idProyecto', 'idRol', 'activo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes',
                  'sabado', 'domingo']
        labels = {
            'idRol': 'Rol',
            'activo': 'Activo',
            'lunes': 'Lunes',
            'martes': 'Martes',
            'miercoles': 'Miercoles',
            'jueves': 'Jueves',
            'viernes': 'Viernes',
            'sabado': 'Sabado',
            'domingo': 'Domingo',
        }
        widgets = {
            'idUser': forms.HiddenInput(),  # oculta el label del idProyectop
            'idProyecto': forms.HiddenInput(),  # oculta el label del idProyectop
            'activo': forms.HiddenInput(),  # oculta el label del idProyectop
        }


class FormularioVerMiembros(forms.ModelForm):  # form para ver miembro

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['lunes'].widget.attrs['readonly'] = True
        self.fields['martes'].widget.attrs['readonly'] = True
        self.fields['miercoles'].widget.attrs['readonly'] = True
        self.fields['jueves'].widget.attrs['readonly'] = True
        self.fields['viernes'].widget.attrs['readonly'] = True
        self.fields['sabado'].widget.attrs['readonly'] = True
        self.fields['domingo'].widget.attrs['readonly'] = True
        self.fields['idRol'].widget.attrs['readonly'] = True

    class Meta:
        model = miembros
        fields = ['idUser', 'idProyecto', 'idRol', 'activo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes',
                  'sabado', 'domingo']
        labels = {
            'idUser': 'Usuario',
            'idProyecto': 'Proyecto',
            'idRol': 'Rol',
            'activo': 'Activo',
            'lunes': 'Lunes',
            'martes': 'Martes',
            'miercoles': 'Miercoles',
            'jueves': 'Jueves',
            'viernes': 'Viernes',
            'sabado': 'Sabado',
            'domingo': 'Domingo',
        }

        widgets = {
            'idUser': forms.HiddenInput(),  # oculta el label del idProyectop
            'idProyecto': forms.HiddenInput(),  # oculta el label del idProyectop
            'activo': forms.HiddenInput(),  # oculta el label del idProyectop 'readonly': 'readonly'
        }


class guardarEstimacion(forms.ModelForm):  # form para nuevo miembro
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['estimacionFinal'].required = False
        self.fields['estimacionEncargado'].required = True

    def clean(self, *args, **kwargs):
        cleaned_data = super(guardarEstimacion, self).clean(*args, **kwargs)
        estimacionEncargado = cleaned_data.get('estimacionEncargado')
        if estimacionEncargado is not None:
            if estimacionEncargado < 0:
                self.add_error('estimacionEncargado', 'rango de horas no valido')

    class Meta:
        model = PlanningPoker
        fields = '__all__'

        labels = {
            'estimacionEncargado': '',
        }

        widgets = {
            'estimacionSM': forms.HiddenInput(),  # oculta el label del idProyectop
            'estimacionFinal': forms.HiddenInput(),  # oculta el label del idProyectop
            'miembroEncargado': forms.HiddenInput(),
            'idUs': forms.HiddenInput(),
            'idSprint': forms.HiddenInput(),
            'estimacionEncargado': forms.NumberInput(
                attrs={'class': 'form-control', 'placeholder': 'Ingrese una estimacion', 'type': 'number'}),
            'prioridad': forms.HiddenInput(),
            'estado' : forms.HiddenInput()

        }


# Formulacio para Crear Nuevo Sprint
class FormularioNuevoSprint(forms.ModelForm):  # form para nuevo us
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # para no hacer obligadatorios algunos campos
        self.fields['fechaFin'].required = False

    class Meta:
        model = Sprint
        fields = ['idProyecto', 'numero', 'fechaInicio', 'fechaFin']
        labels = {
            'idProyecto': 'ID Proyecto',
            'numero': 'Numero de Sprint',
            'fechaInicio': 'Fecha de Inicio',
            'fechaFin': 'Fecha de Fin',
            # 'estado': 'Estado',
        }

        widgets = {
            'idProyecto': forms.HiddenInput(),  # oculta el label del idProyectop
            'numero': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Numero de sprint', 'type': 'number', 'readonly':'readonly'}),
            'fechaInicio': forms.DateInput(attrs={'type': 'date'}),
            'fechaFin': forms.DateInput(attrs={'type': 'date'}),
            # 'estado': forms.Select(attrs={'class': 'form-control'}),
        }


# Formulario para Crear Planningo Pocker al agregar un UserStory al Sprintbacklog
class formCrearPlanningPoker(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # para no hacer obligadatorios algunos campos
        self.fields['estimacionEncargado'].required = False

    # metodo para validar el dominio de los inputs de los campos
    def clean(self, *args, **kwargs):
        cleaned_data = super(formCrearPlanningPoker, self).clean(*args, **kwargs)
        prioridad = cleaned_data.get('prioridad')
        if prioridad is not None:
            if prioridad < 1 or prioridad > 5:
                self.add_error('prioridad', 'rango no valido')

    class Meta:
        model = PlanningPoker
        fields = ['prioridad', 'estimacionSM', 'estimacionEncargado', 'estimacionFinal', 'miembroEncargado', 'idUs',
                  'idSprint', ]
        labels = {
            'prioridad': 'Prioridad',
            'estimacionSM': 'Estimacion del SM',
            'estimacionEncargado': 'Estimacion del Encargado',
            'estimacionFinal': 'Estimacion Final',
            'miembroEncargado': 'Encargado',
            'idUs': 'ID Us',
            'idSprint': 'ID Sprint',
        }

        widgets = {
            'idUs': forms.HiddenInput(),  # oculta el label del idUserStory
            'idSprint': forms.HiddenInput(),  # oculta el label del idSprint
            'estimacionSM': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Ingrese estimacion en Horas', 'type': 'number'}),
            'estimacionEncargado': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Ingrese estimacion en Horas', 'type': 'number'}),
            'estimacionFinal': forms.HiddenInput(),
            'prioridad': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': '1 (Muy Alta) a 5 (Muy Baja)'}),
        }


# Formulario para Editar algunos Campos de UserStory al hacerle un Sprint Planning del mismo
# NO SE ESTA USANDO, SE ESA DEJANDO POR SI SE NECESITA EN UN FUTURO
class formVerUS(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['titulo'].widget.attrs['readonly'] = True
        self.fields['titulo'].required = False
        self.fields['descripcion'].widget.attrs['readonly'] = True
        self.fields['descripcion'].required = False
        self.fields['fechaIngreso'].widget.attrs['readonly'] = True
        self.fields['fechaIngreso'].required = False
        self.fields['titulo'].widget.attrs['readonly'] = True
        self.fields['titulo'].required = False
        self.fields['prioridad'].required = True

    # metodo para validar el dominio de los inputs de los campos
    def clean(self, *args, **kwargs):
        cleaned_data = super(formVerUS, self).clean(*args, **kwargs)
        prioridad = cleaned_data.get('prioridad')
        if prioridad is not None:
            if prioridad < 1 or prioridad > 5:
                self.add_error('prioridad', 'rango no valido')

    class Meta:
        model = userStory
        fields = ['id', 'idProyecto', 'titulo', 'descripcion', 'estado', 'estimacion', 'prioridad', 'encargado',
                  'fechaIngreso']
        labels = {
            'id': 'ID',
            'idProyecto': 'Proyecto',
            'titulo': 'Titulo',
            'descripcion': 'Descripcion',
            'estado': 'Estado',
            'estimacion': 'Estimacion',
            'fechaIngreso': 'Fecha de ingreso',
            'prioridad': 'Prioridad',
            'encargado': 'Encargado',
        }

        # DEFINIR CON LOS PERMISOS QUE SE PUEDE Y QUE NO SE PUEDE EDITAR
        widgets = {
            '   id': forms.IntegerField(),
            'idProyecto': forms.HiddenInput(),  # oculta el label del idProyectop
            'titulo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'titulo de US'}),
            'estado': forms.HiddenInput(),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripcion de US'}),
            'estimacion': forms.HiddenInput(),
            'prioridad': forms.HiddenInput(),
            'encargado': forms.HiddenInput(),
            'fechaIngreso': forms.DateInput(),
        }


class FormAlterarFechaSprint(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # para no hacer obligadatorios algunos campos
        self.fields['fechaFin'].required = True
        self.fields['fechaInicio'].required = True

    class Meta:
        model = Sprint
        fields = ['idProyecto', 'numero', 'fechaInicio', 'fechaFin', 'estado']

        labels = {
            'idProyecto': 'ID Proyecto',
            'numero': 'Numero de Sprint',
            'fechaInicio': 'Fecha de Inicio',
            'fechaFin': 'Fecha de Fin',
            'estado': 'Estado',
        }

        widgets = {
            'idProyecto': forms.HiddenInput(),  # oculta el label del idProyectop
            'numero': forms.HiddenInput(),
            'fechaInicio': forms.DateInput(attrs={'type': 'date'}),
            'fechaFin': forms.DateInput(attrs={'type': 'date'}),
            'estado': forms.HiddenInput(),

        }

#Formulario para que un miembro pueda cargar la actividad que realiza en un  historia de usuario
class FormActividadNueva(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = ActividadUs
        fields = ['fecha', 'detalle', 'duracion', 'idMiembro', 'idUs', 'idSprint']

        labels = {
            'fecha': 'Fecha',
            #'hora': 'Hora',
            'detalle': 'Detalle',
            'duracion': 'Duracion',
            'idMiembro': 'Miembro',
            'idUs': 'User Story',
            'idSprint': 'Sprint',
        }
        widgets = {
            'fecha': forms.DateInput(attrs={'type': 'date'}),
            #'hora': forms.HiddenInput(),
            'detalle': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Actividad realizada'}),
            'duracion': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Tiempo dedicado(en horas)', 'type': 'number'}),
            'idMiembro': forms.HiddenInput(),
            'idUs': forms.HiddenInput(),
            'idSprint': forms.HiddenInput(),
        }


#Form para editar las actividades
class FormEditActividad(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = ActividadUs
        fields = ['fecha', 'detalle', 'duracion', 'idMiembro', 'idUs', 'idSprint']

        labels = {
            'fecha': 'Fecha',
            #'hora': 'Hora',
            'detalle': 'Detalle',
            'duracion': 'Duracion',
            'idMiembro': 'Miembro',
            'idUs': 'User Story',
            'idSprint': 'Sprint',
        }
        widgets = {
            'fecha': forms.DateInput(attrs={'type': 'date'}),
            #'hora': forms.HiddenInput(),
            'detalle': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Actividad realizada'}),
            'duracion': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Tiempo dedicado(en horas)', 'type': 'number'}),
            'idMiembro': forms.HiddenInput(),
            'idUs': forms.HiddenInput(),
            'idSprint': forms.HiddenInput(),
        }