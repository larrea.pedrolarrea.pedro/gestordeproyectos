import datetime

from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import Permission, User, GroupManager
from datetime import date, timezone
from django.contrib.auth.models import Permission, User, GroupManager
from django.utils import *

# Modelos para el modulo de proyecto

# Modelo de Proyecto
from django.utils.datetime_safe import time
from pygments.lexer import default

import Proyecto


def get_name(self):
    return self.first_name + " " + self.last_name + " | " + self.email


User.add_to_class("__str__", get_name)


class Proyectos(models.Model):
    # Lista de Estado que puede tener un Proyecto
    ESTADO = (
        ('P', 'Pendiente'),
        ('E', 'En Curso'),
        ('T', 'Terminado'),
        ('C', 'Cancelado'),
    )

    def __str__(self):
        return self.nombre

    nombre = models.CharField(max_length=150, unique=True)  # Nombre del Proyecto
    fechaInicio = models.DateField()  # Fecha de inicio del Proyecto
    fechaFin = models.DateField(null=True, blank=True)  # Fecha de Fin del Proyecto
    descripcion = models.CharField(max_length=450, null=True, blank=True)  # descripcion del Proyecto

    estado = models.CharField(max_length=12, choices=ESTADO, default='P')  # Estado de un Proyecto

    scrumMaster = models.ForeignKey(User, on_delete=models.RESTRICT)  # Scrum Master del Proyecto


# Modelo de Roles
class Roles(models.Model):
    idProyecto = models.ForeignKey(Proyectos, on_delete=models.RESTRICT)  # Proyecto al que pertence el rol
    nombre = models.CharField(max_length=250)  # Nombre del Rol, ejemplo scrum master
    descripcion = models.CharField(max_length=450)  # Descripcion del Rol

    # Campos de Permisos
    agregarUserStory = models.BooleanField(default=False)
    eliminarUserStory = models.BooleanField(default=False)
    modificarUserStory = models.BooleanField(default=False)
    agregarMiembro = models.BooleanField(default=False)
    modificarMiembro = models.BooleanField(default=False)
    eliminarMiembro = models.BooleanField(default=False)
    crearRol = models.BooleanField(default=False)
    modificarRol = models.BooleanField(default=False)
    eliminarRol = models.BooleanField(default=False)
    crearSprint = models.BooleanField(default=False)
    empezarSprint = models.BooleanField(default=False)
    finalizarSprint = models.BooleanField(default=False)
    agregarSprintBacklog = models.BooleanField(default=False)
    modificarSprintBacklog = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre


# Modelos de Miembros
class miembros(models.Model):
    idUser = models.ForeignKey(User, on_delete=models.RESTRICT)  # Usuario asocicado como miembro a un proyecto
    idProyecto = models.ForeignKey(Proyectos, on_delete=models.RESTRICT)  # Proyecto al que esta asociado el miembro
    idRol = models.ForeignKey(Roles, on_delete=models.RESTRICT, null=True,
                              blank=True)  # Con que tiene asociado el Miembro
    activo = models.BooleanField(default=False)  # Estado del Miembro, si esta Activo == True sino == False

    # carga horaria dentro del proyecto
    lunes = models.FloatField(default=0, null=True, blank=True)
    martes = models.FloatField(default=0, null=True, blank=True)
    miercoles = models.FloatField(default=0, null=True, blank=True)
    jueves = models.FloatField(default=0, null=True, blank=True)
    viernes = models.FloatField(default=0, null=True, blank=True)
    sabado = models.FloatField(default=0, null=True, blank=True)
    domingo = models.FloatField(default=0, null=True, blank=True)

    def __str__(self):
        return self.idUser.first_name + " " + self.idUser.last_name + " | " + self.idUser.email


# Modelo de Historia de Usuario
class userStory(models.Model):
    # Lista de Estado del Proyecto
    ESTADOS = (
        ('N', 'Nuevo'),
        ('PP', 'En Planning Pocker'),
        ('P', 'Pendiente'),
        ('EP', 'En Proceso'),
        ('STSA', 'Sin Terminar Sprint Anterior'),
        ('A', 'Aprobado'),
        ('H', 'Hecho'),
        ('C', 'Cancelado'),
    )

    idProyecto = models.ForeignKey(Proyectos, on_delete=models.RESTRICT)  # Proyecto al que perteneces el US
    titulo = models.CharField(max_length=150)  # Titulo del US
    descripcion = models.CharField(max_length=450, null=True, blank=True)  # Descripcion del US

    estado = models.CharField(max_length=4, choices=ESTADOS, default='N')

    fechaIngreso = models.DateField(("Date"), default=date.today)

    # estado = models.IntegerField( default=1)  # Estado del US

    estimacion = models.FloatField(null=True, blank=True)  # Estimacion del US, (en Horas)
    tiempoDedicado = models.FloatField(default=0)  # Tiempo Dedicado al US, (en Horas)

    prioridad = models.IntegerField(null=True, blank=True)  # Prioridad del US, del 1 al 5
    encargado = models.ForeignKey(User, on_delete=models.RESTRICT, null=True,
                                  blank=True)  # Miembro encargado en trabajar el US


# Modelo de Sprint
class Sprint(models.Model):
    ESTADOS = (
        ('P', 'Pendiente'),
        ('E', 'En Curso'),
        ('T', 'Terminado'),
        ('C', 'Cancelado'),
    )

    idProyecto = models.ForeignKey(Proyectos, on_delete=models.RESTRICT)  # Proyecto al que perteneces el Sprint
    numero = models.IntegerField()  # numero de Sprint
    fechaInicio = models.DateField(null=True)  # Fecha de inicio del Sprint
    fechaFin = models.DateField(null=True)  # Fecha de inicio del Sprint
    estado = models.CharField(max_length=1, choices=ESTADOS, default='P')

    def __str__(self):
        return str(self.id)


# VERIFICAR CON EL RESTO QUE ESTE BIEN
class PlanningPoker(models.Model):
    ESTADOS = (
        ('N', 'Nuevo'),
        ('PP', 'En Planning Pocker'),
        ('P', 'Pendiente'),
        ('EP', 'En Proceso'),
        ('STSA', 'Sin Terminar Sprint Anterior'),
        ('A', 'Aprobado'),
        ('H', 'Hecho'),
        ('C', 'Cancelado'),
    )

    estimacionSM = models.FloatField()  # Estimacion en horas
    estimacionEncargado = models.FloatField(blank=True, null=True)  # Estimacion en horas
    estimacionFinal = models.FloatField(blank=True, null=True)
    prioridad = models.IntegerField()
    # miembroSM = models.ForeignKey(miembros, on_delete=models.RESTRICT)
    estado = models.CharField(max_length=4, choices=ESTADOS, default='N')
    miembroEncargado = models.ForeignKey(miembros,
                                         on_delete=models.RESTRICT)  # Al definirse debe de tener un encargado si o si
    idUs = models.ForeignKey(userStory, on_delete=models.RESTRICT)
    idSprint = models.ForeignKey(Sprint, on_delete=models.RESTRICT)  # Debe pertenecer a un sprintPlanning#o idSprint


# modelo para el "historial" de un Us
class ActividadUs(models.Model):
    # hora y fecha en que se registra una nueva actividad relacionada a la US
    fecha = models.DateField(default=timezone.now)
    # le quito de momento el campo hora
    # hora = models.TimeField(default=datetime.time)
    # descripcion del trabajo realizado sobre la US
    detalle = models.CharField(max_length=500)
    # tiempo dedicado al trabajo sobre la US
    duracion = models.FloatField()
    # miembro que realiza la actividad
    idMiembro = models.ForeignKey(miembros, on_delete=models.RESTRICT)
    # sobre que US se realiza la actividad
    idUs = models.ForeignKey(userStory, on_delete=models.RESTRICT)
    # en que sprint se realizo la actividad
    # hago que pueda ser null porque hay actividades que se deben registrar y no necesariamente estan dentro de un sprint, le doy un valor 0 al id en esos casos
    idSprint = models.ForeignKey(Sprint, on_delete=models.RESTRICT, null=True)

class HistorialModificacion(models.Model):
    #detalle, texto predefinido para saber que se realizo
    detalle = models.CharField(max_length=500)
    # US al que se hace refrencia
    idUs = models.ForeignKey(userStory, on_delete=models.RESTRICT)
    #el miembro que realiza el cambio, ya sea de estado u otro tipo de cambio
    idMiembro = models.ForeignKey(miembros, on_delete=models.RESTRICT)
    #fecha
    fecha = models.DateField(default=timezone.now)
    #hora
    hora = models.TimeField(default=datetime.time)