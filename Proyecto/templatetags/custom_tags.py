from django import template
from Proyecto.models import miembros,Proyectos
from django.shortcuts import get_object_or_404
from allauth.socialaccount.models import SocialAccount
from django.contrib.auth.models import User

register = template.Library()

@register.filter
def get_at_index(list, index):
    return list[index]

@register.filter
def esMiembroEnProyecto(element, idProyecto):
    user = User.objects.get(email__icontains=element.extra_data['email'])
    proyecto = get_object_or_404(Proyectos, pk=idProyecto)
    listaMiembros = miembros.objects.filter(idProyecto=proyecto)
    existe = False
    if proyecto.scrumMaster == user:
        existe = True
    for x in listaMiembros:
        if x.idUser == user and x.activo:
            existe = True
    return existe

@register.filter
def esMiembroElegible(element, idProyecto):
    user = User.objects.get(email__icontains=element.extra_data['email'])
    proyecto = get_object_or_404(Proyectos, pk=idProyecto)
    listaMiembros = miembros.objects.filter(idProyecto=proyecto)
    elegible = False
    for x in listaMiembros:
        if x.idUser == user and x.activo:
            elegible = True
    if proyecto.scrumMaster == user:
        elegible = False
    return elegible

@register.filter
def esEsteMiembro(element, miembro):
    user = User.objects.get(email__icontains=element.extra_data['email'])
    elegible = False
    if user.id == miembro.idUser.id:
        elegible = True
    return elegible