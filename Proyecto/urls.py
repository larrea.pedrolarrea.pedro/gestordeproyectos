from django.urls import path
from .views import *
from Proyecto import views

app_name = 'Proyecto'
urlpatterns = [
    path('', views.index, name='index'),
    path('crear/', views.formCrearProyecto, name='crearProyecto'),
    path('crearRol/<int:id>', views.formCrearRolProyecto, name='crearRolProyecto'),
    path('editarRol/<int:id>', views.formEditarRolProyecto, name='editarRolProyecto'),
    path('verRol/<int:id>', views.formVerRolProyecto, name='verRolProyecto'),
    path('importar/<int:id>', views.listarProyectosRol, name='listarProyectosRol'),
    path('importar/<int:id>/<int:id_proyecto>', views.importarRolesProyectos, name='importarRolesProyectos'),
    path('editar/<int:id>', views.editProyecto, name='editarProyecto'),
    path('eliminar/<int:id>', views.deleteProyecto, name='eliminarProyecto'),
    path('guardarUs/<int:id>', FormularioNewUs, name="guardarUs"),
    path('usuarios/<int:id>', views.listaDeUsuarios, name="listaDeUsuarios"),

    # paths para cambiar el diseño de las pestañas para la vista de un proyecto
    path('ver/UsProyecto/<int:id>', views.UsProyecto, name="UsProyecto"),
    path('ver/DetallesProyecto/<int:id>', views.DetallesProyecto, name="DetallesProyecto"),
    path('ver/MiembrosProyecto/<int:id>', views.MiembrosProyecto, name="MiembrosProyecto"),
    path('ver/RolesProyecto/<int:id>', views.RolesProyecto, name="RolesProyecto"),
    # muestra el productBacklog de un proyecto
    path('ver/SprintProyecto/ListarUsSprintBacklog/<int:id>/<int:idSprint>', views.ListarUsSprintBacklog,
         name="ListarUsSprintBacklog"),
    # Agrega US como Sprint Planning
    path('ver/SprintProyecto/agregarSprintBacklog/<int:id>/<int:idSprint>/<int:idUs>', views.agregarSprintBacklog,
         name="agregarSprintBacklog"),
    # Para creacion de un nuevo sprint
    path('ver/SprintProyecto/Crear/<int:id>', views.CrearSprint, name="CrearSprint"),
    # listar sprint
    path('ver/SprintProyecto/<int:id>', views.ListarSprint, name="ListarSprint"),
    # ruta para verificar las cosas, previo a comenzar el sprint
    path('ver/SprintProyecto/VerificarSprint/<int:id>/<int:idSprint>', views.VerificarSprint, name="VerificarSprint"),

    # Url para agregar miembro a un proyecto (muestra solo los miembros que aun no estan en el proyecto)
    path('agregarMiembro/<int:id>/<int:socialUserId>', views.formCrearMiembro, name="agregarMiembros"),
    # Se muestran los datos del miembro del proyecto (rol y disponibilidad horaria)
    path('verMiembro/<int:id>/<int:miembroId>/<int:userAccId>', views.formVerMiembro, name="verMiembros"),
    # Se muestran los campos editables de un miembro en un proyecto (rol y disponibilidad horaria)
    path('editarMiembro/<int:id>/<int:miembroId>/<int:userAccId>', views.formEditarMiembro, name="editarMiembros"),
    # Boton para eliminar un miembro de un proyecto (rol y disponibilidad horaria)
    path('eliminarMiembro/<int:idProyecto>/<int:miembroId>', views.eliminarMiembroProyecto, name="eliminarMiembros"),

    # Boton para reemplazar un miembro de un proyecto
    path('reemplazarMiembro/<int:idProyecto>/<int:miembroViejoId>/<int:idPP>', views.reemplazarMiembroProyecto,
         name="reemplazarMiembro"),

    # Boton para reemplazar un miembro de un proyecto sin eliminarlo, solo reemplazarlo en una US
    path('reemplazarMiembroEnUS/<int:idProyecto>/<int:idUS>', views.reemplazarMiembroEnUSProyecto,
         name="reemplazarMiembroEnUS"),

    # Se reemplaza al US
    path('cambioDeMiembroAlReemplazarEnUs/<int:idProyecto>/<int:userNuevoId>/<int:idPP>',
         views.cambioDeMiembroAlReemplazarEnUs, name="cambioDeMiembroAlReemplazarEnUs"),

    # Al mostrarse las historias de usuario, se deben reemplazar por otro usuario activo del proyecto, esto posibles ususarios son mostrados con esta url
    path('cambioDeMiembroAlEliminar/<int:idProyecto>/<int:miembroViejoId>/<int:userNuevoId>/<int:idPP>',
         views.cambioDeMiembroAlEliminar, name="cambioDeMiembroAlEliminar"),

    # La ruta 'detalles' en donde mostraremos una página con los detalles de un us o registro
    # aqui se pasan por url el parametro que es el id que se requiere para recuperar un reigstro
    path('ver/detalle/<int:id>', UsDetalle.mostrarDetalle, name='detalle'),

    # La ruta 'actualizar' en donde mostraremos un formulario para actualizar un  registro de la Base de Datos
    # aqui se pasan por url el parametro que es el id que se requiere para recuperar un registro
    path('ver/actualizar/<int:id>', UsActualizar.editar, name='actualizar'),

    # para listar el SPrintBacklog de un sprint en planificacion
    path('ver/SprintProyecto/SprintBacklog/<int:id>/<int:idSprint>', views.ListarSprintBacklog,
         name="ListarSprintBacklog"),

    # para editar una planning poker
    path('ver/SprintProyecto/SprintBacklog/EditarPP/<int:id>/<int:idSprint>/<int:idPP>/<int:idUs>', views.EditarPP,
         name="EditarPlanningPoker"),

    # para quitar un US de un sprintbacklog
    path('ver/SprintProyecto/SprintBacklog/<int:id>/<int:idSprint>/<int:idPP>/<int:idUs>', views.QuitarSprintBacklog,
         name="QuitarSprintBacklog"),

    path('kanban/<int:id>/<int:volver>', views.indexKanban, name="verKanban"),

    # path de PRUEBA
    path('miembroprueba/<int:id>', views.obtenerMiembro),

    # path cuando se desea agregar actividad
    path('kanban/agregarActividad/<int:id>/<int:idSprint>/<int:idUs>/<int:volver>', views.agregarActividad,
         name='agregarActividad'),

    # path para cuando se desea ver el historial de modificaciones
    path('kanban/historialMod/<int:idUs>', views.historialMod, name='historialMod'),

    # path para cuando se desea ver el historial
    path('kanban/historialUs/<int:id>/<int:idUs>/<int:volver>/<int:kanbanvolver>/<int:idSprint>', views.historialUs,
         name='historialUs'),

    # Retorna un json con los datos necesarios para el chart
    path('burndownchartjson/<int:idSprint>', views.burndownChart, name='burndownChartjson'),

    # Despliega el burndown del sprint correspondiente
    path('burndownchart/<int:idSprint>', views.burndownChartIndex, name='burndownChart'),

    # Confirmacion de Finalizar un proyecto
    path('ver/confirmarFinalizarProyecto/<int:id>', views.confirmarFinalizarProyecto,
         name='confirmarFinalizarProyecto'),

    # Finalizar un proyecto, por el ID
    path('ver/finalizarProyecto/<int:id>', views.finalizarProyecto, name='finalizarProyecto'),

    # path para desplegar el resumen cuando se termina un sprint
    path('ver/resumenFinSprint/<int:id>/<int:idSprint>', views.resumenFinSprint, name='resumenFinSprint'),

    # path para terminar el sprint y hacer algunos ajustes en los datos
    path('ver/resumen/<int:id>/<int:idSprint>', views.resumenFinSprint, name='resumenFinSprint'),
    path('SprintProyecto/<int:id>/<int:idSprint>', views.finSprint, name='finSprint'),

    # path para editar una actividad
    path(
        'kanban/historialUs/editar/<int:id>/<int:idSprint>/<int:idUs>/<int:volver>/<int:idActividad>/<int:volverkanban>',
        views.editarActividadUs, name='editarActividadUs'),

    # path para los pdf
    # para listar el Sprint Backlog de un sprint en planificacion
    path('listUserStoryPdf/<int:idSprint>/', views.listUserStoryPdf, name="listUserStoryPdf"),

    # para listar el Product Backlog de un proyecto
    path('listProductBacklogPdf/<int:idProyecto>/', views.listProductBacklogPdf, name="listProductBacklogPdf"),

    # para listar los US del sprint actual de un proyecto
    path('listUsSprintActualPdf/<int:idProyecto>/', views.listUsSprintActualPdf, name="listUsSprintActualPdf"),

    # para ver los miembros de un sprint
    path('miembroSprint/<int:idSprint>/', views.miembroEnSprint, name="miembrosSprint"),

]
