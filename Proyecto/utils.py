from xhtml2pdf import pisa
from django.http import HttpResponse
from io import BytesIO
from django.template.loader import get_template

def render_to_pdf(template_src, context={}):
    template = get_template(template_src)
    codigo_html = template.render(context)
    resultado = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(codigo_html.encode("ISO-8859-1")), resultado)
    if not pdf.err:
        return HttpResponse(resultado.getvalue())

    return None