import threading
from webbrowser import get
from django.views.generic import ListView, View
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import get_template
from requests import request
from Proyecto.utils import render_to_pdf
from Proyecto.forms import ProyectosFormCrear, ProyectosFormEditar
from Proyecto.models import Proyectos, Roles, Sprint
from django.contrib import messages
from django.http import HttpResponse
from django.http import HttpRequest, JsonResponse
from Proyecto.forms import *  # import del formulario(forms.py de APP Proyecto)
from .models import userStory  # import del modelo a utilizar
from allauth.socialaccount.models import SocialAccount
from django.contrib.auth.models import User
import json
from django.http import HttpResponse
# Nos sirve para redireccionar despues de una acción revertiendo patrones de expresiones regulares
from django.urls import reverse
from . import views

import datetime

# Importar para enviar correo
from django.conf import settings
from django.core.mail import EmailMultiAlternatives, send_mail


# Vista principal de proyectos
def index(request):
    # Lista de todos los proyectos
    listProyectos = obtenerlistaDeProyectosUser(request)
    idSprintActivo = []
    for i in listProyectos:
        sprint = Sprint.objects.filter(idProyecto=i, estado='E').first()
        if sprint:
            idSprintActivo.append(sprint.id)
        else:
            idSprintActivo.append(None)
    context = {
        'listProyectos': listProyectos,
        'listaSprint': idSprintActivo,
    }

    return render(request, 'Proyecto/index.html', context, None, 200)


# Metodo para obtener lista de Proyectos a la que perteneces un usuario de sistema
def obtenerlistaDeProyectosUser(request):
    listaDeProyectos = list(Proyectos.objects.filter(scrumMaster=request.user))
    Miembro = miembros.objects.filter(idUser=request.user, activo=True)
    for x in Miembro:
        if not x.idProyecto in listaDeProyectos:
            listaDeProyectos.append(x.idProyecto)
    listaDeProyectos.sort(key=nombreProyecto)
    return listaDeProyectos


# Metodo para obtener Nombre de un proyecto
def nombreProyecto(e):
    return e.nombre


# Formulario para crear un nuevo proyecto
def formCrearProyecto(request):
    if request.method == 'POST':
        formProyecto = ProyectosFormCrear(request.POST)
        if formProyecto.is_valid():
            formProyecto.save()
            # crear el rol SM por debajo
            proyecto = Proyectos.objects.order_by('-id')[0]
            idRol = crearRolSm(proyecto)
            # asignar el rol al miembro SM que se asigno
            asignarRolSm(proyecto, proyecto.scrumMaster, idRol)

            return redirect('Proyecto:index')
    else:
        formProyecto = ProyectosFormCrear()
        formProyecto.fields["scrumMaster"].queryset = User.objects.all().exclude(username='admin')

    context = {'formProyecto': formProyecto}

    return render(request, 'Proyecto/create.html', context, None, 200)


# Ver Pestaña de Detalles del Proyecto
def DetallesProyecto(request, id):
    proyecto = get_object_or_404(Proyectos, pk=id)

    context = {
        'proyecto': proyecto,
        'idProyecto': id,
    }

    return render(request, 'Proyecto/viewDetalles.html', context)


# eliminar un Proyecto
def deleteProyecto(request, id):
    return None


# Formulario para crear un nuevo rol
def formCrearRolProyecto(request, id):
    # para guardar
    if request.method == 'POST':
        formRolProyecto = RolProyectoFormCrear(request.POST)
        if formRolProyecto.is_valid():
            formRolProyecto.save()
            return redirect('Proyecto:RolesProyecto', id)
    # para desplegar el formulario
    else:
        proyecto = get_object_or_404(Proyectos, pk=id)
        formRolProyecto = RolProyectoFormCrear()
        formRolProyecto.fields["idProyecto"].initial = proyecto
        # Prueba para saber si se puede asignar idProyecto a la creacion del rol
        # idProyecto = Proyectos.objects.get(pk=id)
        # formRolProyecto.idProyecto = idProyecto
        # print(f'Entre aqui proyectoid {id} Projecto.objecto: {idProyecto}')
    context = {
        'formRolProyecto': formRolProyecto,
        'idProyecto': id
    }

    return render(request, 'Proyecto/Rol/createRol.html', context, None, 200)


# Edita los Datos del rol de un Proyecto
def formEditarRolProyecto(request, id):
    rol = get_object_or_404(Roles, pk=id)
    if request.method == 'POST':
        # Si es que hace post (al guardar el form)
        formEditRolProyecto = RolProyectoFormEditar(request.POST, instance=rol)
        if formEditRolProyecto.is_valid():
            formEditRolProyecto.save()
            return redirect('Proyecto:RolesProyecto', rol.idProyecto.id)
    else:
        # !Si es que hace post (al guardar el form)
        formEditRolProyecto = RolProyectoFormEditar(instance=rol)
        context = {
            'formEditRolProyecto': formEditRolProyecto,
            'idProyecto': rol.idProyecto.id
        }
    return render(request, 'Proyecto/Rol/editRol.html', context)


# Ver los Datos del rol de un Proyecto
def formVerRolProyecto(request, id):
    rol = Roles.objects.get(pk=id)
    if request.method == 'POST':
        # Si es que hace post (al guardar el form)
        formVerRolProyecto = RolProyectoFormVer(request.POST, instance=rol)
        if formVerRolProyecto.is_valid():
            formVerRolProyecto.save()
            return redirect('Proyecto:RolesProyecto', rol.idProyecto.id)
    else:
        formVerRolProyecto = RolProyectoFormVer(instance=rol)
        context = {
            'formVerRolProyecto': formVerRolProyecto,
            'rol': rol,
            'idProyecto': rol.idProyecto.id
        }
    return render(request, 'Proyecto/Rol/verRol.html', context)


# Ver Pestaña de Detalles del Proyecto
def listarProyectosRol(request, id):
    listProyectos = Proyectos.objects.order_by('-nombre')
    context = {
        'proyectos': listProyectos,
        'idProyecto': id,
    }
    return render(request, 'Proyecto/Rol/importarRol.html', context)


# Permite importar roles de un proyecto a otro
def importarRolesProyectos(request, id, id_proyecto):
    listaRolesImportar = Roles.objects.all().filter(idProyecto_id=id_proyecto)
    listaRolesProyecto = Roles.objects.all().filter(idProyecto_id=id)
    proyecto = get_object_or_404(Proyectos, pk=id)
    mensaje = ""
    for ri in listaRolesImportar:
        bandera = True
        for rp in listaRolesProyecto:
            if ri.nombre == rp.nombre:
                bandera = False
        if bandera:
            r = Roles(idProyecto=proyecto, nombre=ri.nombre, descripcion=ri.descripcion,
                      agregarUserStory=ri.agregarUserStory, eliminarUserStory=ri.eliminarUserStory,
                      modificarUserStory=ri.modificarUserStory,
                      agregarMiembro=ri.agregarMiembro, modificarMiembro=ri.modificarMiembro,
                      eliminarMiembro=ri.eliminarMiembro, crearRol=ri.crearRol, modificarRol=ri.modificarRol,
                      eliminarRol=ri.eliminarRol, crearSprint=ri.crearSprint, empezarSprint=ri.empezarSprint,
                      finalizarSprint=ri.finalizarSprint, agregarSprintBacklog=ri.agregarSprintBacklog,
                      modificarSprintBacklog=ri.modificarSprintBacklog)
            r.save()
    return redirect('Proyecto:RolesProyecto', id)


# Ver Pestaña de Roles del Proyecto
def RolesProyecto(request, id):
    listRoles = Roles.objects.all().filter(idProyecto_id=id)
    context = {
        'listRoles': listRoles,
        'idProyecto': id,
    }
    return render(request, 'Proyecto/viewRoles.html', context)


# Edita los Datos de un Proyecto
def editProyecto(request, id):
    proyecto = get_object_or_404(Proyectos, pk=id)
    if request.method == 'POST':
        formEditProyecto = ProyectosFormEditar(request.POST, instance=proyecto)
        if formEditProyecto.is_valid():
            print("hola")
            # para actualizar el miembro con el rol "SM_under", sucede cuando se edita el proyecto y se cambia el scrum master
            miembro = miembros.objects.filter(idProyecto=id, idUser=request.user).first()
            print("miembro id: ", miembro.id)
            # guarda el proyecto, el cual se modifico los campos, probablemente el SM
            # al guardar la actualizacion capaz ya se cambia el SM
            formEditProyecto.save()
            # obtenemos el proyecto que se acaba de guardar
            proyecto = Proyectos.objects.get(pk=id)
            print("proyecto id: ", proyecto.id)
            # verificamos si se cambio el scrum master
            if miembro.idUser != proyecto.scrumMaster:
                print("son distintos")
                # se debe borrar el miembro que va a ser ahora scrum master, para que no tenga 2 roles en el proyect
                try:
                    miembroEliminar = miembros.objects.get(idProyecto=id, idUser=proyecto.scrumMaster)
                    miembroEliminar.delete()
                except:
                    pass
                # al miembro se le actualiza el user, que en este caso es el Scrum Master por que se ha cambiado de scrum
                miembro.idUser = proyecto.scrumMaster
                # se guarda el miembro
                miembro.save()
            return redirect('Proyecto:index')
    else:
        formEditProyecto = ProyectosFormEditar(instance=proyecto)
        # obtengo el sprint activo actual del proyecto
        sprintActivo = Sprint.objects.filter(idProyecto=id, estado='E').first()

        if sprintActivo:
            # si obtiene, hace la consulta que la excluye a los user que tienen trabajo
            formEditProyecto.fields["scrumMaster"].queryset = User.objects.all().exclude(
                miembros__planningpoker__idSprint=sprintActivo).exclude(username='admin')
        else:
            # si no, obtiene todos los user
            formEditProyecto.fields["scrumMaster"].queryset = User.objects.all().exclude(username='admin')

    return render(request, 'Proyecto/edit.html', {'formEditProyecto': formEditProyecto})


# Formulario para crear un nuevo US
def FormularioNewUs(request, id):
    # post, para cuando ya se hace la solicitud de guardar
    if request.method == 'POST':
        formUs = FormularioUs(request.POST)
        if formUs.is_valid():
            formUs.save()
            return redirect('Proyecto:UsProyecto', id)
    else:
        # solicitud de despliegue
        proyecto = get_object_or_404(Proyectos, pk=id)
        formUs = FormularioUs()
        # despliega el formulario identificando a que proyecto pertenece la nueva US
        formUs.fields[
            "idProyecto"].initial = proyecto
        formUs.fields["encargado"].queryset = User.objects.all().exclude(username='admin')

    context = {'formUs': formUs, "idProyecto": id}

    return render(request, 'Proyecto/Us/UsCrear.html', context, None, 200)


# Metodos que sirven para mostar informacion referente a un proyecto
# Ver Proyecto
# Muestra vista de los detalles de la persona X
def obtenerPermisosProyecto(request, proyecto):
    # obtenemos los datos del miembro
    miembro = miembros.objects.all().filter(idProyecto_id=proyecto, idUser=request.user).first()
    permisos = []
    # si el user es miembro del proyecto
    if miembro:
        rol = miembro.idRol
        # cargar permisos
        permisos.append(rol.agregarUserStory)
        permisos.append(rol.eliminarUserStory)
        permisos.append(rol.modificarUserStory)
        permisos.append(rol.agregarMiembro)
        permisos.append(rol.modificarMiembro)
        permisos.append(rol.eliminarMiembro)
        permisos.append(rol.crearRol)
        permisos.append(rol.modificarRol)
        permisos.append(rol.eliminarRol)
        permisos.append(rol.crearSprint)
        permisos.append(rol.empezarSprint)
        permisos.append(rol.finalizarSprint)
        permisos.append(rol.agregarSprintBacklog)
        permisos.append(rol.modificarSprintBacklog)

    else:
        # verificamos si el user es scrum master
        if request.user == proyecto.scrumMaster:
            # cargar permisos
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
            permisos.append(True)
    # retornamos una lista de permisos o una lista vacia
    return permisos


# Ver Pestaña de US del Proyecto
def UsProyecto(request, id):
    listUs = userStory.objects.filter(idProyecto=id).order_by('prioridad')
    # obtenemos las US de que filtradas por el id del proyecto y ordenadas por el nombre
    proyecto = get_object_or_404(Proyectos, pk=id)
    permisos = obtenerPermisosProyecto(request, proyecto)
    context = {
        'listUs': listUs,
        'idProyecto': id,
        'permisos': permisos,
        'proyecto': proyecto,
    }

    return render(request, 'Proyecto/viewUs.html', context)


# Ver Pestaña de Detalles del Proyecto
def DetallesProyecto(request, id):
    proyecto = get_object_or_404(Proyectos, pk=id)
    permisos = obtenerPermisosProyecto(request, proyecto)
    context = {
        'proyecto': proyecto,
        'idProyecto': id,
        'permisos': permisos
    }

    return render(request, 'Proyecto/viewDetalles.html', context)


# Lista Roles dentro de un proyecto
def RolesProyecto(request, id):
    # el exclude al final omite que se enliste el rol SM_under
    listRoles = Roles.objects.all().filter(idProyecto_id=id).exclude(nombre="SM_under")
    proyecto = get_object_or_404(Proyectos, pk=id)
    permisos = obtenerPermisosProyecto(request, proyecto)

    context = {
        'listRoles': listRoles,
        'idProyecto': id,
        'permisos': permisos,
        'proyecto': proyecto,
    }
    return render(request, 'Proyecto/viewRoles.html', context)


# Lista miembros dentro de un proyecto
def MiembrosProyecto(request, id):
    # Obtener los objetos necesarios para pasar como contexto
    proyecto = get_object_or_404(Proyectos, pk=id)
    # se obtiene el rol SM_under para no mostrar
    rol = Roles.objects.filter(idProyecto=id, nombre="SM_under")[0]
    # ze excluye el rol
    listaMiembros = miembros.objects.filter(idProyecto=proyecto).exclude(idRol=rol)
    permisos = obtenerPermisosProyecto(request, proyecto)
    horas = []
    for element in listaMiembros:
        horaTotal = element.lunes + element.martes + element.miercoles + element.jueves + element.viernes + element.sabado + element.domingo
        horas.append(horaTotal)

    context = {
        'proyecto': proyecto,
        'idProyecto': id,
        'miembrosProyecto': listaMiembros,
        'horasMiembros': horas,
        'permisos': permisos
    }

    return render(request, 'Proyecto/viewMiembros.html', context)


# Lista Sprints de un proyecto
def SprintProyecto(request, id):
    # Obtener los objetos necesarios para pasar como contexto
    # Aqui las cosas de chino para mostrar cosas referentes al sprint
    proyecto = get_object_or_404(Proyectos, pk=id)
    permisos = obtenerPermisosProyecto(request, proyecto)
    context = {
        'idProyecto': id,
        'permisos': permisos
    }

    return render(request, 'Proyecto/viewSprint.html', context)


# clase de los metodos para ver los detalles de un US
class UsDetalle():
    # funcion para desplegar la vista donde se muestran los datos de un US
    def mostrarDetalle(request, id):
        # Llamamos a la clase que se encuentra en nuestro archivo 'models.py'
        model = get_object_or_404(userStory, pk=id)
        id = model.idProyecto.id
        formUs = FormularioUs(instance=model)
        # retorna el render con la vista y datos a exponer
        return render(request, "Proyecto/Us/UsDetalles.html", {"us": model, "idProject": id, "formUs": formUs})


# clase de los metodos para actualizar un US
class UsActualizar():
    # Edita los Datos de un US
    def editar(request, id):
        # recibe el id de la US que se va a editar
        # obtiene el registro que se va a editar
        us = get_object_or_404(userStory, pk=id)
        # para cuando edito y guarda(POST)
        id = us.idProyecto.id
        if request.method == 'POST':
            formUs = FormularioEditUs(request.POST, instance=us)
            # verifica si los datos son validos en el formulario
            if formUs.is_valid():
                # verifica que los datos sean validos
                formUs.save()
                # guarda

                return redirect('Proyecto:UsProyecto', id)  # retorna la url de la vista
        else:
            # cuando debe traer al formulario los datos (GET)
            formUs = FormularioEditUs(instance=us)
            formUs.fields["encargado"].queryset = User.objects.all().exclude(username='admin')

        return render(request, 'Proyecto/Us/UsActualizar.html', {'formUs': formUs, "mensaje": "OK", "idProject": id})


# Miembro
# Form para agregar un miembro a un proyecto, se puede asignar un rol y su disponibilidad horaria
def formCrearMiembro(request, id, socialUserId):
    socialUser = get_object_or_404(SocialAccount, pk=socialUserId)
    user = User.objects.get(email__icontains=socialUser.extra_data['email'])
    proyecto = get_object_or_404(Proyectos, pk=id)
    miembro = miembros.objects.filter(idUser=user.id, idProyecto=proyecto).first()

    if request.method == 'POST':
        # Se muestra el cuadro de crear miembro
        formMiembrosProyecto = FormularioMiembros(request.POST)
        if formMiembrosProyecto.is_valid():
            formMiembrosProyecto.save()

            miembro = miembros.objects.filter(idUser=user.id, idProyecto=proyecto).first()

            # Enviar Correo notificando que a sido agregado al proyecto como miembro
            url = settings.PATH_HOME + 'proyectos/verMiembro/' + str(id) + '/' + str(miembro.id) + '/0'
            nombre = miembro.idUser.first_name + ' ' + miembro.idUser.last_name
            thread = threading.Thread(target=enviarMailANuevoMiembro, args=(
                miembro.idUser.email, "Ha sido agregado como Miembro en un Proyecto",
                nombre, url, proyecto.nombre, miembro.idRol.nombre))
            thread.start()
        return redirect('Proyecto:MiembrosProyecto', id)

    else:
        if miembro:
            miembro.activo = True
            # Se muestra el cuadro de editar
            miembro.save()

            # Enviar Correo notificando que a sido agregado al proyecto como miembro
            url = settings.PATH_HOME + 'proyectos/verMiembro/' + str(id) + '/' + str(miembro.id) + '/0'
            thread = threading.Thread(target=enviarMailANuevoMiembro, args=(
                miembro.idUser.email, "Ha sido agregado como Miembro en un Proyecto",
                miembro.idUser.username, url, proyecto.nombre, miembro.idRol.nombre))
            thread.start()

            return redirect('Proyecto:editarMiembros', id, miembro.id, miembro.idUser.id)

        else:
            formMiembrosProyecto = FormularioMiembros()
            formMiembrosProyecto.fields["idProyecto"].initial = proyecto
            formMiembrosProyecto.fields["idUser"].initial = user
            formMiembrosProyecto.fields["activo"].initial = True

            # se busca el rol SM_under que se excluye
            rol = Roles.objects.filter(idProyecto=id, nombre="SM_under")[0]
            # se excluye el rol
            formMiembrosProyecto.fields["idRol"].queryset = Roles.objects.filter(idProyecto=proyecto).exclude(
                nombre="SM_under")

            context = {
                'formMiembroProyecto': formMiembrosProyecto,
                'idProyecto': id,
                'Usuario': user
            }

            return render(request, 'Proyecto/Miembro/crearMiembro.html', context, None, 200)


# Dar de baja a un miembro dentro de un proyecto, validando las cosas pendientes que tiene el miembro. sprint, us, atiividades
def eliminarMiembroProyecto(request, idProyecto, miembroId=None):
    miembroInactivo = miembros.objects.get(id=miembroId)
    HistoriaUsuarios = userStory.objects.filter(idProyecto=idProyecto)

    # Trae todos los planning poker que esten relacionados con miembros id y cuyos Sprints sean Pendientes o En Curso
    InactivoDirecto = PlanningPoker.objects.filter(miembroEncargado=miembroId).exclude(idSprint__estado='T').exclude(
        idSprint__estado='C')

    if InactivoDirecto:
        entra = False
        for x in InactivoDirecto:
            # Se verifica que las historias de usuario del miembro sean Pendiente, En Proceso, Hecho
            # En esos casos, el miembro necesitara ser reemplazado
            if x.idUs.estado == 'P' or x.idUs.estado == 'PP' or x.idUs.estado == 'EP' or x.idUs.estado == 'H':
                entra = True
                # reemplazar
                listaUSporUser = PlanningPoker.objects.filter(miembroEncargado=miembroId).exclude(
                    idSprint__estado='T').exclude(
                    idSprint__estado='C').exclude(idUs__estado='N').exclude(idUs__estado='A').exclude(idUs__estado='C')
                listaDeUsarios = SocialAccount.objects.order_by('id')
                context = {
                    'idProyecto': idProyecto,
                    'miembroId': miembroId,
                    'miembroPorEliminarId': miembroId,
                    'miembro': miembroInactivo,
                    'listaUSporUser': listaUSporUser,
                    'listaDeUsarios': listaDeUsarios
                }
                return render(request, 'Proyecto/Miembro/listaruserUS.html', context, None, 200)
        # Si no se necesita reemplazo para el miembro, se lo pone como miembro del proyecto inactivo
        if not entra:
            miembroInactivo.activo = False
            miembroInactivo.save()
    else:
        # Si no se necesita reemplazo para el miembro, se lo pone como miembro del proyecto inactivo
        miembroInactivo.activo = False
        miembroInactivo.save()

    return redirect('Proyecto:MiembrosProyecto', idProyecto)


# Funcion para reemplazar un miembro de un proyecto
# Si hay historias de usuario de ese miembro que se deberian reemplazar por otro miembro, se mostrara por pantalla
def reemplazarMiembroProyecto(request, idProyecto, miembroViejoId=None, idPP=None):
    PP = PlanningPoker.objects.get(pk=idPP)
    miembroPorEliminar = miembros.objects.get(id=miembroViejoId)
    listaDeUsarios = SocialAccount.objects.order_by('id')

    if request.method == 'POST':
        return redirect('Proyecto:MiembrosProyecto', idProyecto)

    context = {
        'PP': PP,
        'idProyecto': idProyecto,
        'miembroPorEliminarId': miembroViejoId,
        'miembroPorEliminar': miembroPorEliminar,
        'listaDeUsarios': listaDeUsarios
    }

    return render(request, 'Proyecto/Miembro/reemplazarUsuario.html', context, None, 200)


# Metodo para reemplar miembro por otro nuevo
def reemplazarMiembroEnUSProyecto(request, idProyecto, idUS=None):
    PP = PlanningPoker.objects.filter(idUs=idUS).order_by('idSprint').last()
    US = userStory.objects.get(pk=idUS)
    miembro = miembros.objects.filter(idProyecto=idProyecto, idUser=US.encargado_id).first()
    Proyecto = get_object_or_404(Proyectos, pk=idProyecto)
    listaDeUsarios = SocialAccount.objects.order_by('id')

    context = {
        'PP': PP,
        'miembro': PP.miembroEncargado,
        'idMiembro': PP.miembroEncargado.id,
        'encargado_id': US.encargado_id,
        'listaDeUsarios': listaDeUsarios,
        'US': US,
        'idProyecto': idProyecto
    }

    return render(request, 'Proyecto/Us/reemplazarEnUS.html', context, None, 200)


# Cuando se desea reemplazar a un miembro en una US
def cambioDeMiembroAlReemplazarEnUs(request, idProyecto, userNuevoId=None, idPP=None):
    # Se cambia el miembro encargado del planning poker
    miembroNuevo = miembros.objects.get(idUser=userNuevoId, idProyecto=idProyecto)
    PlanningPokerCambiarEncargado = PlanningPoker.objects.filter(id=idPP).first()
    # registro quien era el anterior miembro para registrar el cambio
    miembroViejo = PlanningPokerCambiarEncargado.miembroEncargado
    # Se reemplaza el miembro
    PlanningPokerCambiarEncargado.miembroEncargado = miembroNuevo
    # Se guarda el Planning Poker con el miembro nuevo
    PlanningPokerCambiarEncargado.save()
    # se actualiza la US
    usCambiar = PlanningPokerCambiarEncargado.idUs
    # Se cambia el User Story con el miembro nuevo
    usCambiar.encargado = miembroNuevo.idUser
    # Se guarda el User Story con el miembro nuevo
    usCambiar.save()

    # ahora se registra la modificacion
    registroModificacion = registrarModificacionBase(request, idProyecto)
    # se especifica el cambio
    registroModificacion.detalle = "Cambio de encargado.\nAnterior: %s %s\nNuevo: %s %s" % (
        miembroViejo.idUser.first_name, miembroViejo.idUser.last_name, miembroNuevo.idUser.first_name,
        miembroNuevo.idUser.last_name)
    # se relacion la US con el registro
    registroModificacion.idUs = usCambiar
    # se guarda el registro
    registroModificacion.save()

    return redirect('Proyecto:UsProyecto', idProyecto)


def cambioDeMiembroAlEliminar(request, idProyecto, miembroViejoId=None, userNuevoId=None, idPP=None):
    # Se cambia el miembro encargado del planning poker
    miembroInactivo = miembros.objects.get(id=miembroViejoId)
    miembroNuevo = miembros.objects.get(idUser=userNuevoId, idProyecto=idProyecto)
    PlanningPokerCambiarEncargado = PlanningPoker.objects.filter(id=idPP).first()
    # Se reemplaza el miembro
    PlanningPokerCambiarEncargado.miembroEncargado = miembroNuevo
    # Se guarda con el miembro nuevo
    PlanningPokerCambiarEncargado.save()

    usCambiar = PlanningPokerCambiarEncargado.idUs
    usCambiar.encargado = miembroNuevo.idUser
    usCambiar.save()

    listaUSporUser = PlanningPoker.objects.filter(miembroEncargado=miembroViejoId).exclude(
        idSprint__estado='T').exclude(idSprint__estado='C')
    # Si aun hay Historias de usuario, se muestra por pantalla las historias de usuario que faltan reemplazar
    if listaUSporUser:
        entra = False
        for x in listaUSporUser:
            if x.idUs.estado == 'P' or x.idUs.estado == 'PP' or x.idUs.estado == 'EP' or x.idUs.estado == 'H':
                entra = True
                # reemplazar
                listaUSporUserFiltroUSEstado = PlanningPoker.objects.filter(miembroEncargado=miembroViejoId).exclude(
                    idSprint__estado='T').exclude(idSprint__estado='C').exclude(idUs__estado='N').exclude(
                    idUs__estado='A').exclude(idUs__estado='C')
                listaDeUsarios = SocialAccount.objects.order_by('id')
                context = {
                    'idProyecto': idProyecto,
                    'miembroId': miembroViejoId,
                    'miembroPorEliminarId': miembroViejoId,
                    'miembro': miembroInactivo,
                    'listaUSporUser': listaUSporUserFiltroUSEstado,
                    'listaDeUsarios': listaDeUsarios
                }
                return render(request, 'Proyecto/Miembro/listaruserUS.html', context, None, 200)
    # Si ya no hay Us por reemplazar, se vuelve inactivo el miembro
    miembroInactivo.activo = False
    # se guarda el miembro inactivo
    miembroInactivo.save()
    # Pagina de miembros
    return MiembrosProyecto(request, idProyecto)


# Form para ver un miembro de un proyecto, se visualiza su rol y su disponibilidad horaria
def formVerMiembro(request, id, miembroId, userAccId):
    miembro = get_object_or_404(miembros, pk=miembroId)
    if request.method == 'POST':
        formVerMiembrosProyecto = FormularioVerMiembros(request.POST)
        if formVerMiembrosProyecto.is_valid():
            formVerMiembrosProyecto.save()
            return redirect('Proyecto:MiembrosProyecto', id)
    else:
        proyecto = get_object_or_404(Proyectos, pk=id)
        formVerMiembrosProyecto = FormularioVerMiembros(instance=miembro)
        user = User.objects.get(pk=miembro.idUser.id)
        # SA = SocialAccount.objects.get(extra_data__email=user.email)

    context = {
        'formMiembroProyecto': formVerMiembrosProyecto,
        'idProyecto': id,
        'Usuario': user,
    }

    return render(request, 'Proyecto/Miembro/verMiembro.html', context, None, 200)


# Al querer agregar un usuario al proyecto, se visualizara una lista de usuarios disponibles
# disponibles significa que aun no este en el proyecto
def listaDeUsuarios(request, id):
    # lista de usuario del sistema autenticados por el sso
    listaDeUsarios = SocialAccount.objects.order_by('id')
    context = {
        'listaDeUsarios': listaDeUsarios,
        'idProyecto': id,
    }
    return render(request, 'Proyecto/Miembro/listarUsuarios.html', context)


# Form para editar un miembro a un proyecto, se puede asignar un nuevo rol o cambiar su disponibilidad horaria
def formEditarMiembro(request, id, miembroId, userAccId):
    miembro = get_object_or_404(miembros, pk=miembroId)

    if request.method == 'POST':
        formMiembrosProyecto = FormularioEditarMiembros(request.POST or None, instance=miembro)
        if formMiembrosProyecto.is_valid():
            formMiembrosProyecto.save()
            return redirect('Proyecto:MiembrosProyecto', id)
    else:
        formMiembrosProyecto = FormularioEditarMiembros(instance=miembro)

        proyecto = get_object_or_404(Proyectos, pk=id)
        # obtenemos los datos del usuario con el id que se obtiene como parametro
        user = User.objects.get(pk=miembro.idUser.id)

        # se busca el rol SM_under que se excluye
        rol = Roles.objects.filter(idProyecto=id, nombre="SM_under")[0]
        # se excluye el rol
        formMiembrosProyecto.fields["idRol"].queryset = Roles.objects.filter(idProyecto=proyecto).exclude(
            nombre="SM_under")

    context = {
        'formEditarMiembroProyecto': formMiembrosProyecto,
        'idProyecto': id,
        'Usuario': user
    }

    return render(request, 'Proyecto/Miembro/editMiembro.html', context, None, 200)


# carga la pag principal de los sprints que le pertenecen al proyecto X
def ListarSprint(request, id):
    proyecto = get_object_or_404(Proyectos, pk=id)
    countSprint = Sprint.objects.filter(idProyecto=id, estado='E').count()
    bandSprint = False
    if (proyecto):
        sprintlist = Sprint.objects.filter(idProyecto_id=id).order_by('numero')
        permisos = obtenerPermisosProyecto(request, proyecto)
        # verifica si hay un sprint activo, para evitar arrancar otro sprint al mismo tiempo
        for sl in sprintlist:
            if sl.estado == 'E':
                bandSprint = True
                break

        print(sprintlist)
        context = {
            'listarSprint': sprintlist,
            'idProyecto': id,
            'countSprintEnCurso': countSprint,
            'bandSprint': bandSprint,
            'proyecto': proyecto,
            'permisos': permisos,
        }
        return render(request, 'Proyecto/viewSprint.html', context)
    else:
        return None


# Crea un Sprint con los Datos basicos
def CrearSprint(request, id):
    cantSprintPend = Sprint.objects.filter(idProyecto=id, estado='P').count()
    if request.method == 'POST':
        # post, para cuando ya se hace la solicitud de guardar
        formSprint = FormularioNuevoSprint(request.POST)
        if formSprint.is_valid():
            formSprint.save()
            return redirect('Proyecto:ListarSprint', id)
    else:
        # solicitud de despliegue
        nroultimosprint = Sprint.objects.filter(
            idProyecto=id)
        # Aqui obtiene el numero del ultimo sprint, para hacer "autoincrementable"
        proyecto = get_object_or_404(Proyectos, pk=id)
        ultimoSprint = Sprint.objects.filter(idProyecto=proyecto).order_by("-numero").first()
        formSprint = FormularioNuevoSprint()
        if nroultimosprint:
            numero = nroultimosprint.order_by('numero').last().numero
            formSprint.fields["numero"].initial = numero + 1
        else:
            formSprint.fields["numero"].initial = 1

        formSprint.fields["idProyecto"].initial = proyecto
        # despliega el formulario identificando a que proyecto pertenece la nueva US
    context = {
        'cantSprintPend': cantSprintPend,
        'formSprint': formSprint,
        'idProyecto': id,
        'ultimoSprint': ultimoSprint,
    }

    return render(request, 'Desarrollo/Sprint/CrearSprint.html', context, None, 200)


# esto es productBacklog en realidad
def ListarUsSprintBacklog(request, id, idSprint):
    # Obtenemos las US de que filtradas por el id del proyecto y ordenadas por el nombre y con estado='N' Nuevo
    listUs = list(userStory.objects.filter(idProyecto=id, estado__in=['N', 'STSA']).order_by('prioridad'))

    context = {
        'listUs': listUs,
        'idProyecto': id,
        'idSprint': idSprint,
    }

    return render(request, 'Proyecto/SprintBacklog/viewListaUsSprintBacklog.html', context)


# Agregar un US al sprint Backlog
def agregarSprintBacklog(request, id, idSprint, idUs):
    # obtiene el Us
    us = get_object_or_404(userStory, pk=idUs)
    sprint = get_object_or_404(Sprint, pk=idSprint)
    proyecto = Proyectos.objects.filter(pk=id)
    miembro = miembros.objects.filter(idProyecto=id, idUser=request.user).first()
    print('agregarSprintBacklog')

    if request.method == 'POST':
        crearPlanningPokerForm = formCrearPlanningPoker(request.POST)

        if crearPlanningPokerForm.is_valid():

            # registrar el detalle de la modificacion del US(ingresa a planning)
            registroModificacion = registrarModificacionBase(request, id)
            # asociamos con la US la modificacion
            registroModificacion.idUs = us
            # registro de que ingresa a planificacion en el sprint x
            registroModificacion.detalle = "Ingresa a planificacion Sprint %d" % (sprint.numero)
            registroModificacion.save()

            # ahora registra los cambios de estado
            # Se cambia de estado de nuevo o sin terminar sprint anterior a en sprint
            # El estado viejo es pasado a String
            estadoViejoString = EstadoUserStory(us.estado)
            # El estado nuevo es pasado a String
            estadoNuevoString = 'En Planning Poker'

            # Se genera el historial de cambio de estado HistorialModificacion
            nuevoHM = HistorialModificacion(idUs=us, idMiembro=miembro,
                                            detalle="Cambio de Estado: %s --> %s" % (
                                                estadoViejoString, estadoNuevoString))
            tiempo = datetime.datetime.now()
            nuevoHM.fecha = tiempo.date()
            nuevoHM.hora = tiempo.time()
            # Se guarda en la base de datos
            nuevoHM.save()

            # crearPlanningPokerForm.save()
            planinng_poker = PlanningPoker()
            planinng_poker.prioridad = crearPlanningPokerForm['prioridad'].value()
            planinng_poker.estimacionSM = crearPlanningPokerForm['estimacionSM'].value()

            registroModificacion = registrarModificacionBase(request, id)
            registroModificacion.idUs = us
            registroModificacion.detalle = "Prioridad: %s Estimacion SM: %s horas" % (
                planinng_poker.prioridad, planinng_poker.estimacionSM)

            planinng_poker.estimacionEncargado = None
            if crearPlanningPokerForm['estimacionEncargado'].value() and crearPlanningPokerForm[
                'estimacionEncargado'].value() != '':
                planinng_poker.estimacionEncargado = crearPlanningPokerForm['estimacionEncargado'].value()
                registroModificacion.detalle += "Estimacion miembro: %s" % (planinng_poker.estimacionEncargado)

            # registra aqui por si ambas estimaciones las carga el SM
            registroModificacion.save()

            planinng_poker.estimacionFinal = None

            idMiembro = crearPlanningPokerForm['miembroEncargado'].value()
            planinng_poker.miembroEncargado = miembros.objects.get(pk=idMiembro)
            us.encargado = planinng_poker.miembroEncargado.idUser
            planinng_poker.idUs = us
            planinng_poker.idSprint = sprint
            # se registra el encargado
            registroModificacion.detalle += "Encargado: %s %s" % (us.encargado.first_name, us.encargado.last_name)
            registroModificacion.save()

            # Si tiene la estimacion del SM y Encargado se calcula directamente la estimacion Final y acutaliza Estimacion del US
            if planinng_poker.estimacionEncargado and float(planinng_poker.estimacionEncargado) > 0:
                planinng_poker.estimacionFinal = str(
                    (float(planinng_poker.estimacionSM) + float(planinng_poker.estimacionEncargado)) / 2)
                us.estimacion = planinng_poker.estimacionFinal
                registroModificacion = registrarModificacionBase(request, id)
                registroModificacion.idUs = us
                registroModificacion.detalle = "Estimacion final: %s horas" % (planinng_poker.estimacionFinal)
                registroModificacion.save()

                # Envio de Mail para notificar que se
                url = settings.PATH_HOME + 'proyectos/ver/detalle/' + str(us.id)
                thread = threading.Thread(target=enviarMailPP, args=(
                    us.encargado.email, 'Nuevo US Asignado', us.encargado.username, sprint.numero,
                    us.titulo, us.prioridad, url, True))
                thread.start()

            else:
                # nose  tiene estimacion del encargado, enviar mail para que estime
                thread = threading.Thread(target=enviarMailPP, args=(
                    us.encargado.email, 'Nuevo US Asignado, Estime', us.encargado.username, sprint.numero,
                    us.titulo, us.prioridad, settings.PATH_HOME, False))
                thread.start()

            # Reasignamos la Prioridad al UserStory
            us.prioridad = planinng_poker.prioridad
            # cambiamos el estado de la US a PP, en planning pocker
            us.estado = 'PP'
            planinng_poker.estado = us.estado
            planinng_poker.save()
            us.save()

            return redirect('Proyecto:ListarUsSprintBacklog', id, idSprint)
    else:
        crearPlanningPokerForm = formCrearPlanningPoker()
        crearPlanningPokerForm.fields["idUs"].initial = us.id
        crearPlanningPokerForm.fields["idSprint"].initial = sprint.id
        # se obtiene el rol SM_under para no mostrar
        rol = Roles.objects.filter(idProyecto=id, nombre="SM_under")[0]
        # ze excluye el rol de la paleta
        crearPlanningPokerForm.fields["miembroEncargado"].queryset = miembros.objects.filter(idProyecto=id).exclude(
            idRol=None).exclude(idRol=rol).exclude(activo=False)

    context = {
        'us': us,
        'idProyecto': id,
        'idSprint': idSprint,
        'formPlanningPoker': crearPlanningPokerForm,
    }

    return render(request, 'Proyecto/SprintBacklog/agregarSprintBacklog.html', context)

    # return None


# metodo para cuando se desea quitar una US de un sprint en planificacion
def QuitarSprintBacklog(request, id, idSprint, idUs, idPP):
    # obtiene el Us
    us = get_object_or_404(userStory, pk=idUs)

    # hay que verificar el estado anterior que tenia el US que puede ser N o STSA
    pp = PlanningPoker.objects.filter(idUs=idUs).exclude(idSprint=idSprint)
    # si retorna algo, entonces el US tuvo estimacion anteoriormente y por lo tanto su estado es STSA
    if pp:
        us.estado = 'STSA'
    # si no retorna nada, es porque es la primera vez en la que se le realiza un pp
    else:
        us.estado = 'N'

    # ponemos a nulo lso sgtes atributos
    us.estimacion = None
    us.encargado = None
    us.save()
    sprint = get_object_or_404(Sprint, pk=idSprint)

    # registrar la modificacion
    registroModificacion = registrarModificacionBase(request, id)
    registroModificacion.idUs = us
    registroModificacion.detalle = "Se quito de planificacion Sprint: %d" % (sprint.numero)
    registroModificacion.save()

    # Obtiene el planning poker correspondiente para eliminar
    try:
        pp = get_object_or_404(PlanningPoker, pk=idPP)
        pp.delete()
    except:
        pass

    return ListarSprintBacklog(request, id, idSprint)


# Verifica si este sprint puede iniciar, si puede inicia, sino emite mensaje
def VerificarSprint(request, id, idSprint):
    sprint = get_object_or_404(Sprint, pk=idSprint)

    # pal formulario de las fechas
    # post, para cuando ya se hace la solicitud de guardar
    if request.method == 'POST':
        formfechacambiada = FormAlterarFechaSprint(request.POST, instance=sprint)
        if formfechacambiada.is_valid():
            if puedeIniciarSprint(idSprint):
                # Cambiar estado del sprint
                sprint.estado = 'E'
                formfechacambiada.save()
                sprint.save()

                # Actualizar el estado del Proyecto a "En Curso"
                proyecto = sprint.idProyecto
                proyecto.estado = 'E'
                proyecto.save()

                listUs = PlanningPoker.objects.filter(idSprint=sprint)
                # Settear los usarStorys en estado pendiente y actualizar sus datos
                for pp in listUs:
                    us = pp.idUs
                    us.estado = 'P'
                    us.estimacion = pp.estimacionFinal
                    us.encargado = pp.miembroEncargado.idUser
                    us.prioridad = pp.prioridad
                    us.save()
                    pp.estado = 'P'
                    pp.save()
                    # ahora registramos que las US entran en el sprint para el historial
                    # se registra primero que ingreso al sprint nro x y luego se registran los cambios de estados respectivos de la US
                    registroModificacion = registrarModificacionBase(request, id)
                    registroModificacion.detalle = "ingreso al sprint numero %d" % (sprint.numero)
                    # asocia el US con el registro
                    registroModificacion.idUs = pp.idUs
                    # guarda el registro
                    registroModificacion.save()

                # Enviar correo de notificacion, a los miembros del Sprint, direccionando al kanban
                listaMiembros = miembros.objects.filter(idProyecto=proyecto, activo=True)
                url = settings.PATH_HOME + 'proyectos/kanban/' + str(sprint.id) + '/0'
                for miembro in listaMiembros:
                    thread = threading.Thread(target=enviarMailEmpiezaSprint, args=(
                        miembro.idUser.email, "Nuevo Sprint en Curso", miembro.idUser.username, url,
                        sprint.idProyecto.nombre, sprint.numero))
                    thread.start()

                return redirect('Proyecto:ListarSprint', id)
    else:
        # solicitud de despliegue
        proyecto = get_object_or_404(Proyectos, pk=id)
        formfechacambiada = FormAlterarFechaSprint()
        formfechacambiada.fields["idProyecto"].initial = id
        # despliega el formulario identificando a que proyecto pertenece la nueva US
        formfechacambiada.fields["numero"].initial = sprint.numero
        formfechacambiada.fields["fechaInicio"].initial = sprint.fechaInicio
        formfechacambiada.fields["fechaFin"].initial = sprint.fechaFin
        formfechacambiada.fields["estado"].initial = sprint.estado

    listPP = PlanningPoker.objects.filter(idSprint=idSprint)
    # obtiene los planning poker de un sprint
    horastrabajo = 0
    for pp in listPP:
        if pp.estimacionFinal:
            horastrabajo += pp.estimacionFinal
            # acumula para saber cuantas horas de trabajo son en total

    # todos los distintos en miembroEncargado, filtrado por idSprint
    listPP = PlanningPoker.objects.filter(idSprint=idSprint).distinct('miembroEncargado')

    cargasemanal = 0
    for pp in listPP:
        # print(pp.miembroEncargado)
        xd = get_object_or_404(miembros, pk=pp.miembroEncargado.id)
        # objeto miembro
        cargasemanal += xd.lunes + xd.martes + xd.miercoles + xd.jueves + xd.viernes + xd.sabado + xd.domingo

    # Listado Sprint Backlog(Planning poker)
    listUs = PlanningPoker.objects.filter(idSprint=sprint).order_by('prioridad')

    # para las advertencias

    context = {
        'idProyecto': id,
        'idSprint': idSprint,
        'sprint': sprint,
        'horastrabajo': horastrabajo,
        'formfecha': formfechacambiada,
        'tipoalerta': "alert-success",
        'cargasemanal': cargasemanal,
        # horas disponibles con los miembros incluidos dentro del sprint
        'listUs': listUs,
        # lista de US que le corresponde al Sprint
        'puedeIniciar': puedeIniciarSprint(idSprint),
    }

    return render(request, 'Desarrollo/Sprint/VerificacionSprint.html', context)


# Vista para inicar un Sprint
def IniciaSprint(request, id, idSprint):
    # E en curso un sprint
    sprint = get_object_or_404(Sprint, pk=idSprint)
    sprint.estado = 'E'
    listUs = PlanningPoker.objects.filter(idSprint=sprint)
    # Settear los usarStorys en estado pendiente
    for pp in listUs:
        us = pp.idUs
        us.estimacion = pp.estimacionFinal
        us.encargado = pp.miembroEncargado
        us.prioridad = pp.prioridad
        us.estado = 'P'
        print('us id', us.id)
        print('estimacion', us.estimacion)
        print('encargado', us.encargado)
        print('prioridad', us.prioridad)
        print('estado', us.estado)
        us.save()
        # ahora registramos que las US entran en el sprint para el historial
        # se registra primero que ingreso al sprint nro x y luego se registran los cambios de estados respectivos de la US
        registroModificacion = registrarModificacionBase(request, id)
        registroModificacion.detalle = ("ingreso al sprint numero ", sprint.numero)
        # asocia el US con el registro
        registroModificacion.idUs = pp.idUs.id
        registroModificacion.save()

    sprint.save()

    # Acutalizamo el estado del proyecto a "En Curso"
    proyecto = sprint.idProyecto
    proyecto.estado = 'E'
    proyecto.save()

    return redirect('Proyecto:ListarSprint', id)


# Funcion usada para verificar si un sprint puede ser iniciado o no
def puedeIniciarSprint(idSprint):
    sprint = Sprint.objects.get(pk=idSprint)
    listaPP = PlanningPoker.objects.filter(idSprint=sprint)
    # si tiene planningPockers y si es que tiene verifica si todas tiene estimacion
    if listaPP:
        for pp in listaPP:
            if not pp.estimacionFinal:
                return False
    else:
        return False

    return True


# Lista de US que estan detro un sprint X
def ListarSprintBacklog(request, id, idSprint):
    sprint = get_object_or_404(Sprint, pk=idSprint)
    listUs = PlanningPoker.objects.filter(idSprint=sprint).order_by('prioridad')
    context = {
        'sprint': sprint,
        'idProyecto': id,
        'idSprint': idSprint,
        'listUs': listUs,
    }

    return render(request, 'Desarrollo/Sprint/viewSprintBacklog.html', context)


def EditarPP(request, id, idSprint, idPP, idUs):
    pp = get_object_or_404(PlanningPoker, pk=idPP)
    us = get_object_or_404(userStory, pk=idUs)
    sprint = get_object_or_404(Sprint, pk=idSprint)

    oldPrioridad = pp.prioridad
    oldEstimacionEncargado = pp.estimacionEncargado
    oldEstimacionSM = pp.estimacionSM
    oldMiembroEncargado = miembros.objects.filter(pk=pp.miembroEncargado.id).first()

    if request.method == 'POST':

        # post, para cuando ya se hace la solicitud de guardar
        formppeditado = formCrearPlanningPoker(request.POST, instance=pp)

        # Datos
        if formppeditado['estimacionEncargado'].value():
            pp.estimacionEncargado = float(formppeditado['estimacionEncargado'].value())
        pp.estimacionSM = float(formppeditado['estimacionSM'].value())
        pp.miembroEncargado = miembros.objects.filter(pk=formppeditado['miembroEncargado'].value()).first()
        pp.prioridad = formppeditado['prioridad'].value()

        # se especifica el detalle
        if pp.estimacionEncargado != oldEstimacionEncargado:
            # se registra el cambio
            registroModificacion = registrarModificacionBase(request, id)
            # se relacion la US con el registro
            registroModificacion.idUs = us
            registroModificacion.detalle = 'Sprint %d Modificacion de planificacion Encargado Estimacion Anterior: %s Nueva: %s' % (
                sprint.numero, oldEstimacionEncargado, pp.estimacionEncargado)
            registroModificacion.save()
        if pp.estimacionSM != oldEstimacionSM:
            # se registra el cambio
            registroModificacion = registrarModificacionBase(request, id)
            # se relacion la US con el registro
            registroModificacion.idUs = us
            registroModificacion.detalle = "Sprint %d Modificacion de planificacion SM Estimacion: Anterior: %s  Nueva: %s" % (
                sprint.numero, oldEstimacionSM, pp.estimacionSM)
            registroModificacion.save()
        if pp.miembroEncargado != oldMiembroEncargado:
            # se registra el cambio
            registroModificacion = registrarModificacionBase(request, id)
            # se relacion la US con el registro
            registroModificacion.idUs = us
            registroModificacion.detalle = "Sprint %d Modificacion de Encargado Anterior: %s %s  Nuevo: %s %s" % (
                sprint.numero, oldMiembroEncargado.idUser.first_name, oldMiembroEncargado.idUser.last_name,
                pp.miembroEncargado.idUser.first_name, pp.miembroEncargado.idUser.last_name)
            registroModificacion.save()
        if pp.prioridad != oldPrioridad:
            # se registra el cambio
            registroModificacion = registrarModificacionBase(request, id)
            # se relacion la US con el registro
            registroModificacion.idUs = us
            registroModificacion.detalle = "Sprint %d Modificacion de planificacion Prioridad: Anterior: %s  Nueva: %s" % (
                sprint.numero, oldPrioridad, pp.prioridad)
            registroModificacion.save()

        if formppeditado.is_valid():
            formppeditado.save()

            # Setea encargado
            us.encargado = pp.miembroEncargado.idUser

            # si tiene las dos estimaciones, calcular la estimacion Final
            if pp.estimacionEncargado and pp.estimacionSM:
                pp.estimacionFinal = (pp.estimacionSM + pp.estimacionEncargado) / 2
                oldEstimacionFinal = us.estimacion
                us.estimacion = pp.estimacionFinal

                # se registra el cambio
                registroModificacion = registrarModificacionBase(request, id)
                # se relacion la US con el registro
                registroModificacion.idUs = us
                if oldEstimacionFinal is None:
                    oldEstimacionFinal = 0.0
                registroModificacion.detalle = "Sprint %d Modificacion de planificacion Estimacion Final: Anterior: %.1f  Nueva: %.1f" % (
                    sprint.numero, oldEstimacionFinal, us.estimacion)
                registroModificacion.save()
            else:
                oldEstimacionFinal = us.estimacion
                # Si no tiene, o se le quita la estimacionEncargado, no se tiene la estimacion final, settar None
                pp.estimacionFinal = None
                # se registra el cambio
                registroModificacion = registrarModificacionBase(request, id)
                # se relacion la US con el registro
                registroModificacion.idUs = us
                if oldEstimacionFinal is None:
                    oldEstimacionFinal = 0.0
                registroModificacion.detalle = "Sprint %d Modificacion de planificacion Estimacion Final: Anterior: %.1f  Nueva: Sin estimacion final" % (
                    sprint.numero, oldEstimacionFinal)
                registroModificacion.save()

            pp.save()
            us.prioridad = pp.prioridad
            us.save()

            return redirect('Proyecto:ListarSprintBacklog', id, idSprint)
    else:
        # solicitud de despliegue
        proyecto = get_object_or_404(Proyectos, pk=id)
        formppeditado = formCrearPlanningPoker(instance=pp)
        # se obtiene el rol SM_under para no mostrar
        rol = Roles.objects.filter(idProyecto=id, nombre="SM_under")[0]
        # ze excluye el rol de la paleta
        formppeditado.fields["miembroEncargado"].queryset = miembros.objects.filter(idProyecto=id, activo=True).exclude(
            idRol=None).exclude(idRol=rol)

    context = {
        'idProyecto': id,
        'idSprint': idSprint,
        'idPP': idPP,
        'idUs': idUs,
        'formppeditado': formppeditado,
        'us': us,

    }

    return render(request, 'Proyecto/PlanningPoker/EditarPlanningPoker.html', context)


# Vista para visualizar el kanban de un proyecto
def indexKanban(request, id, volver):
    sprint = Sprint.objects.filter(id=id).first()
    miembroDelUS = miembros.objects.get(idUser=request.user, idProyecto=sprint.idProyecto)
    proyecto = sprint.idProyecto
    # Cuando se queremos cambiar el estado de un spring planning en el kanban
    if request.method == 'POST':
        estado = request.POST.get("estado")
        PP = get_object_or_404(PlanningPoker, pk=request.POST.get("planningPoker"))
        US = PP.idUs
        actividades = ActividadUs.objects.filter(idUs=US, idSprint=sprint)
        cambiarEstado = True
        if estado == 'P' and actividades:
            cambiarEstado = False
        if (cambiarEstado):
            # El estado nuevo es pasado a String
            estadoNuevoString = EstadoUserStory(estado)
            # El estado viejo es pasado a String
            estadoViejoString = EstadoUserStory(US.estado)
            # Se genera el historial de cambio de estado HistorialModificacion
            nuevoHM = HistorialModificacion(idUs=US, hora=datetime.datetime.now().time(), idMiembro=miembroDelUS,
                                            detalle="Cambio de Estado: %s --> %s" % (
                                                estadoViejoString, estadoNuevoString))
            # Se guarda en la base de datos el historial de cambio de estado
            nuevoHM.save()
            US.estado = estado
            US.save()
            PP.estado = estado
            PP.save()
            if estado == "A":
                fechaAprobado = request.POST.get("fecha")
                usTerminado = ActividadUs(fecha=fechaAprobado, idSprint=sprint, idUs=US, idMiembro=PP.miembroEncargado,
                                          duracion=0, detalle="US_aprobada_actividad")
                usTerminado.save()

                # Email para notificar que el US a sido aprobado por el Scrum y de url,
                # muestre el historial de las actividades del mismo US
                url = settings.PATH_HOME + 'proyectos/kanban/historialUs/' + str(id) + '/' + str(US.id) + '/1/0/0'
                thread = threading.Thread(target=enviarMailUSAprobado, args=(
                    US.encargado.email, "US Aprobado por Scrum Master", US.encargado.username, url,
                    proyecto.nombre, sprint.numero, US.titulo))
                thread.start()
            else:
                eliminarActividad = ActividadUs.objects.filter(idSprint=sprint, idUs=US,
                                                               detalle="US_aprobada_actividad").first()
                if eliminarActividad:
                    eliminarActividad.delete()
    planningPokers = PlanningPoker.objects.filter(idSprint=sprint).order_by("prioridad")
    esScrumMaster = False
    if request.user == sprint.idProyecto.scrumMaster:
        esScrumMaster = True
    modificable = True
    if sprint.estado != 'E':
        modificable = False

    context = {
        'sprint': sprint,
        'planningPokers': planningPokers,
        'esScrumMaster': esScrumMaster,
        'modificable': modificable,
        'proyecto': proyecto,
        'volver': volver
    }
    return render(request, 'Proyecto/Kanban/KanbanSprint.html', context, None, 200)


# recuperar miembro
# aca obtengo el codigo del miembro
def obtenerMiembro(request, id):
    proyecto = get_object_or_404(Proyectos, pk=id)
    miembro = miembros.objects.filter(idProyecto=proyecto, idUser=request.user).first()

    return miembro


# metodo para cuando se agrega una nueva actividad sobre un uS
def agregarActividad(request, id, idSprint, idUs, volver):
    if request.method == 'POST':  # post, para cuando ya se hace la solicitud de guardar
        formActividad = FormActividadNueva(request.POST)
        # validacion del formulario
        if formActividad.is_valid():
            formActividad.save()
            actualizarTiempoDedicado(idUs)
            return redirect('Proyecto:verKanban', idSprint, volver)
    else:  # solicitud de despliegue
        proyecto = get_object_or_404(Proyectos, pk=id)
        miembro = miembros.objects.filter(idProyecto=proyecto, idUser=request.user).first()

        sprint = get_object_or_404(Sprint, pk=idSprint)
        us = get_object_or_404(userStory, pk=idUs)
        formActividad = FormActividadNueva()

        # Settea valor por Default al form
        formActividad.fields["idMiembro"].initial = miembro
        formActividad.fields["idSprint"].initial = sprint
        formActividad.fields["idUs"].initial = us

    context = {
        'formActividad': formActividad,
        'idProyecto': id,
        'idSprint': idSprint,
        'idUs': idUs,
        'idMiembro': miembro,
        'volver': volver
    }

    return render(request, "Proyecto/Actividad/newActividad.html", context)


# metodo para actualizar el tiempo total dedicado a la historia de usuario
def actualizarTiempoDedicado(idUs):
    us = get_object_or_404(userStory, pk=idUs)
    actividadesUs = ActividadUs.objects.filter(idUs=idUs)
    # acumulador
    ac = 0
    for actividad in actividadesUs:
        ac += actividad.duracion

    us.tiempoDedicado = ac
    us.save()


# metodo para listar todas las modificaciones que se realizaron sobre un us
def historialMod(request, idUs):
    US = get_object_or_404(userStory, pk=idUs)
    modificacionesUs = HistorialModificacion.objects.filter(idUs=idUs).order_by("fecha")
    context = {
        'US': US,
        'modificacionesUs': modificacionesUs,
        'idProyecto': US.idProyecto.id,
    }

    return render(request, "Proyecto/Actividad/historialModUs.html", context)


# metodo para listar todas las actividades que se realizaron sobre un us
def historialUs(request, id, idUs, volver, kanbanvolver, idSprint):
    actividadesUs = ActividadUs.objects.filter(idUs=idUs).order_by("fecha")
    context = {
        'actividadesUs': actividadesUs,
        'idProyecto': id,
        'volver': volver,
        'kanbanvolver': kanbanvolver,
        'idSprint': idSprint
    }

    return render(request, "Proyecto/Actividad/historialUs.html", context)


# Metodo que retorna un json con los datos necesarios para el burndown chart
def burndownChart(request, idSprint):
    response_data = {}
    # SE obtiene los datos del sprint
    sprint = Sprint.objects.get(id=idSprint)

    # Duracion en dias del sprint
    duracionDias = sprint.fechaFin - sprint.fechaInicio

    # Historias de usuario que se trabajan en este sprint
    planningPokersUs = PlanningPoker.objects.filter(idSprint=idSprint)
    # Sumatoria de las horas estimadas en los us del Sprint
    totalDeHoras = 0
    for p in planningPokersUs:
        totalDeHoras += p.estimacionFinal

    # lista que guarda las horas que faltan por dia
    horasRestantes = []
    label = []
    totalDeHorasAux = totalDeHoras
    lineaIdeal = []
    totalUS = planningPokersUs.count()
    tareasTerminadas = [0] * (duracionDias.days + 2)
    usRestantes = []
    for d in range(0, duracionDias.days + 2):
        label.append(sprint.fechaInicio + datetime.timedelta(days=d) - datetime.timedelta(days=1))

        # Se obtienen todas las actividades del sprint realizadas en una fecha en concreto
        actividades = ActividadUs.objects.filter(
            fecha=sprint.fechaInicio + datetime.timedelta(days=d) - datetime.timedelta(days=1),
            idSprint=idSprint).order_by("fecha")

        # Caculo de hora total de Actividades
        for a in actividades:
            if a.detalle == "US_aprobada_actividad":
                tareasTerminadas[d] += 1
                totalUS -= 1
            totalDeHoras -= a.duracion
        usRestantes.append(totalUS)
        horasRestantes.append(totalDeHoras)
        lineaIdeal.append((totalDeHorasAux / (duracionDias.days + 1)) * (duracionDias.days + 1 - d))

    # Setteo de datos por default al form
    response_data['labelsDias'] = label
    response_data['horasRestanteTrabajo'] = horasRestantes
    response_data['lineaIdeal'] = lineaIdeal
    response_data['nroDeTareasTerminadas'] = tareasTerminadas
    response_data['usRestantes'] = usRestantes
    response_data['cantidadPP'] = planningPokersUs.count()
    return JsonResponse(response_data)


# Responde a la peticion de visualizacion del burndownChart
def burndownChartIndex(request, idSprint):
    sprint = Sprint.objects.filter(id=idSprint).first()
    proyecto = Proyectos.objects.filter(id=sprint.idProyecto.id).first()
    context = {
        'idSprint': idSprint,
        'proyecto': proyecto,
        'idProyecto': proyecto.id
    }

    return render(request, "Proyecto/BurnDownChart/index.html", context)


# funcion para crear el rol SM al crear un proyecto, lo que permite es agregar al scrum master asignado al proyecto como miembro del  proyecto
# por lo tanto las actividades que realice el SM se pueden registrar
def crearRolSm(proyecto):
    # instanciar el objeto
    RolSM = Roles()
    RolSM.idProyecto = proyecto
    # nombre del rol predeterminado es "SM_under"
    RolSM.nombre = "SM_under"
    RolSM.descripcion = "rol SM_under creado por debajo al crear un proyecto"
    RolSM.agregarUserStory = True
    RolSM.eliminarUserStory = True
    RolSM.modificarUserStory = True
    RolSM.agregarMiembro = True
    RolSM.modificarMiembro = True
    RolSM.eliminarMiembro = True
    RolSM.crearRol = True
    RolSM.modificarRol = True
    RolSM.eliminarRol = True
    RolSM.crearSprint = True
    RolSM.empezarSprint = True
    RolSM.finalizarSprint = True
    RolSM.agregarSprintBacklog = True
    RolSM.modificarSprintBacklog = True
    RolSM.save()

    # va a retornar el id del rol
    return Roles.objects.order_by('-id')[0]


# en la funcion asignamos al miembro scrum master el rol "SM_under", para que pueda identificarse como miembro
def asignarRolSm(idProyecto, user, idRol):
    miembro = miembros()
    miembro.idRol = idRol
    miembro.idProyecto = idProyecto
    miembro.idUser = user
    miembro.activo = True
    miembro.lunes = 0
    miembro.martes = 0
    miembro.miercoles = 0
    miembro.jueves = 0
    miembro.viernes = 0
    miembro.sabado = 0
    miembro.domingo = 0

    miembro.save()


# Metodo de vista para ver la confirmacion de Finalizar un proyecto
def confirmarFinalizarProyecto(request, id):
    proyecto = get_object_or_404(Proyectos, pk=id)

    # post, para cuando ya se hace la solicitud de finalizar
    if request.method == 'POST':
        # cambia el estado del proyecto a Finalizado
        return redirect('Proyecto:verKanban', id)

    context = {
        'proyecto': proyecto,
    }

    return render(request, "Proyecto/finalizarProyecto.html", context)


# Metodo para finalizar un proyecto "En Curso"
def finalizarProyecto(request, id):
    # Recupera datos del protecto a finalizar
    proyecto = get_object_or_404(Proyectos, pk=id)

    # Finalizar el proyecto, settea fecha fin proyecto
    proyecto.estado = 'T'
    proyecto.fechaFin = date.today()
    proyecto.save()
    listaSprints = Sprint.objects.filter(idProyecto=proyecto)

    # Cancelando Sprints inicados o Planeados
    for sprint in listaSprints:
        if sprint.estado != 'T':
            sprint.estado = 'C'
            sprint.save()

    return redirect('Proyecto:DetallesProyecto', id)


# remite un resumen de las historias de usuarios cuando se desea finalizar el sprint
def resumenFinSprint(request, id, idSprint):
    # obtenemos el sprint
    sprint = get_object_or_404(Sprint, pk=idSprint)
    # traemos los pp del proyecto relacionados al sprint
    listUs = PlanningPoker.objects.filter(idSprint=sprint).order_by('prioridad')
    # verifico si evaluo todas las istorias de usuario que estaan en el estado H
    # esta bandera sirve para verificar si existe una US que esta en H
    bandUsH = False

    for lu in listUs:
        if lu.idUs.estado == 'H':
            bandUsH = True
            break

    context = {
        'idProyecto': id,
        'idSprint': idSprint,
        'listUs': listUs,
        'bandUsH': bandUsH,
    }

    return render(request, 'Desarrollo/Sprint/ResumenFinSprint.html', context)


# el metodo finaliza el sprint haciendo los ajustes necesarios a los datos
def finSprint(request, id, idSprint):
    sprint = get_object_or_404(Sprint, pk=idSprint)
    listUs = PlanningPoker.objects.filter(idSprint=sprint).order_by('prioridad')

    # por cada us en ese sprint si su estado quedo en 'en proceso' lo modifica a que no esta terminado del sprint anterior y aumenta su prioridad
    # tambien se debe registrar el estado en el que terminan en el sprint
    registroModificacion = HistorialModificacion()
    for lu in listUs:
        # genera el registro con los datos base
        registroModificacion = registrarModificacionBase(request, id)
        # genera el detalle
        registroModificacion.detalle = "Estado final en el Sprint %d : %s" % (sprint.numero, obtenerEstadoUs(lu.idUs))
        # relaciona la US con el detalle
        registroModificacion.idUs = lu.idUs
        # guarda el registro
        registroModificacion.save()
        if lu.idUs.estado == 'EP' or lu.idUs.estado == 'P':
            us = get_object_or_404(userStory, pk=lu.idUs.id)
            us.estado = 'STSA'
            us.prioridad = 1
            us.save()

    # se actualiza el estado a Terminado
    sprint.estado = 'T'
    sprint.save()

    return redirect('Proyecto:ListarSprint', id)


# Edita Actividad de US del kanban
def editarActividadUs(request, id, idSprint, idUs, volver, idActividad, volverkanban):
    # recupera datos de la actividad
    actividad = get_object_or_404(ActividadUs, pk=idActividad)

    if request.method == 'POST':  # post, para cuando ya se hace la solicitud de guardar
        formEditarActividad = FormEditActividad(request.POST, instance=actividad)
        # validacion del formulario
        if formEditarActividad.is_valid():
            formEditarActividad.save()
            actualizarTiempoDedicado(idUs)
            return redirect('Proyecto:historialUs', id, idUs, volver, volverkanban, idSprint)
    else:  # solicitud de despliegue
        formEditarActividad = FormEditActividad(instance=actividad)

        proyecto = get_object_or_404(Proyectos, pk=id)
        miembro = miembros.objects.filter(idProyecto=proyecto, idUser=request.user).first()

    context = {
        'formEditarActividad': formEditarActividad,
        'idProyecto': id,
        'idSprint': idSprint,
        'idUs': idUs,
        'idMiembro': miembro,
        'volver': volver,
        'volverkanban': volverkanban
    }

    return render(request, "Proyecto/Actividad/editarHistorialUs.html", context)


# metodo para registrar el historial donde asigna los datos base al objeto, OJO no asigna el detalle, varía dependiendo del caso y tampoco asigna el US
def registrarModificacionBase(request, id):
    tiempo = datetime.datetime.now()
    registroHistorialMod = HistorialModificacion()
    registroHistorialMod.hora = tiempo.time()
    registroHistorialMod.fecha = tiempo.date()
    registroHistorialMod.idMiembro = obtenerMiembro(request, id)
    return registroHistorialMod


# Metodo para obtener el estado de un US casteado correctamente
def obtenerEstadoUs(us):
    if us.estado == 'P':
        return "Pendiente"
    elif us.estado == 'EP':
        return "En Proceso"
    elif us.estado == 'H':
        return "Hecho"
    else:
        return "Aprobado"


# Crar un Mail con template
# user_mail_destino: email del correo destino
# asunto: Asunto del correo
# template_name: Path del template para usar de Base
# context: diccionario de contexto para el template
def create_mail(user_mail_destino, asunto, template_name, context):
    # Settea el template
    template = get_template(template_name)
    # Settea el contexto dentro del template
    content = template.render(context)

    # Crea un MAIL con los datos anteriores
    message = EmailMultiAlternatives(
        subject=asunto,
        body='',
        from_email=settings.EMAIL_HOST_USER,
        to=[
            user_mail_destino
        ],
        cc=[]
    )

    message.attach_alternative(content, 'text/html')

    # Retorna el mail
    return message


# Enviar Mail al crear un Planning Pocker, para notificar el Encargado que se le asigno un US
def enviarMailPP(mailDestino, asunto, miembro, sprint, us, prioridad, url, estimado):
    # Settea datos de entra para crear un mensaje de tipo EMAIL
    mail = create_mail(
        mailDestino,
        asunto,
        'Mails/AlCrearPP.html',
        {
            'miembro': miembro,
            'url': url,
            'sprint': sprint,
            'us': us,
            'prioridad': prioridad,
            'estimado': estimado,
        }
    )

    # Accion de Enviar Mail
    mail.send(fail_silently=False)


# Al agregar un Miembro, enciar que se le agrego al proyecto X.(URL de ver miembro)
def enviarMailANuevoMiembro(mailDestino, asunto, miembro, url, proyecto, rol):
    # Settea datos de entra para crear un mensaje de tipo EMAIL
    mail = create_mail(
        mailDestino,
        asunto,
        'Mails/AlAgregarMiembro.html',
        {
            'miembro': miembro,
            'url': url,
            'proyecto': proyecto,
            'rol': rol,
        }
    )

    # Accion de Enviar Mail
    mail.send(fail_silently=False)


# Al empezar un sprint. enviar a todos los mienbros que tienen plannig diciendo que empezo el sprint.(URL del kanban)
def enviarMailEmpiezaSprint(mailDestino, asunto, miembro, url, proyecto, sprint):
    # Settea datos de entra para crear un mensaje de tipo EMAIL
    mail = create_mail(
        mailDestino,
        asunto,
        'Mails/AlEmpezarSprint.html',
        {
            'miembro': miembro,
            'url': url,
            'proyecto': proyecto,
            'sprint': sprint,
        }
    )

    # Accion de Enviar Mail
    mail.send(fail_silently=False)


# Cuando un Scrum pase a "Aprobado" un US en el kanban. notificar al miembro encargado que el US
# ha sido aprobado por el scrum(URL ver actividades del US aprobado)
def enviarMailUSAprobado(mailDestino, asunto, miembro, url, proyecto, sprintnro, us):
    # Settea datos de entra para crear un mensaje de tipo EMAIL
    mail = create_mail(
        mailDestino,
        asunto,
        'Mails/AlAprobarUS.html',
        {
            'miembro': miembro,
            'url': url,
            'proyecto': proyecto,
            'sprint': sprintnro,
            'us': us,
        }
    )

    # Accion de Enviar Mail
    mail.send(fail_silently=False)


# Lista de US en pdf
def listUserStoryPdf(request, idSprint):
    # Recupera Datos
    sprint = get_object_or_404(Sprint, pk=idSprint)
    listUs = PlanningPoker.objects.filter(idSprint=sprint).order_by('-prioridad')
    horasTrabajadas = []
    # Calcula suma de horas de las actividades realizadas sobre un US
    for us in listUs:
        actividades = ActividadUs.objects.filter(idUs=us.idUs, idSprint=sprint)
        sum = 0;
        for a in actividades:
            sum += a.duracion
        horasTrabajadas.append(sum)

    # Se pasa el contexto o los datos que seran necesarios en el html
    data = {
        'ProyectoNombre': sprint.idProyecto.nombre,
        'SprintFechaInicio': sprint.fechaInicio,
        'SprintFechaFin': sprint.fechaFin,
        'Sprint': sprint,
        'listUsPdf': listUs,
        'horasTrabajadas': horasTrabajadas
    }
    # Se renderiza el html en pdf
    pdf = render_to_pdf('Proyecto/Reportes/SprintBacklogPdf.html', data)
    return HttpResponse(pdf, content_type='application/pdf')


# Lista el product backlog y lo muestra en Pdf
def listProductBacklogPdf(request, idProyecto):
    # Se traen los US y se ordena por fecha Ingreso descendente
    listUs = userStory.objects.filter(idProyecto=idProyecto).order_by('-fechaIngreso')
    # obtenemos las US de que filtradas por el id del proyecto y ordenadas por el nombre
    proyecto = get_object_or_404(Proyectos, pk=idProyecto)
    # Se pasa el contexto o los datos que seran necesarios en el html
    context = {
        'ProyectoNombre': proyecto.nombre,
        'listUs': listUs,
        'idProyecto': idProyecto,
        'proyecto': proyecto,
    }
    # Se renderiza el html en pdf
    pdf = render_to_pdf('Proyecto/Reportes/ProductBacklogPdf.html', context)
    return HttpResponse(pdf, content_type='application/pdf')


# Lista los US del sprint actual y lo muestra en Pdf
def listUsSprintActualPdf(request, idProyecto):
    # Se traen los datos del proyecto para dar mas datos de este
    proyecto = get_object_or_404(Proyectos, pk=idProyecto)
    sprint = Sprint.objects.filter(idProyecto=idProyecto, estado='E').first()
    # Se traen los planning pokers que tienen relacionadas las US y se ordena por prioridad descendente
    PP = PlanningPoker.objects.filter(idSprint=sprint).order_by('-prioridad')
    # Se pasa el contexto o los datos que seran necesarios en el html
    context = {
        'ProyectoNombre': proyecto.nombre,
        'Sprint': sprint,
        'SprintEstado': EstadoSprint(sprint.estado),
        'listPP': PP,
        'SprintFechaInicio': sprint.fechaInicio,
        'SprintFechaFin': sprint.fechaFin,
        'idProyecto': idProyecto,
        'proyecto': proyecto,
    }
    # Se renderiza el html en pdf
    pdf = render_to_pdf('Proyecto/Reportes/SprintActualPdf.html', context)
    return HttpResponse(pdf, content_type='application/pdf')


# Metodo para retorna Estado de Sprint Cateado correctemente
def EstadoSprint(estado):
    estadoNuevoString = ''
    if (estado == 'P'):
        estadoNuevoString = 'Pendiente'
    elif (estado == 'E'):
        estadoNuevoString = 'En Curso'
    elif (estado == 'T'):
        estadoNuevoString = 'Terminado'
    elif (estado == 'C'):
        estadoNuevoString = 'Cancelado'
    return estadoNuevoString


# Metodo para retorna Estado de Proyecto Cateado correctemente
def EstadoProyecto(estado):
    estadoNuevoString = ''
    if (estado == 'P'):
        estadoNuevoString = 'Pendiente'
    elif (estado == 'E'):
        estadoNuevoString = 'En Curso'
    elif (estado == 'T'):
        estadoNuevoString = 'Terminado'
    elif (estado == 'C'):
        estadoNuevoString = 'Cancelado'
    return estadoNuevoString


# Metodo para retorna Estado de US Cateado correctemente
def EstadoUserStory(estado):
    estadoNuevoString = 'En Planning Poker'
    # El estado nuevo es pasado a String
    if (estado == 'P'):
        estadoNuevoString = 'Pendiente'
    elif (estado == 'EP'):
        estadoNuevoString = 'En Proceso'
    elif (estado == 'STSA'):
        estadoNuevoString = 'Sin Terminar Sprint Anterior'
    elif (estado == 'A'):
        estadoNuevoString = 'Aprobado'
    elif (estado == 'H'):
        estadoNuevoString = 'Hecho'
    elif (estado == 'C'):
        estadoNuevoString = 'Cancelado'
    return estadoNuevoString


# Metodo vista de ver Miembros en un sprint
def miembroEnSprint(request, idSprint):
    sprint = Sprint.objects.filter(pk=idSprint).first()
    listapp = PlanningPoker.objects.filter(idSprint=sprint)
    listaMiembros = []
    horasEstimadas = []
    horasTrabajadas = []

    # para obtener la lista de miembros
    for pp in listapp:
        a = True
        for i in listaMiembros:
            if pp.miembroEncargado == i:
                a = False
        if a:
            listaMiembros.append(pp.miembroEncargado)

    # Calcular horas trabajadas
    for miembro in listaMiembros:
        sumEstimado = 0
        sumTrabajado = 0
        # para calcular cuantas horas se le asigno
        listaPPMiembro = PlanningPoker.objects.filter(idSprint=sprint, miembroEncargado=miembro)
        for pp in listaPPMiembro:
            # Horas estimada por planning poker
            if (pp.estimacionFinal):
                sumEstimado += pp.estimacionFinal
            # horas trabajadas por planning poker
            actividades = ActividadUs.objects.filter(idUs=pp.idUs, idSprint=sprint)
            for a in actividades:
                sumTrabajado += a.duracion
        horasEstimadas.append(sumEstimado)
        horasTrabajadas.append(sumTrabajado)

    context = {'listaMiembros': listaMiembros,
               'horasEstimadas': horasEstimadas,
               'horasTrabajadas': horasTrabajadas,
               'idProyecto': sprint.idProyecto,
               }

    return render(request, "Proyecto/miembroSprint.html", context)
